<?php 
namespace Api;
use Api\Contracts\Api as ApiContract,
		Api\Options\Configurable,
		Api\Traits\Getterable,
		Api\Routing\Router,
		DirectoryIterator;
/**
 * Servicio para el manejo de la compañía activa en el sistema.
 */
class Api implements ApiContract{
	use Configurable,
			Getterable{
				__get as private __getter;
			}

	public static function bootstrap(){
		$r = Router::newInstance(__DIR__.'/../routes/')->namespace('App\Http\Controllers')
																							 ->run();
		app()->configure('api');
		$locale = isset($_SERVER['HTTP_ACCEPT_LANGUAGE']) 
							? trim($_SERVER['HTTP_ACCEPT_LANGUAGE'])
							: '';
		$locale = strtolower(empty($locale) 
												 ? env('APP_LOCALE', 'en') 
												 : $locale);
		app('translator')->setLocale($locale);

		date_default_timezone_set(env('APP_TIMEZONE','UTC'));
	}

	protected $_modules = [
		'auth'     => 'Api\Api\Auth',
		'company'  => 'Api\Api\Company',
		'group'    => 'Api\Api\Group',
		'response' => 'Api\Api\Response',
		'user'     => 'Api\Api\User',
		'upload'   => 'Api\Api\Upload',
		'translator' => 'Api\Api\Translator',
		'notify'   => 'Api\Api\Notify'
	];

	public function __construct(){
		$this->options->import(config('api'));
	}

	/**
	 * Método mágico.
	 * Devuelve un módulo de la instancia.
	 * @param  string $name Nombre del módulo.
	 * @return Api\Contracts\Api\Module
	 */
	public function __get($name){
		if($this->has($name)){
			return $this->get($name);
		}
		return $this->getProperty($name);
	}

	/**
	 * Inicializa un módulo.
	 * @param  string $name Nombre del módulo.
	 * @return void
	 */
	public function initialize($name){
		if(isset($this->_modules[$name])){
			if(is_string($this->_modules[$name])){
				$this->_modules[$name] = new $this->_modules[$name]($this,$this->options->get($name));
			}
		}
	}
	/**
	 * Determina la existencia de un módulo.
	 * @param  string $name Nombre del módulo.
	 * @return boolean
	 */
	public function has($name){
		return is_string($name) && isset($this->_modules[strtolower($name)]);
	}

	/**
	 * Devuelve un módulo de la instancia.
	 * @param  string $name Nombre del módulo.
	 * @return Api\Contracts\Api\Module
	 */
	public function get($name=NULL){
		if($name === null){
			return $this;
		}
		if(!$this->has($name)){
			return null;
		}
		$ci = strtolower($name);
		$this->initialize($ci);
		return $this->_modules[$ci];
	}

}