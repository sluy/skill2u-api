<?php namespace Api\Api;
use App\Models\User,
		App\Models\User\Log,
		App\Models\User\Session,
		Carbon\Carbon;
/**
 * Módulo de la api dedicado a la autenticación y verificación del usuario autenticado.
 * 
 */
class Auth extends Module {
	/**
	 * sesión actual.
	 * @var string|null
	 */
	protected $session;

	/**
	 * Método ejecutado al iniciar la clase.
	 * @return void
	 */
	protected function boot(){
		$this->load();
	}

	/**
	 * Determina si se existe un usuario autenticado en el sistema.
	 * @return boolean
	 */
	public function guess(){
		return !$this->session;
	}
	
	/**
	 * Devuelve el modelo del usuario actualmente autenticado o establece 
	 * el nuevo usuario autenticado en el sistema.
	 * @param  integer|App\User|null $user Id o instancia del usuario a 
	 *                                     establecer como autenticado.
	 * @return App\User|null
	 */
	public function user($user=null){
		if(is_string($user)){
			if(strval(intval($user)) == $user){
				$user = intval($user);
			}else{
				$user = User::where('email',$user)->first();
			}
		}
		if(is_int($user)){
			$user = User::find($user);
		}
		if($user instanceof User){
			$session = new Session;
			$session->user_id = $user->id;
			$session->token   = $this->makeToken();
			$this->session($session);
			$this->log('signin.success',
								 $this->session->id,
								 null,
								 $user);
		}
		return $this->session ? $this->session->user : null;
	}

	/**
	 * Genera un registro de log para el usuario autenticado.
	 * 
	 * @param  string     $event    Nombre del evento.
	 * @param  int|null   $resource Recurso asociado o su id.
	 * @param  array|null $data     Datos adicionales del log.
	 * @param  User|null  $user     Usuario asociado. Si no es especificado
	 *                              usará el usuario actualmente autenticado.
	 *                              
	 * @return App\Models\User\Log
	 */
	public function log(string $event,
											int $resource=null,
											array $data=null,
											User $user=null){
		$user = $user ? $user : $this->user();
		$log = new Log;
		$log->code = $event;
		$log->resource_id = is_object($resource)?$resource->id:$resource;
		$log->user_id = $user->id;
		$log->data = is_array($data) ? json_encode($data) : null;
		if($log->user_id){
			$log->save();
		}
		return $log;
	}

	/**
	 * Devuelve la sesión asociada al usuario actualmente autenticado o establece
	 * la sesión del usuario autenticado en el sistema.
	 * @param  string|App\UserSession|null $session Token o instancia de la sesión.
	 * @return App\UserSession|null
	 */
	public function session($session=null){
		if($session !== null){
			if($session === false){
				if($this->session instanceof Session){
					$this->session->delete();
				}
				$this->session = null;
			}else{
				if(is_string($session)){
					$session = Session::where('token',$session)->first();
					if(!$session){
						return $this->session(false);
					}
				}
				if($session instanceof Session){
					$expiration = $this->options->get('session.token.expiration',1,'integer')*3600;
					
					//Chequeamos que el token sea válido aún.
					if(!empty($session->updated_at)){
						$now = Carbon::now();
						$last = Carbon::createFromFormat('Y-m-d H:i:s',$session->updated_at);
						if($now->diffInSeconds($last) > $expiration){
							$session->delete();
							return $this->session;
						}
					}
					$session->updated_at = Carbon::now();
					$session->save(['timestamps'=>false]);
					if($this->session && $this->session->token != $session->token){
						$this->session->delete();
					}
					$this->session = $session;
				}
			}
		}
		return $this->session;
	}
	/**
	 * Refresca de la base de datos al modelo del usuario actualmente autenticado.
	 * @return Auth
	 */
	public function refresh(){
		if(!$this->guess()){
			$this->user = User::find($this->user->id);
		}
		return $this;
	}

	/**
	 * Genera un token único para una sesión.
	 * @return string
	 */
	public function makeToken(){
		while (true) {
      $token = str_random($this->api->options->get('auth.session.token.length',64));
    	if(!Session::where('token',$token)->first()){
    		return $token;
    	}
    }
	}

	/**
	 * Autentica el usuario via Token.
	 * @return void
	 */
	public function load(){
		$from = $this->options->get('session.token.from','header');
		$name = $this->options->get('session.token.name','Authorization');

		$token = null;
		if($from == 'header' || $from == 'all'){
			$token = trim(strval(request()->header($name)));
		}
		if(!$token  && ($from == 'input' || $from == 'all')){
			$token = request()->input($name);
		}
		$this->session(is_string($token) && !empty($token) ? $token : false);
	}
}
