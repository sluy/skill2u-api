<?php namespace Api\Api\Auth;

trait Authenticatable
{
  /**
   * Devuelve el nombre de la columna usada cómo nombre único.
   *
   * @return string
   */
  public function getAuthIdentifierName()
  {
    return 'email';
  }
  
  /**
   * Devuelve el valor de la columna usada cómo nombre único.
   *
   * @return mixed
   */
  public function getAuthIdentifier()
  {
    return $this->{$this->getAuthIdentifierName()};
  }

  /**
   * Get the password for the user.
   *
   * @return string
   */
  public function getAuthPassword()
  {
    return $this->password;
  }

  /**
   * Devuelve el valor del último token 
   *
   * @return string
   */
  public function getRememberToken()
  {
    return $this->{$this->getRememberTokenName()};
  }

  /**
   * Establece el valor del token para recordar el usuario.
   *
   * @param  string                $value Valor del token.
   * @return void
   */
  public function setRememberToken($value)
  {
  	$this->{$this->getRememberTokenName} = $value;
  }

  /**
   * Devuelve el nombre de la columna dentro de la tabla de tokens de usuario.
   *
   * @return string
   */
  public function getRememberTokenName()
  {
    return 'token';
  }

  /**
   * Devuelve el nombre usado para las relaciones del modelo actual con otros modelos.
   * @return string
   */
  public function getRelationName(){
    return 'id';
  }

  /**
   * Devuelve el nombre usado para las relaciones de otros modelos con el modelo actual.
   * @return string
   */
  public function getForeignName(){
    return 'user_id';
  }
}