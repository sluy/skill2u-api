<?php namespace Api\Api\Auth;

trait Authorizable
{
    /**
     * Determina si la entidad tiene la habilidad suministrada.
     *
     * @param  string  $ability
     * @param  array|mixed  $arguments
     * @return bool
     */
    public function can($ability, $arguments = [])
    {
    	return app('api')->auth->check($ability,$arguments);
    }

    /**
     * Determina si la entidad no tiene la habilidad suministrada.
     *
     * @param  string  $ability
     * @param  array|mixed  $arguments
     * @return bool
     */
    public function cant($ability, $arguments = [])
    {
        return ! $this->can($ability, $arguments);
    }

    /**
     * Determine si la entidad no tiene la habilidad suministrada.
     *
     * @param  string  $ability
     * @param  array|mixed  $arguments
     * @return bool
     */
    public function cannot($ability, $arguments = [])
    {
        return $this->cant($ability, $arguments);
    }
}