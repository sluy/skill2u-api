<?php namespace Api\Api\Auth;
use Illuminate\Contracts\Auth\Authenticatable as UserContract,
    Illuminate\Support\Facades\Hash;
trait UserProvider{
  /**
   * Devuelve el nombre de la clase usada como modelo de la instancia.
   *
   * @return string
   */
  public function getModel()
  {
    return $this->options->get('models.user','App\User');
  }

  /**
   * Crea una nueva instancia del modelo.
   *
   * @return \Illuminate\Database\Eloquent\Model
   */
  public function createModel()
  {
      $class = '\\'.ltrim($this->getModel(), '\\');
      return new $class;
  }
  /**
   * Devuelve el modelo usado para las sesiones.
   * @return string
   */
  public function getSessionModel(){
    return $this->options->get('models.session','App\UserToken');
  }

  /**
   * Devuelve la instancia del modelo usado para las sesiones.
   * @return App\UserSession
   */
  public function createSessionModel(){
  		$class = '\\'.ltrim($this->getSessionModel(), '\\');
      return new $class;
  }
  
	/**
	 * Devuelve un usuario por su identificador único.
	 * @param  integer $identifier Valor del identificador único.
	 * @return App\Contracts\Api\Auth\Authenticatable|null
	 */
	public function retrieveById($identifier){
		$model = $this->createModel();
		return $model->newQuery()
						->withTrashed()
            ->where($model->getAuthIdentifierName(), $identifier)
            ->first();
	}
	/**
	 * Busca un usuario por su token de sesión.
	 * @param  integer $identifier Valor del identificador único.
	 * @param  string $token       Valor del token.
	 * @param  boolean $active     Determina si se verificará que el mismo esté activo.
	 * @return App\Contracts\Api\Auth\Authenticatable|null
	 */
	public function retrieveByToken($identifier,$token){
		$model = $this->retrieveById($identifier);
		if(!$model){
			return null;
		}
		$session = $this->createSessionModel()
										->newQuery()
										->where($user->getRememberTokenName(),$token)
										->where($user->getForeignName(),$model->{$user->getRelationName()})
										->first();
		if($session){
			return $model;
		}
	}

	/**
	 * Devuelve el usuario que esté asociado al token suministrado.
	 * @param  string  $token  Token del usuario.
	 * @param  boolean $active Determina si se verificará que el mismo esté activo.
	 * @return App\Contracts\Api\Auth\Authenticatable|null
	 */
	public function retrieveOnlyByToken($token,$minDate=null){
		$user = $this->createModel();
		$session = $this->createSessionModel()
										->newQuery()
										->where($user->getRememberTokenName(),$token);
		if($minDate){
			$session->where('updated_at','>',$minDate);
		}
		$session = $session->first();

		if(!$session){
			return null;
		}
		return $user->newQuery()
							  ->where($user->getRelationName(),$session->{$user->getForeignName()})
							  ->first();
	}

  /**
   * Actualiza el token usado por el usuario para la sesión activa.
   *
   * @param  \Illuminate\Contracts\Auth\Authenticatable  $user
   * @param  string  $token
   * @return void
   */
  public function updateRememberToken(UserContract $user, $token)
  {
  	if(!is_string($token) || strlen($token) < 64){
  		return;
  	}
  	$session = $this->createSessionModel()->newQuery()
  																				->where($user->getForeignName(),
  																								$user->{$user->getRelationName()})
  																				->where($user->getRememberTokenName(),$token)
  																				->first();
  	if(!$session){
  		$session = $this->createSessionModel();
			$session->{$user->getForeignName()} = $user->{$user->getRelationName()};
			$session->{$user->getRememberTokenName()} = $token;
  	}
  	$session->save();
  }

	/**
	 * Devuelve el usuario que coincida con las credenciales suministradas.
	 * @param  array  $credentials Credenciales a comprobar.
	 * @return \Illuminate\Contracts\Auth\Authenticatable|null
	 */
	public function retrieveByCredentials(array $credentials){
		if (empty($credentials)) {
      return;
    }
    $query = $this->createModel()->newQuery()->withTrashed();
    foreach ($credentials as $key => $value) {
      if (! Str::contains($key, 'password')) {
        $query->where($key, $value);
      }
    }
    return $query->first();
	}

  /**
   * Valida el usuario suministrado contra las credenciales suministradas.
   *
   * @param  \Illuminate\Contracts\Auth\Authenticatable  $user
   * @param  array  $credentials
   * @return bool
   */
  public function validateCredentials(UserContract $user, array $credentials)
  {
      $pass = $credentials['password'];
      return Hash::check($pass, $user->getAuthPassword());
  }

	/**
	 * Genera un nuevo token de autenticación.
	 * 
	 * @return String
	 */
	public function generateToken(){
		$user = $this->createModel();
    while (true) {
      $token = str_random($this->api->options->get('auth.session.token.length',64));
    	$session = $this->createSessionModel()->newQuery()
																			->where($user->getRememberTokenName(),$token)
																			->first();
      if(!$session){
        return $token;
      }
    }
	}
}