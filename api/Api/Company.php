<?php namespace Api\Api;
use Carbon\Carbon,
		App\Models\Company as Model,
		App\Models\Level,
		App\Models\Period,
		App\Models\Performance,
		App\Models\Evaluation,
		App\Models\Lib,
		App\Models\User,
		App\Models\User\Performance as UserPerformance;

class Company extends Module {
	
	public function current(){
		$user = app('Api\Api')->auth->user();
		return ($user instanceof User)
					 ? $user->company
					 : null;
	}

	/**
	 * Devuelve las evaluaciones que pueden ser tomadas por un usuario en particular.
	 * @param  User    $user    [description]
	 * @param  boolean $asQuery [description]
	 * @return [type]           [description]
	 */
	public function availableEvals(User $user,$asQuery=false,$date=null){
		$evals = [];
		foreach($user->company->evals as $eval){
			if($this->canEvaluate($user,$eval,$date)){
				$evals[] = $eval->id;
			}
		}
		$query = $user->company->evals()->whereIn('id',$evals);
		return $asQuery
					 ? $query
					 : $query->get();
	}

	public function availableLibs(User $user, $asQuery=false) {
		$libs = [];
		foreach($user->company->libs as $lib) {
			if($this->canView($user, $lib)) {
				$libs[] = $lib->id;
			}
		}
		$query = $user->company->libs()->whereIn('id', $libs);
		return $asQuery ? $query : $query->get();
	}

	public function canView(User $user, Lib $lib) {
		$levels = $lib->levels;
		if(count($levels)){
			$found = false;
			foreach($levels as $level){
				if($level->level_id == $user->level_id){
					$found = true;
					break;
				}
			}
			if(!$found){
				return false;
			}
		}
		$usegmentations = $user->segmentations()->pluck('segmentation_id')->toArray();
		$segmentations = $lib->segmentations->toArray();
		if(count($segmentations)){
			$found = false;
			foreach($segmentations as $segmentation){
				if(in_array($segmentation['segmentation_id'], $usegmentations)){
					$found = true;
				}
				break;
			}
			if(!$found){
				return false;
			}
		}
		return true;
	}

	/**
	 * Determina si un usuario puede tomar una evaluación en particular.
	 * @param  User       $user       [description]
	 * @param  Evaluation $evaluation [description]
	 * @param  [type]     $now        [description]
	 * @return [type]                 [description]
	 */
	public function canEvaluate(User $user,Evaluation $evaluation,$now=null){
		if($now === null){
			$now = Carbon::now()->toDateString();
		}
		if($now !== false && ($evaluation->start_at > $now || $evaluation->finish_at < $now)){
			return false;
		}
		$levels = $evaluation->levels;
		if(count($levels)){
			$found = false;
			foreach($levels as $level){
				if($level->level_id == $user->level_id){
					$found = true;
					break;
				}
			}
			if(!$found){
				return false;
			}
		}
		$usegmentations = $user->segmentations()->pluck('segmentation_id')->toArray();
		$segmentations = $evaluation->segmentations->toArray();
		
		
		if(count($segmentations)){
			$found = false;
			foreach($segmentations as $segmentation){
				if(in_array($segmentation['segmentation_id'], $usegmentations)){
					$found = true;
				}
				break;
			}
			if(!$found){
				return false;
			}
		}
		$parents = $evaluation->parents;
		if(count($parents)){
			foreach($parents as $parent){
				$ueval = $parent->users()->where('user_id',$user->id)->first();
				if(!$ueval || $ueval->approved_percent <= $ueval->score){
					return false;
				}
			}
		}
		return true;
	}

	public function calculateUsersPerformance(Performance $performance){
		foreach($performance->company->users as $user){
			$this->calculateUserPerformance($performance,$user);
		}
	}

	public function getUserScore(Performance $performance, User $user){
		$company = $user->company;
		$evals = $company->evals()->where('finish_at','>=',$performance->start_at)
															->where('finish_at','<=',$performance->finish_at)
															->get();
		$scores = [];
		$score = null;
		
		foreach($evals as $eval){
			if(!$this->canEvaluate($user,$eval,false)){
				continue;
			}
			$ueval = $eval->users()->where('user_id',$user->id)->first();
			$scores[] = !$ueval ? '0.00' : $ueval->score;
		}

		$count = count($scores);
		if($count){
			$score = 0;
			foreach($scores as $s){
				$score += $s;
			}
			$score = number_format($score/$count,2,'.','');
		}
		return $score;
	}


	public function calculateUserPerformance(Performance $performance,User $user,$score=false){
		$company = $performance->company;
		$uperformance = $user->performances()
												 ->where('performance_id',$performance->id)
												 ->first();
		$weight = number_format($performance->user_eval_weight, 2, '.', '');
		$sysWeight = number_format(100 - $performance->user_eval_weight,2,'.','');

		/*
		$last = $company->performances()->orderBy('id','desc')->first();


		//Comprobamos que los valores a editar sean del último performance creado.
		//Cómo el sistema necesita devolver el nivel al usuario en el caso de edición,
		//si se habilita para performances previos al último, podría crear inconsistencia
		//en los datos
		if($last->id != $performance->id){
			return false;
		}*/

		if(!$uperformance){
			$uperformance = new UserPerformance;
			$uperformance->user_id = $user->id;
			$uperformance->performance_id = $performance->id;
			$uperformance->from_level_id = $user->level->id;
			$uperformance->to_level_id = $user->level->id;
			$uperformance->save();
		}else{
			$user->level_id = $uperformance->from_level_id;
			$uperformance->to_level_id = $uperformance->from_level_id;
			$user->save();
		}

		$nextLevel = $company->levels()
															 ->where('order','>',$user->level->order)
															 ->orderBy('order','asc')
															 ->first();
		$prevLevel = $company->levels()
												 ->where('order','<',$user->level->order)
												 ->orderBy('order','desc')
												 ->first();
		if(!$nextLevel){
			$nextLevel = $user->level;
		}
		if(!$prevLevel){
			$prevLevel = $user->level;
		}
		$uperformance->system_eval = $this->getUserScore($performance,$user);

		if((is_numeric($score) || is_null($score)) && $score !== false){
			$uperformance->user_eval = number_format($score,2,'.','');
			$uperformance->save();
		}
		//Si no hay evaluación del sistema (no hubieron cursos),ni la manual, 
		//el total del período será heredado del último performance del usuario.
		//Tomar en cuenta que el user_eval y system_eval seran null cuando suceda 
		//este evento.
		if($uperformance->user_eval === null || $uperformance->system_eval === null){
			$uperformance->total = null;
			$lastest = $performance->company
														 ->performances()
														 ->where('finish_at','<',$performance->finish_at)
														 ->where('finished',1)
														 ->orderBy('finish_at','desc')
														 ->get();

			foreach($lastest as $test){
				$tmp = $user->performances()->where('performance_id',$test->id)->first();
				if($tmp->total){
					$uperformance->total = $tmp->total;
				}
			}
			if($uperformance->total){
				$uperformance->save();
			}
			return $uperformance;
		}
		$div = 2;
		//Si no hubieron cursos que contar, hacemos que el peso de la evaluación 
		//"manual" sea de 100%. 
		if($uperformance->system_eval === null){
			$weight = 100;
			$sysWeight = 0;
			$div = 1; 
		}
		//Usando la variable
		if ($performance->use_user_eval === false) {
			$weight = 0;
			$uperformance->user_eval = 0;
			$div = 1;
		}
		//Cambio : Ahora se suman y dividen entre la cantidad de variables usadas.
		$uperformance->total = number_format((floatval($uperformance->system_eval) + floatval($uperformance->user_eval)) / $div, '.', '');
		if($user->level->up <= $uperformance->total){
			$user->level_id = $nextLevel->id;
			$uperformance->to_level_id = $nextLevel->id;
		}
		if($user->level->down >= $uperformance->total){
			$user->level_id = $prevLevel->id;
			$uperformance->to_level_id = $prevLevel->id;
		}
		$user->save();
		$uperformance->save();
		return $uperformance;
	}
	/**
	 * Comprueba si a la fecha suministrada culminó un período de cada compañía del sistema, y
	 * en caso de ser así genera el performance.
	 * Usado por el cron job para comprobar diariamente si debe generar el performance.
	 * @param  string  $date    Fecha a comprobar. Si no es especificada tomará el día
	 *                          actual.
	 * @return void
	 */
	public function checkAllPeriods($date=null){
		$performances = [];
		foreach(Model::all() as $company){
			$performance = $this->checkPeriod($date,$company);
			if($performance){
				$performances[] = $performance;
			}
		}
		return $performances;
	}

	public function getPeriodRange($date=null,Model $company=null){
		if(!$company){
			$company = $this->current();
		}
		if(!$date){
			$period = Carbon::now()->toDateString();
		}
		if(!$company){
			return false;
		}
		$date = Carbon::parse($date);
		$period = $company->periods()
											->where('start_at','<',$date->toDateString())
											->orderBy('start_at','desc')
											->first();
		if(!$period){
			return false;
		}
		//Establecemos la fecha de inicio
		$start = Carbon::parse($period->start_at);
		//Establecemos la fecha de finalización.
		$finish = null;
		//Creamos un bucle para establecer correctamente el período actual.
		while(1){
			//Si la fecha de inicio ya sobrepasa la fecha actual, cancelamos 
			//toda la operación.
			if($start->toDateString() > $date->toDateString()){
				return false;
			}
			//Le agregamos a la fecha de inicio los meses
			$finish = $start->copy()->addMonths($period->months);
			//Si la fecha de inicio es menor a la actual, y la de finalización mayor,
			//significa que es el período actual. Devolvemos una matriz cuyos elementos
			//(en orden) es la fecha de inicio y la fecha del fin
			if($start->toDateString() <= $date->toDateString() && 
				 $finish->toDateString() >= $date->ToDateString()){
				return [$start->toDateString(),$finish->toDateString()];
			}
			//incrementamos los meses para buscar el lapso correcto.
			$start->addMonths($period->months);
		}
		return false;

	}

	/**
	 * Comprueba si a la fecha suministrada culminó un período de la compañía suministrada y en 
	 * caso de ser así genera el performance.
	 * @param  string                  $date    Fecha a comprobar. Si no es especificada tomará el día
	 *                                          actual.
	 * @param  App\Models\Company|null $company Compañía a la cual se generará el performance. Si no
	 *                                          es especificada tomará la compañía actual.
	 * @return void
	 */
	public function checkPeriod($date=null,Model $company=null){

		if(!$company){
			$company = $this->current();
		}
		if(!$date){
			$period = Carbon::now()->toDateString();
		}
		
		if(!$company){
			return;
		}
		$date = Carbon::parse($date);
		$userEvalWeight = $company->getOptions()->get('user_eval_weight','50','float');
		$userEvalWeight = number_format($userEvalWeight, 2, '.', '');
		$useUserEval = $company->getOptions()->get('use_user_eval', false, 'boolean');
		$period = $company->periods()
											->where('start_at','<',$date->toDateString())
											->orderBy('start_at','desc')
											->first();
		if(!$period){
			return;
		}
		//Establecemos la fecha de inicio
		$start = Carbon::parse($period->start_at)->addDays(1);
		//Establecemos la fecha de finalización.
		$finish = null;
		$performance = null; 
		//Creamos un bucle para establecer correctamente el período actual.
		while(1){
			//Si la fecha de inicio ya sobrepasa la fecha actual, cancelamos 
			//toda la operación.
			if($start->toDateString() > $date->toDateString()){
				return;
			}
			//Le agregamos a la fecha de inicio los meses
			$finish = $start->copy()->addMonths($period->months);
			$performance = $company->performances()
													 ->where('start_at','>=',$start->toDateString())
													 ->where('finish_at','<=',$finish->toDateString())
													 ->first();
			if(!$performance){
				$performance = new Performance;
				$performance->company_id = $company->id;
				$performance->title = $period->title;
				$performance->picture = $period->picture;
				$performance->options = $period->options;
				$performance->start_at = $start->toDateString();
				$performance->finish_at = $finish->toDateString();
				$performance->user_eval_weight = $userEvalWeight;
				$performance->use_user_eval = $useUserEval;
				$performance->save();
			}
			//incrementamos los meses para buscar el lapso correcto.
			$start->addMonths($period->months);
		}
		return $performance;
	}
	/*
	public function checkPeriod($date=null,Model $company=null){

		if(!$company){
			$company = $this->current();
		}
		if(!$date){
			$period = Carbon::now()->toDateString();
		}
		
		if(!$company){
			return;
		}
		$date = Carbon::parse($date);
		$userEvalWeight = $company->getOptions()->get('user_eval_weight','50','float');
		$userEvalWeight = number_format($userEvalWeight, 2, '.', '');
		$period = $company->periods()
											->where('start_at','<',$date->toDateString())
											->orderBy('start_at','desc')
											->first();
		if(!$period){
			return;
		}
		//Establecemos la fecha de inicio
		$start = Carbon::parse($period->start_at);
		//Establecemos la fecha de finalización.
		$finish = null;
		//Creamos un bucle para establecer correctamente el período actual.
		while(1){
			//Si la fecha de inicio ya sobrepasa la fecha actual, cancelamos 
			//toda la operación.
			if($start->toDateString() > $date->toDateString()){
				return;
			}
			//Le agregamos a la fecha de inicio los meses
			$finish = $start->copy()->addMonths($period->months);

			//Si la fecha actual es igual a la fecha de corte, detenemos
			//ya que es el registro que buscamos.
			if($finish->toDateString() == $date->toDateString()){
				break;
			}
			//incrementamos los meses para buscar el lapso correcto.
			$start->addMonths($period->months);
		}
		$performance = $company->performances()
													 ->where('start_at',$start->toDateString())
													 ->where('finish_at',$finish->toDateString())
													 ->first();
		if(!$performance){
			$performance = new Performance;
			$performance->company_id = $company->id;
			$performance->title = $period->title;
			$performance->picture = $period->picture;
			$performance->options = $period->options;
			$performance->start_at = $start->toDateString();
			$performance->finish_at = $finish->toDateString();
			$performance->user_eval_weight = $userEvalWeight;
			$performance->save();
		}
		return $performance;
	}*/

}