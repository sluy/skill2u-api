<?php namespace Api\Api;
use Api\Api,
		Api\Options,
		Api\Options\Configurable,
		Api\Contracts\Options\Configurable as ConfigurableContract,
		Api\Contracts\Messageable as MessageableContract,
		Api\Traits\Messageable,
		Api\Traits\Batch;
/**
 * Base de los módulos de la API.
 */
class Module implements ConfigurableContract, MessageableContract{
	use Configurable,Messageable,Batch;
	/**
	 * Instancia de la api.
	 * @var Api\Api
	 */
	protected $_api;

	/**
	 * Constructor. Establece la configuración y la api en la instancia.
	 * @param   Api\Api    $api    Instancia de la Api.
	 * @param   array               $config Configuración del módulo.
	 * @return  void
	 */
	public function __construct(Api $api,array $config=null){
		$this->_api = $api;
		$this->getOptions()->import($config);
		if(method_exists($this,'boot')){
			$this->boot();
		}
	}
	
	/**
	 * Devuelve un módulo de la API..
	 * @param  string|null $module Nombre del módulo. Si no es especificado devolverá la instancia de la api.
	 * @return Api\Api\Libs\Module|Api\Api
	 */
	public function getApi($module=null){
		return $this->_api->get($module);
	}

	/**
	 * Método mágico.
	 * Devolverá el resultado de cualquiér función de la instancia que sea compatible con la
	 * estructura get{name}.
	 * 
	 * @param  string $name Nombre del método.
	 * @return mixed
	 */
	public function __get($name){
		if(method_exists($this,'get'.$name)){
			return call_user_func([$this,'get'.$name]);
		}
		return null;
	}
	/**
	 * Método mágico.
	 * Ejecuta la función de la instancia que sea compatible con la
	 * estructura set{name}.
	 * 
	 * @param  string $name  Nombre del método.
	 * @param  mixed  $value Valor a suministrar.
	 * @return mixed
	 */
	public function __set($name,$value){
		if(method_exists($this,'set'.$name)){
			call_user_func_array([$this,'set'.$name], [$value]);
		}
	}
}