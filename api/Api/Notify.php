<?php namespace Api\Api;
use App\Models\User,
		App\Models\Company,
		App\Models\User\Notification,
		Traversable;

class Notify extends Module {
	
	public function __call($name,$args){
		$call = 'store';
		$name = strtolower($name);
		if(strlen($name) > 4 && substr($name, 0,4) == 'mass'){
			$call = 'massStore';
			$name = substr($name, 4);
		}
		array_unshift($args, $name);
		call_user_func_array([$this,$call], $args);
	}

	public function massStore($type,$users,$title,$message,array $vars=array(),$sendMail=false){
		foreach($users as $user){
			$tmp = $vars;
			$tmp['user'] = $user;
			$this->store($type,$title,$message,$tmp,$sendMail);
		}
	}

	public function store($type,$title,$message,array $vars,$sendMail=false){
		if(!isset($vars['user'])){
			$vars['user'] = app('Api\Api')->auth->user();
			$vars['user']->profile;
		}

		if(!isset($vars['company'])){
			if(is_array($vars['user']) && isset($vars['user']['company'])){
				$vars['company'] = Company::find($vars['user']['company']);
			}else if($vars['user'] instanceof User){
				$vars['company'] = $vars['user']->company;
			}
		}

		$vars = $this->formatVars($vars);

		foreach($vars as $name => $value){
			$name = '['.$name.']';
			if(stripos($title, $name) !== false){
				$title = str_replace($name, $value, $title);
			}
			if(stripos($message, $name) !== false){
				$message = str_replace($name, $value, $message);
			}
		}
		$notification = new Notification;
		$notification->type    = $type;
		$notification->user_id = $vars['user.id'];
		$notification->title   = $title;
		$notification->message = $message;
		$notification->unread  = $sendMail === true
														 ? false
														 : true;
		$notification->save();
		if($sendMail && function_exists('mail')){
			// Always set content-type when sending HTML email
			$headers = "MIME-Version: 1.0" . "\r\n";
			$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
			// More headers
			$headers .= 'From: <noreply@skill2u.com>' . "\r\n";
			mail($vars['user.email'],$title,wordwrap($message,70),$headers);
		}
		return $notification;
	}


	protected function formatVars($vars,$prefix=""){
		if(!is_array($vars) && !($vars instanceof Traversable)){
			return [];
		}
		$formatted = [];
		
		foreach($vars as $name => $value){
			if($prefix){
				$name = $prefix . '.' . $name;
			}
			if(is_object($value) && method_exists($value, 'toArray')){
				$value = $value->toArray();
			}
			if(is_array($value) || $value instanceof Traversable){
				$value = $this->formatVars($value,$name);
				$formatted = array_merge($formatted,$value);
			}else{
				$formatted[$name] = $value;
			}
		}
		return $formatted;
	}
}