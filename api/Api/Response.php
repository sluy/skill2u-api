<?php namespace Api\Api;
use Illuminate\Http\Response as IResponse,
		Api\Format;
class Response extends Module{

	protected $codes = [
		'ok'           => 200,
		'success'      => 200,
		'created'      => 201,
		'updated'      => 200,
		'deleted'      => 200,
		'restored'     => 201,
		'bad'          => 400,
		'error'        => 422,
		'unauthorized' => 401,
		'forbidden'    => 403,
		'notfound'     => 404,
		'failure'      => 500,
	];

	public function __call($name,$params){
		$code = null;

		$ci = strtolower($name);

		if(!isset($this->codes[$ci])){
			$ci = 'unknow';
		}
		return response((isset($params[0]) && !empty($params[0]) 
										? $params[0] : trans('response.'.$ci)),
										$this->codes[$ci]);
	}

	public function render($data=null,$code=null){
		if ($data instanceof IResponse){
			$r = $data;
			$data = $r->content();
			if($code === null){
				$code = $r->status();
			}
		}
		if($code === null){
			$code = 'ok';
		}

		if(is_string($code)){
			$code = strtolower($code);
			if(isset($this->codes[$code])){
				$code = $this->codes[$code];
			}
		}


		if(!is_int($code)){
			$code = 400;
		}
		if($data === null || (is_string($data) && empty($data))){
			if(in_array($code,$this->codes)){
				$data = trans('response.' . array_search($code, $this->codes));
			}
		}
		else if(is_object($data)){
			$data = Format::cast('array',$data);
		}
		http_response_code($code);
		header('Content-Type: application/json');
		die(json_encode(is_scalar($data)?['message' => $data]:$data,true));
	}
}