<?php namespace Api\Api;
use Stichoza\GoogleTranslate\TranslateClient,
		ErrorException;
/**
 * Módulo de la api dedicado a las traducciones con la api de google.
 * Composer:
 * composer require stichoza/google-translate-php
 * 
 */
class Translator extends Module {

	protected $_source = null;
	protected $_target = null;

	protected $_status = false;

	public function status(bool $value){
		$this->_status = $value;
		return $this;
	}

	public function enable(){
		return $this->status(true);
	}

	public function disable(){
		return $this->status(false);
	}

	public function isEnabled(){
		return $this->_status === true;
	}

	public function boot(){
		$locale = isset($_SERVER['HTTP_ACCEPT_LANGUAGE']) 
							? trim($_SERVER['HTTP_ACCEPT_LANGUAGE'])
							: '';
		$locale = strtolower(empty($locale) 
												 ? env('APP_LOCALE', 'en') 
												 : $locale);
		$this->_target = $locale;
	}

	public function setSource($lang){
		$this->_source = $lang;
		return $this;
	}

	public function setTarget($lang){
		$this->_target = $lang;
		return $this;
	}

	public function getSource(){
		return $this->_source;
	}

	public function getTarget(){
		return $this->_target;
	}

	public function gtrans($text,$targetLang=null,$sourceLang=null){
		if(!is_string($text) || empty($text)){
			return $text;
		}
		$targetLang = $targetLang === null 
									? $this->_target
									: $targetLang;
		$sourceLang = $sourceLang === null
									? $this->_source
									: $sourceLang;

		if($this->isEnabled()){
			try {
				$tr = new TranslateClient;
				$tr->setSource($sourceLang);
				$tr->setTarget($targetLang);
				$translation = $tr->translate($text);
				return $sourceLang === null && $targetLang == $tr->getLastDetectedSource()
							 ? $text
							 : $translation;
			} catch (ErrorException $e) {}
		}
		return $text;
	} 
}