<?php namespace Api\Api;
use Carbon\Carbon,
		Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Módulo de la api dedicado a la subida de imágenes
 * 
 */
class Upload extends Module {

	protected function getUrl($path=null,$name=null){
		$base = trim(strval($this->options->get('url', 'uploads')));
		$name = trim(strval($name),'/');
		$path = trim(strval($path),'/');
		return trim(trim($base . '/' . $path,'/') . '/' . $name,'/');
	}

	/**
	 * Devuelve la ruta absoluta de acceso de un archivo dentro del directorio de uploads.
	 * @param  string $path Ruta relativa del archivo o directorio.
	 * @return string
	 */
	protected function getAbsPath($path=null){
		return app()->basePath('public/'.$this->getRelPath($path));
	}

	/**
	 * Devuelve la ruta relativa de un archivo dentro del directorio de uploads.
	 * @param  string $path Ruta relativa del archivo o directorio.
	 * @return string
	 */
	protected function getRelPath($path=null){
		$base = trim(strval($this->options->get('paths', 'uploads')));
		$path = trim(strval($path),'/');
		return trim($base . '/' . $path,'/');
	}

	/**
	 * Crea el directorio suministrado recursivamente.
	 * @param  string  $path Directorio a crear.
	 * @return boolean       TRUE si el directorio ya existe o fue creado exitosamente,
	 *                       FALSE si ocurrió algún error al crear el directorio.
	 */
	protected function createPath($path){
		if(empty($path)){
			return false;
		}
		$sections = explode('/',$path);
		$current = '';
		foreach($sections as $dir){
			$current .= '/' . array_shift($sections);
			if(file_exists($current)){
				if(is_dir($current)){
					continue;
				}
				if(!@unlink($current)){
					return false;
				}
			}
			if(!@mkdir($current)){
				return false;
			}
		}
		return true;
	}

	/**
	 * Genera el nombre del archivo.
	 * @param  string $path Directorio absoluto donde irá el archivo.
	 * @param  string $ext  Extensión del archivo.
	 * @return string
	 */
	protected function generateName($path,$ext=null){
		$name = '';
		while (true) {
			$name = str_random(32) . (!empty($ext) ? ('.'.strtolower($ext)) : '');
			if(!file_exists($path . '/' . $name)){
				return $name;
			}
		}
	}

	public function exists($relPath){
		$baseUrl = $this->getUrl();
		$baseLength = strlen($baseUrl);
		if(!is_string($relPath) || empty($relPath) || strlen($relPath) < $baseLength){
			return false;
		}
		$abs = $this->getAbsPath(substr($relPath,$baseLength));
		return file_exists($abs) && is_file($abs);
	}

	/**
	 * Genera un upload a partir del Request.
	 * @param  [type]         $name Nombre del campo del request que tendrá el
	 *                              upload.
	 * @param  string         $path Directorio relativo donde se guardará el archivo.
	 * @return string|false         Devolverá la ruta relativa del archivo o false si 
	 *                              no se pudo crear por algún motivo.
	 */
	public function fromRequest($name,$path=null){
		if(app()->request->file($name)){
			return $this->fromFile(app()->request->file($name),$path);
		}
		$file = trim(trim(app()->request->input($name,''),' '),'/');
		return $this->exists($file)
					 ?$file
					 :'';
	}

	/**
	 * Genera un nuevo archivo a partir de un upload.
	 * @param  UploadedFile|null $file Instancia del archivo subido.
	 * @param  string            $path Directorio relativo donde se guardará el archivo.
	 * @return string|false            Devolverá la ruta relativa del archivo o false si 
	 *                                 no se pudo crear por algún motivo.
	 */
	public function fromFile(UploadedFile $file, $path=null){		
		$rel = $this->getRelPath($path);
		$abs = $this->getAbsPath($rel);
		if(!$file){
			return false;
		}
		if(!$this->createPath($abs)){
			return false;
		}
		$name = $this->generateName($abs,$file->getClientOriginalExtension());
		if($file->move($rel,$name)){
			return $this->getUrl($path,$name);
		}
		return false;
	}
}