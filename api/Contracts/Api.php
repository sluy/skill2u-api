<?php namespace Api\Contracts;
use Api\Contracts\Options\Configurable as ConfigurableContract,
		Api\Contracts\Getterable as GetterableContract;
interface Api extends ConfigurableContract, GetterableContract{

	/**
	 * Determina la existencia de un módulo.
	 * @param  string $name Nombre del módulo.
	 * @return boolean
	 */
	public function has($name);

	/**
	 * Devuelve un módulo de la instancia.
	 * @param  string $name Nombre del módulo.
	 * @return Api\Contracts\Api\Module
	 */
	public function get($name=NULL);
}