<?php namespace Api\Contracts\Api\Auth;

use Illuminate\Contracts\Auth\Authenticatable as IAuthenticatable;

interface Authenticatable extends IAuthenticatable{
	
}

