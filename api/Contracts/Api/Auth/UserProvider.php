<?php namespace Api\Contracts\Api\Auth;

use Illuminate\Contracts\Auth\UserProvider as UserProviderContract;


interface UserProvider extends UserProviderContract{
	
  /**
   * Devuelve el modelo usado para las sesiones.
   * @return string
   */
  public function getSessionModel();

  /**
   * Devuelve la instancia del modelo usado para las sesiones.
   * @return App\UserSession
   */
  public function createSessionModel();

	/**
	 * Devuelve el usuario que esté asociado al token suministrado.
	 * @param  string  $token  Token del usuario.
	 * @param  boolean $active Determina si se verificará que el mismo esté activo.
	 * @return App\Contracts\Api\Auth\Authenticatable|null
	 */
	public function retrieveOnlyByToken($token,$active=true);

	/**
	 * Genera y devuelve un nuevo token de autenticación.
	 * 
	 * @return String
	 */
	public function generateToken();
}
