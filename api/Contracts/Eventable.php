<?php namespace Api\Contracts;
use Closure;
interface Eventable {
	/**
	 * Devuelve la instancia de un módulo de la api.
	 * @param  string|null $module Nombre del módulo. Si no es especificado
	 *                             devolverá la instancia de la API.
	 * @return Api\Api\Module
	 */
	function trigger($events,$arguments=null);

	function on($eventName,Closure $callback);
}
