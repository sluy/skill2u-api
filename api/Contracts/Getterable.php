<?php namespace Api\Contracts;

interface Getterable{
	public function getProperty($name);
}