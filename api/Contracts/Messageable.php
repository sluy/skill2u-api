<?php namespace Api\Contracts;
/**
 * Trait para el manejo de mensajes de error dentro de la instancia.
 */
interface Messageable {
	/**
	 * Establece el mensaje de la instancia.
	 * @param   string|array             $message Mensaje.
	 * @return  Api\Api\Module
	 */
	public function setMessage($message);
	/**
	 * Determina si existe un mensaje en la instancia.
	 * @return boolean
	 */
	public function hasMessage();
	/**
	 * Devuelve el último mensaje de la instancia.
	 * @return array|null
	 */
	public function getMessage();

	/**
	 * Limpia el mensaje de la instancia.
	 * @return Api\Api\Module
	 */
	public function clearMessage();
}