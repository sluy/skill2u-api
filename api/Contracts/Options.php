<?php namespace Api\Contracts;
use Closure;
/**
 * Interface para el manejo de opciones.
 */
interface Options {
	/**
	 * Constructor.
	 * Inicializa la instancia.
	 * @param array|null $values Valores de la instancia.
	 * @param array|null $fields Campos válidos.
	 * @param array|null $types  Tipos de datos.
	 */
	public function __construct($values = null,array $fields=null,array $types=null);

	/**
	 * Bloquea la escritura de datos en la instancia.
	 * @return Api\OptionsInterface
	 */
	public function lock();
	
	/**
	 * Determina si la instancia está bloqueada para la escritura de datos.
	 * @return boolean
	 */
	public function isLocked();

	/**
	 * Determina si un campo es permitido dentro de la instancia.
	 * @param  string  $name Nombre del campo a comprobar.
	 * @return boolean
	 */
	public function isAllowed($name);

	/**
	 * Determina si existe un campo en la instancia.
	 * @param  string  $name Nombre del campo.
	 * @return boolean
	 */
	public function has($name);

	/**
	 * Devuelve el valor de un campo.
	 * @param  string $name    Nombre del campo.
	 * @param  mixed  $default Valor por defecto si este no está definido.
	 * @return mixed
	 */
	public function get($name,$default=null);

	/**
	 * Establece el valor de un campo en la instancia.
	 * @param   string $name  Nombre del campo.
	 * @param   mixed  $value Valor del campo.
	 * @return  Api\OptionsInterface
	 */
	public function set($name,$value);

	/**
	 * Elimina un campo de la instancia.
	 * @param  string $name Nombre del campo.
	 * @return Api\OptionsInterface
	 */
	public function delete($name);
	/**
	 * Importa los valores a la instancia.
	 * @param  array  $values Valores a importar.
	 * @return Api\OptionsInterface
	 */
	public function import($values);

	/**
	 * Devuelve todos los campos de la instancia en una matriz unidimensional.
	 * @return array
	 */
	public function all();

	/**
	 * Devuelve todos los campos de la instancia en una matriz bidimensional.
	 * @return array
	 */
	public function toArray();

	/**
	 * Agrega un callback para un evento.
	 * @param  string  $name     Nombre del evento.
	 * @param  Closure $callback Callback a ejecutar.
	 * @return Api\OptionsInterface
	 */
	public function on($name,Closure $callback);
}