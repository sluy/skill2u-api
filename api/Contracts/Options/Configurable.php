<?php namespace Api\Contracts\Options;
/**
 * Interface para objetos configurables.
 */
interface Configurable{
	/**
	 * Devuelve la instancia de las opciones.
	 * @return Api\Option
	 */
	public function getOptions();
}