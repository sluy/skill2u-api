<?php namespace Api\Contracts;

interface Setterable{
	public function setProperty($name,$value);
}