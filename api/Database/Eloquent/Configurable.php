<?php namespace Api\Database\Eloquent;
use Api\Options,
		Api\Options\ConfigurableInterface,
		Exception;
/**
 * Trait para modelos con opciones;
 */
trait Configurable {
	/**
	 * Instancia de las opciones.
	 * @var Api\Options|null
	 */
	protected $_options;

	/**
	 * Devuelve la instancia de las opciones.
	 * @return Api\Options
	 */
	public function getOptions($name=null){
		if(!$this->_options){
			//Usamos el nombre de la opción para permitir el cambio de nombre de la propiedad.
			$field = isset($this->optionsField) ? $this->optionsField : 'options';
			$this->_options = new Options(
				isset($this->attributes[$field]) ? $this->attributes[$field] : null,
				isset($this->fillableOptions) ? $this->fillableOptions : null,
				isset($this->castsOptions) ? $this->castsOptions : null,
				true
			);
			$this->attributes[$field] = json_encode($this->_options->toArray(),true);
			$this->_options->on('change',function() use($field){
				$this->attributes[$field] = json_encode($this->_options->toArray(),true);
			});
		}
		if(is_array($name)){
			$values = [];
			foreach($name as $n){
				$values[$n] = $this->_options->get($name);
			}
			return $values;
		}
		return $name !== null ? $this->_options->get($name) : $this->_options;
	}
	/**
	 * Devuelve la instancia de las opciones.
	 * @return Api\Options
	 */
	public function getOptionsAttribute(){
		return $this->getOptions()->toArray();
	}
	/**
	 * Importa los valores a las opciones.
	 * @param   array $values 
	 * @return  void
	 */
	public function setOptionsAttribute($values){
		$this->getOptions()->import($values);
	}
}