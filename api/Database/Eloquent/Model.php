<?php namespace Api\Database\Eloquent;
use Illuminate\Database\Eloquent\Model as Base,
		Illuminate\Database\Eloquent\Relations\HasMany,
		Illuminate\Database\Eloquent\Relations\HasOne,
		ReflectionClass,
		Exception,
		Api\Format;
/**
 * Modelo base del sistema.
 */
class Model extends Base{

	/**
	 * Nombre de los métodos que devuelven queries con las tablas relacionadas.
	 * Usado para eliminar registros relacionados antes de eliminar el registro actual.
	 * @var array
	 */
	public $rels = [];

	public $crossAlias = null;

	public $crossRelModel = null;

	public $crossParentModel = null;

	public $crossType = 'one';

	/**
	 * Método ejecutado al inicio del modelo.
	 * @return void
	 */
	protected static function boot(){
		parent::boot();
		/*
		static::deleting(function($model){
			if(is_array($model->rels)){
				foreach($model->rels as $rel){
	      	$model->$rel()->delete();
	      }
			}
    });*/
	}

	public function getTable(){
		if(property_exists($this, 'table') && !empty($this->table)){
			return $this->table;
		}
		$table = get_class($this);
		if(starts_with(strtolower($table),'app\models\\')){
			$table = substr($table,11);
		}
		$table = str_replace('\\', '_', $table);
		return strtolower(Format::cast('pluralize',$table));
	}

	public function hasCross(){
		return $this->getCrossRel() && $this->getCrossParent();
	}

	public function isOneCross(){
		return $this->hasCross() && $this->crossType !== 'many';
	}

	public function getCross($type,$id=null){
		$tmp = explode('_',$this->getTable());
		$count = count($tmp);
		//No hay modelos cruzados
		if($count < 2){
			return null;
		}
		$model = null;
		$alias = null;
		$name  = Format::cast('singularize',$tmp[$count-($type=='rel'?1:2)]);
		$prop  = 'cross'.ucfirst($type);

		if(isset($this->{$prop.'Alias'})){
			$alias = $this->{$prop.'Alias'};
		}else{
			$alias = $name;
		}
		if(isset($this->{$prop.'Model'})){
			$model = $this->{$prop.'Model'};
		}else{
			$model = 'App\\Models\\' . ucfirst($name);
		}
		if(is_string($model)){
			$model = new $model;
		}
		if($model instanceof Model){
			if($id !== null){
				$model = $model->newQuery()->find($id);
				if(!$model){
					return null;
				}
			}
			$model->crossAlias = $alias;
			return $model;
		}
	}

	public function getCrossType(){
		return $this->type == 'many'
					 ? 'many'
					 : 'one';
	}

	public function getCrossName(){
		return isset($this->crossAlias)
					 ? $this->crossAlias
					 : @Format::cast('singularize',end(explode('_',$this->getTable())));
	}

	public function getCrossKeyName(){
		return $this->getCrossName() . '_' . $this->getKeyName();
	}

	public function hasCrossParent(){
		return $this->getCross('parent') !== null;
	}

	public function hasCrossRel(){
		return $this->getCross('rel') !== null;
	}

	public function getCrossParent($id=null){
		return $this->getCross('parent',$id);
	}

	public function getCrossRel($id=null){
		return $this->getCross('rel',$id);
	}
}
