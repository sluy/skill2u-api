<?php namespace Api;
use Traversable;

class Format {

	protected static $aliases = [
		'int'     => 'integer',
		'str'     => 'string',
		'bool'    => 'boolean',
		'decimal' => 'float',
		'arr'     => 'array',
	];

	public static function __callStatic($name,$attrs){
		return call_user_func_array(get_called_class().'::cast', $attrs);
	}

	public static function cast($name,$value){
		$ci = strtolower($name);
		$attrs = func_get_args();
		array_shift($attrs);
		$attrs = array_values($attrs);
		if(method_exists(get_called_class(), $name)){
			return call_user_func_array(get_called_class().'::'.$name, $attrs);
		}
		return (in_array($name, static::$aliases))
					 ? call_user_func_array(get_called_class().'::'.static::$aliases[$name],$attrs)
					 : $value;
	}

	public static function array($value){
		if(is_object($value)){
			if(method_exists($value,'toArray')){
				$value = $value->toArray();
			}else if($value instanceof Traversable){
				$tmp = [];
				foreach($value as $key => $v){
					$tmp[$key] = $v;
				}
				$value = $tmp;
			}
		}else if(is_string($value)){
			$json = @json_decode($value);
			if(is_array($json)){
				$value = $json;
			}
		}
		return is_array($value) ? $value : [];
	}

	public static function string($value){
		if(is_numeric($value)){
			$value = strval($value);
		}
		return is_array($value) || is_object($value) ? '' :$value;
	}

	public static function integer($value){
		if(is_string($value) || is_float($value)){
			$value = intval($value);
		}
		return is_array($value) || is_object($value) ? 0 :$value;
	}

	public static function float($value){
		if(is_string($value) || is_int($value)){
			$value = floatval($value);
		}
		return is_array($value) || is_object($value) ? floatval(0) :$value;
	}

	public static function singularize($value){
		$value = static::cast('string',$value);
		$len = strlen($value);
		if($len > 1 && substr($value,-1) == 's'){
			if($len > 3 && substr($value, -3) == 'ies'){
				return substr($value, 0,-3) . 'y';
			}else{
				return substr($value,0,-1);
			}
		}
		return $value;
	}

	public static function pluralize($value){
		$value = static::cast('string',$value);
		$len = strlen($value);
		if($len > 1){
			$last = strtolower(substr($value,-1));
			if($last == 'y' || $last != 's'){
				return $last == 'y'
							 ? substr($value,0,-1) . 'ies'
							 : $value . 's';
			}
		}
		return $value;
	}



	public static function boolean($value){
		if(is_string($value)){
			$value = in_array(strtolower($value),['1','true','t','yes','y','allow','allowed','accept']);
		}
		if(is_numeric($value)){
			$value = $value == 1;
		}
		return is_bool($value) ? $value : false;
	}
}