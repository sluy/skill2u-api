<?php
namespace Api\Http\Middleware;
use Closure,
		Api\Api;
/**
 * Middleware para comprobar que el usuario autenticado sea un administrador
 * del sistema.
 */
abstract class ApiMiddleware {
  /**
   * Instancia de la api.
   *
   * @var \Api\Api 
   */
  protected $api;


  /**
   * Crea la instancia del middleware
   *
   * @param  \Api\Api  $api
   * @return void
   */
	public function __construct(Api $api){
		$this->api = $api;
	}
  /**
   * Maneja la petición entrante.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \Closure                  $next
   * @param  string|null               $guard
   * @return mixed
   */
  abstract public function handle($request, Closure $next, $guard = null);
}