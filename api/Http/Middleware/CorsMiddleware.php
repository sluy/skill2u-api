<?php namespace Api\Http\Middleware;
class CorsMiddleware {
  public function handle($request, \Closure $next)
  {
    $response = $next($request);
    if($request->getMethod() === 'OPTIONS') {
      return response('');
    }
    return $response;
  }
}