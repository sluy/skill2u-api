<?php namespace Api\Http\Middleware;

class JsonRequest {
  public function handle($request, \Closure $next)
  {
   	$all = $request->all();
   	foreach($all as $k => $v){
   		$test = @json_decode($v,true);
   		if(is_array($test)){
   			$all[$k] = $test;
   		}
   	}
   	$request->replace($all);
    return $next($request);
  }
}