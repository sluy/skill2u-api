<?php namespace Api;
use Closure,
		Api\Contracts\Options as OptionsContract,
		stdClass,
		Traversable;
/**
 * Clase para el manejo de opciones.
 */
class Options implements OptionsContract {
	/**
	 * Valores.
	 * @var array
	 */
	protected $_values = [];
	/**
	 * Campos exclusivos.
	 * @var array
	 */
	protected $_fields = [];
	/**
	 * Tipos de datos.
	 * @var array
	 */
	protected $_types = [];
	/**
	 * Callbacks de los eventos.
	 * @var array
	 */
	protected $_callbacks = [];

	/**
	 * Determina si las opciones estarán bloqueadas para la escritura.
	 * @var boolean
	 */
	protected $_locked = false;

	protected $dump = false;

	/**
	 * Constructor.
	 * Inicializa la instancia.
	 * @param array|null $values Valores de la instancia.
	 * @param array|null $fields Campos válidos.
	 * @param array|null $types  Tipos de datos.
	 */
	public function __construct($values = null,array $fields=null,array $types=null,$dump=false){
		$this->dump = $dump;
		$this->setFields($fields ? $fields : []);
		$this->setTypes($types ? $types : []);
		$this->import($values ? $values : []);
	}

	/**
	 * Método mágico.
	 * Devuelve el valor de un campo de la instancia.
	 * @param  string $name    Nombre del campo.
	 * @return mixed
	 */
	public function __get($name){
		return $this->get($name);
	}

	/**
	 * Método mágico.
	 * Establece el valor de un campo de la instancia.
	 * @param  string $name    Nombre del campo.
	 * @param  string $value   Valor del campo.
	 * @return void
	 */
	public function _set($name,$value){
		$this->set($name,$value);
	}

	/**
	 * Bloquea la escritura de datos en la instancia.
	 * @return Api\Options
	 */
	public function lock(){
		$this->_locked = true;
		return $this;
	}
	
	/**
	 * Determina si la instancia está bloqueada para la escritura de datos.
	 * @return boolean
	 */
	public function isLocked(){
		return $this->_locked;
	}

	/**
	 * Determina si un campo es permitido dentro de la instancia.
	 * @param  string  $name Nombre del campo a comprobar.
	 * @return boolean
	 */
	public function isAllowed($name){
		return !$this->_locked && (empty($this->_fields) || in_array($name, $this->_fields));
	}

	/**
	 * Determina si existe un campo en la instancia.
	 * @param  string  $name Nombre del campo.
	 * @return boolean
	 */
	public function has($name){
		return array_key_exists(strtolower($name), $this->_fields);
	}
	/**
	 * Devuelve el valor de un campo.
	 * @param  string $name    Nombre del campo.
	 * @param  mixed  $default Valor por defecto si este no está definido.
	 * @return mixed
	 */
	public function get($name,$default=null,$cast=null){
		$value = [];
		$name = strtolower($name);
		$length = strlen($name);

		foreach($this->_values as $key => $v){
			if($name === $key){
				$value = $v;
				break;
			}
			if(strlen($key) > $length && substr($key, 0 , $length) === $name){
				$value[trim(substr($key,$length),'.')] = $v;
			}
		}
		if(is_array($value) && empty($value)){
			$value = null;
		}
		return ($value === null)  
					 ? $this->format($name,$default) 
					 : $value;
	}

	/**
	 * Establece el valor de un campo en la instancia.
	 * @param   string $name  Nombre del campo.
	 * @param   mixed  $value Valor del campo.
	 * @return  Api\Options
	 */
	public function set($name,$value){
		$name = strtolower($name);
		if($this->isAllowed($name)){
			if(is_array($value)){
				foreach($this->dotCase($value,$name) as $k => $v){
					$this->set($k,$v);
				}
			}else{
				$this->_values[$name] = $this->format($name,$value);
				$this->trigger('update',$name,$this->_values[$name]);
			}
		}
		return $this;
	}

	/**
	 * Cambia los arrays multidimensionales por arrays simples con 
	 * los nombres separados por puntos.
	 * @param  array|object  $a      Objeto iterable a dar formato.
	 * @param  array         $parent Nombre padre de la matriz.
	 * @return array
	 */
	protected function fixedDotCase($a,$parent=null){
		$dot = [];
		if(is_iterable($a)){
			foreach($a as $k => $v){
				if(is_string($parent) || is_int($parent)){
					$k = strtolower($parent.'.'.$k);
				}
				if(is_iterable($v)){
					$dot = array_merge($dot,$this->dotCase($v,$k));
				}else{
					$dot[$k] = $v;
				}
			}
		}
		return $dot;
	}


	/**
	 * Elimina un campo de la instancia.
	 * @param  string $name Nombre del campo.
	 * @return Api\Options
	 */
	public function delete($name){
		$name = strtolower($name);
		if(array_key_exists($name, $this->_fields)){
			$deleted = $this->_fields[$name];
			unset($this->_fields[$name]);
			$this->trigger('delete',$name,$deleted);
		}
		return $this;
	}

	/**
	 * Devuelve todos los campos de la instancia en una matriz unidimensional.
	 * @return array
	 */
	public function all(){
		return $this->_values;
	}
	/**
	 * Devuelve todos los campos de la instancia en una matriz bidimensional.
	 * @return array
	 */
	public function toArray(){
		return $this->dotToArray($this->_values);
	}

	/**
	 * Agrega un callback para un evento.
	 * @param  string  $name     Nombre del evento.
	 * @param  Closure $callback Callback a ejecutar.
	 * @return Api\Options
	 */
	public function on($name,Closure $callback){
		$name = strtolower($name);
		if(!isset($this->_callbacks[$name])){
			$this->_callbacks[$name] = [];
		}
		$this->_callbacks[$name][] = $callback;
	}

	/**
	 * Ejecuta los callbacks asociados a un evento.
	 * @param  string  $name Nombre del evento.
	 * @return Api\Options
	 */
	protected function trigger($name){
		$name = strtolower($name);
		$params = func_get_args();
		unset($params[0]);
		$params = array_values($params);
		$params[] = $name;
		$params[] = $this;
		if(isset($this->_callbacks[$name])){
			foreach($this->_callbacks[$name] as $callback){
				call_user_func_array($callback, $params);
			}
		}
		if($name == 'update' || $name == 'delete'){
			$this->trigger('change',$name);
		}
	}

	/**
	 * Convierte del formato de puntos a una matriz multidimensional.
	 * @param  array  $data  Datos originales.
	 * @return array
	 */
	protected function dotToArray($data){
		$arr = [];
		foreach($data as $dotKey => $value){
			$sections = explode('.',$dotKey);
			$current = array_shift($sections);
			if(count($sections) > 0){
				if(!isset($arr[$current]) || !is_array($arr[$current])){
					$arr[$current] = [];
				}
				$arr[$current] = array_merge_recursive($arr[$current],
																		 $this->dotToArray([implode('.',$sections) => $value]));
			}else{
				$arr[$current] = $value;
			}
		}
		return $arr;
	}

	/**
	 * Establece los tipos de datos de los valores.
	 * @param  array $types Matriz con el par nombre_campo=>tipo_dato
	 * @return void
	 */
	protected function setTypes(array $types){
		$this->_types = $this->dotCase($types);
	}
	/**
	 * Establece los campos permitidos en la instancia.
	 * @param  array $fields Matriz cuyo valor de cada registro será el nombre del campo.
	 * @return void
	 */
	protected function setFields(array $fields){
		$this->_fields = array_map('strtolower', $fields);
		$this->_fields = array_change_key_case($this->_fields);
	}


	/**
	 * Importa los valores a la instancia.
	 * @param  array  $values Valores a importar.
	 * @return Api\Options
	 */
	public function import($values){
		if(is_string($values)){
			$values = @json_decode($values);
		}
		if($values instanceof OptionsInterface){
			$values = $values->toArray();
		}
		foreach($this->dotCase($values) as $name => $value){
			$this->set($name,$value);
		}
		return $this;
	}


	/**
	 * Cambia los arrays multidimensionales por arrays simples con 
	 * los nombres separados por puntos.
	 * @param  array|object  $a      Objeto iterable a dar formato.
	 * @param  array         $parent Nombre padre de la matriz.
	 * @return array
	 */
	protected function dotCase($a,$parent=null){
		$dot = [];
		if(is_array($a) || $a instanceof Traversable || $a instanceof stdClass){
			foreach($a as $k => $v){
				if(is_string($parent) || is_int($parent)){
					$k = strtolower($parent.'.'.$k);
				}
				if(is_iterable($v)){
					$dot = array_merge($dot,$this->dotCase($v,$k));
				}else{
					$dot[$k] = $v;
				}
			}
		}
		return $dot;
	}

	/**
	 * Da formato al tipo de datos de un valor.
	 * @param  mixed  $type  Tipo de datos.
	 * @param  mixed  $value Valor a dar formato.
	 * @return mixed
	 */
	protected function format($name,$value,$cast=null){
		if(empty($cast) || !is_string($cast)){
			$cast = isset($this->_types[$name]) 
						  ?$this->_types[$name]
						  : null;
		}	
		return ($cast !== null)
					 ? Format::cast($cast,$value)
					 : $value;
	}
}