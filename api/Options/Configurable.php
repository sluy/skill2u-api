<?php namespace Api\Options;
use Closure;
use Api\Options;
/**
 * Trait para objetos configurables.
 */
trait Configurable {
	/**
	 * Instancia de las opciones
	 * @var Api\Options|nul
	 */
	protected $_options;

	/**
	 * Devuelve las opciones de la instancia.
	 * @return Api\Options
	 */
	public function getOptions(){
		if(!$this->_options){
			$this->_options = new Options(
				isset($this->defaultOptions) ? $this->defaultOptions : null,
				isset($this->fillableOptions) ? $this->fillableOptions : null,
				isset($this->castOptions) ? $this->castOptions : null
			);
			if(isset($this->lockOptions) && $this->lockOptions === true){
				$this->_options->lock();
			}
		}
		return $this->_options;
	}
}