<?php namespace Api\Providers;

use Illuminate\Support\ServiceProvider;
use Api\Api;

class ApiServiceProvider extends ServiceProvider
{
	public function register(){
		$this->app->singleton(Api::class,function($app){
			return new Api();
		});
	}

  /**
   * Register the application's response macros.
   *
   * @return void
   */
  public function boot()
  {
  }
}