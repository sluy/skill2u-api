<?php namespace Api\Routing;
use Laravel\Lumen\Routing\Controller as Base,
		Api\Routing\Controller\Contracts\Controller as Contract,
		Api\Routing\Controller\Api,
		Api\Traits\Eventable,
		Api\Routing\Controller\Request,
		Api\Routing\Controller\Response,
		Api\Routing\Controller\Route,
		Api\Routing\Controller\Validation,
		Api\Format,
		Exception;
/**
 * Controlador base del sistema.
 */
class Controller extends Base implements Contract 
{
	use Api,
			Eventable,
			Request,
			Response,
			Route,
			Validation;

	/**
	 * Método mágico.
	 * Habilita la obtención de módulos de la API cómo propiedades de la 
	 * instancia actual.
	 * @param  string $name Nombre del módulo.
	 * @return App\Api\Module
	 * @throws Exception    Si el método no existe en el controlador.
	 */
	public function __get($name){
		if($this->api()->has($name)){
			return $this->api()->get($name);
		}
		throw new Exception('No existe la propiedad '.$name.
												' en el controlador ' . get_class($this), 1);
	}

	/**
	 * Método mágico.
	 * Habilita la definición y obtención de reglas de validación cómo métodos
	 * check{escenario}.
	 * @param  string $name  Nombre del método.
	 * @param  array  $attrs Atributos a suministrar.
	 * @return array
	 * @throws Exception    Si el método no existe en el controlador.
	 */
	public function __call($name,$attrs){
		if(strlen($name) > 5 && substr($name, 0,5) == 'check'){
			array_unshift($attrs, lcfirst(substr($name, 5)));
			return call_user_func_array([$this,'check'], $attrs);
		}
		throw new Exception('No existe el método '.$name.
												' en el controlador ' . get_class($this), 1);
	}

	public function user(){
		return $this->auth->user();
	}

	public function company(){
		return ($this->auth->session()) ? $this->auth->user()->company : null;
	}

	public function cast($type,$value){
		return Format::cast($type,$value);
	}
}