<?php namespace Api\Routing\Controller;

trait Api {	
	/**
	 * Devuelve la instancia de un módulo de la api.
	 * @param  [type] $module Nombre del módulo. Si no es especificado
	 *                        devolverá la instancia de la API.
	 * @return Api\Api|Api\Api\Module
	 */
	public function api($module=null){
		return $module === null 
					 ? app('Api\Api')
					 : app('Api\Api')->get($module);
	}
}