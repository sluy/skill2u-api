<?php namespace Api\Routing\Controller;
use Api\Routing\Controller,
		Illuminate\Database\Eloquent\Model,
		Illuminate\Http\Response,
		Symfony\Component\HttpFoundation\File\UploadedFile,
		Api\Routing\Controller\Contracts\Validation,
		Api\Format,
		Illuminate\Database\Eloquent\Builder,
		Closure,
		Exception;
/**
 * Provee de métodos para la creación edición y eliminación de registros en la base de
 * datos tomando los mismos directamente de la petición actual.
 */
class CRUD extends Controller {

	public $crossLookup = true;

	public $forceKeySearch = true;

	public $conditions = [];

	public $guard = [];

	public $defaults = [];

	public $nullable = [];

	public $statics = [];

	public $morph = [];

	public $with = [];

	public $model;

	public $search;

	public $searchField;

	public $query;

	public $return;

	public $trashed;

	public $all;

	public $single;

	public $callback;

	public $location;

	public $groupLocation;

	public $appends;

	protected $_customFilters=[];

	protected $_customOrders=[];

	protected $_filterFormatters=[];


	public function __construct(){
		if($this->crossLookup){
			if($this->model()->hasCrossParent()){
				$parent = $this->model()->getCrossParent()->find($this->route(0,0,'integer'));
				if(!$parent){
					$this->notFound();
				}
				$this->conditions[$this->model()->getCrossParent()->getCrossKeyName()] = $parent->id;
			}
			if($this->model()->hasCrossRel()){
				$this->searchField = $this->model()->getCrossRel()->getCrossKeyName();
			}else{
				$this->searchField = $this->model()->getKeyName();
			}
		}
		if(method_exists($this, 'boot')){
			$this->boot();
		}
	}

	/**
	 * Devuelve la instancia de un modelo.
	 *
	 * @param  string|null   $model Instancia o clase del modelo a devolver. Si no es especificado
	 *                              devolverá el modelo actual.
	 * @param  integer|null  $id    Valor del campo primario a buscar.
	 * @return Illuminate\Database\Eloquent\Model
	 * @throws Exception   Si el modelo no se pudo resolver.
	 */
	public function model($model=null,$id=null){
		if($model === null){
			if(property_exists($this, 'model') && !empty($this->model)){
				$model = $this->model;
			}else{
				$tmp = get_class($this);
				//Eliminamos xxxxxController
				if(ends_with(strtolower($tmp),'controller')){
					$tmp = substr($tmp, 0,-10);
				}
				$tmp = explode('\\',$tmp);
				//Eliminamos App\Http\Controllers\{module}
				if($tmp > 4){
					for($n=0;$n<4;$n++){
						array_shift($tmp);
					}
					foreach($tmp as $k => $item){
						$tmp[$k] = $this->cast('singularize',$item);
					}
					$model = 'App\\Models\\' . implode('\\',$tmp);
				}
			}
		}
		if(is_string($model) && !empty($model)){
			$model = new $model;
		}
		if($model instanceof Model){
			return empty($id) ? $model : $model->newQuery()->find($id);
		}
		throw new Exception("No se pudo resolver el modelo en " . get_class($this));
	}


	public function find($id=null,$cfg=null){
		extract($this->cfg($cfg,['search'=>$id]));
		$searchField = !$searchField ? $model->getKeyName() : $searchField;
		$queryItems = [];
		if($conditions){
			foreach($conditions as $field => $value){
				$queryItems[$field] = $value;
			}
		}
		if($search && !array_key_exists($searchField, $queryItems)){
			$queryItems[$searchField] = $search;
		}
		if($queryItems){
			foreach($queryItems as $field => $value){
				$query->{is_array($value) ? 'whereIn' : 'where'}($field,$value);
			}
		}else if($this->forceKeySearch){
			$query->where($model->getKeyName(),$model->{$model->getKeyName()});
		}
		if($trashed && can_trash($model)){
			$query->withTrashed();
		}
		if($with){
			$query->with($with);
		}

		if ($appends) {
			$query->appends($appends);
		}

		if($callback && $callback instanceof Closure){
			$callback($query,$cfg,$this);
		}

		if($return == 'query'){
			return $query;
		}
		$model = $query->first();

		if(!isset($cfg['silent']) || $cfg['silent'] !== true){
			$this->trigger('find',['model'=>$model],'after');
			if($model){
				$this->trigger('found',['model'=>$model],'after');
			}
		}
		return $this->formatReturn($return,'read', $this->discrimineFields($model));
	}

	protected function hasCustomOrder($name){
		foreach(func_get_args() as $name){
			$name = camel_case($name);
			if(!isset($this->_customOrders[$name]) && !method_exists($this, 'orderBy'.$name)){
				return false;
			}
		}
		return true;
	}

	protected function getCustomOrder($name){
		$name = camel_case($name);
		if(isset($this->_customOrders[$name])){
			return $this->_customOrders[$name];
		}
		if(method_exists($this, 'orderBy'.$name)){
			$method = 'orderBy'.$name;
			return function($items,$type='asc') use($method){
				return $this->$method($items,$type);
			};
		}
		return null;
	}

	protected function setCustomOrder($name,$callback){
		$name = camel_case($name);
		$this->_customOrders[$name] = $callback;
		return $this;
	}

	protected function callCustomOrder($name,$items,$type='asc'){
		$order = $this->getCustomOrder($name);
		return ($order)
					 ? $order($items,$type)
					 : null;
	}

	protected function hasCustomFilter($name){
		foreach(func_get_args() as $name){
			$name = camel_case($name);
			if(!isset($this->_customFilters[$name]) && !method_exists($this, 'filterBy'.$name)){
				return false;
			}
		}
		return true;
	}

	protected function getCustomFilter($name){
		$name = camel_case($name);
		if(isset($this->_customFilters[$name])){
			return $this->_customFilters[$name];
		}
		if(method_exists($this, 'filterBy'.$name)){
			$method = 'filterBy'.$name;
			return function(&$query,$values,$type,$all=null) use($method){
				return $this->$method($query,$values,$type,$all);
			};
		}
		return null;
	}

	protected function setCustomFilter($name,Closure $callback){
		$this->_customFilters[camel_case($name)] = $callback;
		return $this;
	}

	public function callCustomFilter($name,&$query,$values,$type,$all=null){
		$filter = $this->getCustomFilter($name);
		return isset($filter)
					 ? $filter($query,$values,$type,$all)
					 : null;
	}

	protected function hasInputFilter($name){
		$filters = $this->getInputFilters();
		foreach(func_get_args() as $name){
			if(is_scalar($name) && array_key_exists($name, $filters)){
				return true;
			}
		}
		return false;
	}

	protected function getInputLimit(){
		$limit = intval(app('request')->input('limit'));
		if($limit < 2){
			$limit = 10;
		}
		if($limit > 100){
			$limit = 100;
		}
		return $limit;
	}

	protected function getInputOrder(){
		$order = $this->request()->has('order_by')
						 ? trim($this->input('order_by',''))
						 : '';
		if(!$order){
			return '';
		}
		$desc  = $this->request()->has('order_inverse')
						 ? strtolower(trim($this->input('order_inverse','')))
						 : '';
		$desc = (empty($desc) || $desc == 'true' || $desc == 'yes')
						? true
						: false;
		return [$order,$desc?'desc':'asc'];
	}

	protected function hasFilterFormatter($name){
		foreach(func_get_args() as $name){
			$name = camel_case($name);
			if(!isset($this->_filterFormatters[$name]) &&
				 !method_exists($this,$name.'FilterFormatter')){
				return false;
			}
		}
		return true;
	}

	protected function getFilterFormatter($name){
		$name = camel_case($name);
		if(isset($this->_filterFormatters[$name])){
			return $this->_filterFormatters[$name];
		}
		if(method_exists($this,$name.'FilterFormatter')){
			$method = $name.'FilterFormatter';
			return function($value,$literal) use($method){
				return $this->{$method}($value,$literal);
			};
		}
		return null;
	}

	protected function setFilterFormatter($name,$callback){
		$name = camel_case($name);
		$this->_filterFormatters[$name] = $callback;
		return $this;
	}

	protected function callFilterFormatter($name,$values,$literal){
		$formatter = $this->getFilterFormatter($name);
		return ($formatter)
					 ? $formatter($values,$literal)
					 : null;
	}


	protected function getInputFilters(){
		$filters = [];
		if($this->request()->has('search_fields')){
			$fields = array_map('trim',explode(',',$this->input('search_fields','')));
			$values = array_map('trim',explode(',',$this->input('search_values','')));
			foreach($fields as $k => $name){
				if(!empty($name) && array_key_exists($k, $values)){
					$literal = false;
					$vals = trim($values[$k]);
					if(empty($vals)){
						$vals = [];
					}else if(strpos($vals, '|')!==false){
						$vals = array_map('trim',explode('|',$vals));
					}else{
						$vals = [$vals];
					}
					if(substr($name, -1) == '*'){
						$literal = true;
						$name = substr($name, 0,-1);
					}
					$filters[$name] = ['value'=>$vals,'literal'=>$literal];

					if($this->hasFilterFormatter($name)){
						$tmp = $this->callFilterFormatter($name,$vals,$literal);
						if(!$tmp){
							unset($filters[$name]);
						}else{
							$filters[$name]['value'] = $tmp;
						}
					}
				}
			}
		}
		return $filters;
	}

	protected function dateRangeFilterFormatter($values){
		if(is_array($values) && count($values) === 2){
			return $values;
		}
		return null;
	}

	protected function filterByDateRange(&$query,$values,$type,$all){
		return $query;
	}


	protected function filterByPeriodRange(&$query,$values,$type,$all){
		return $this->filterByDateRange($query,$values,$type,$all);
	}

	protected function periodRangeFilterFormatter($values){
		$performance = $values[0];
		if($performance === 'current'){
			return $this->company->getPeriodRange();
		}
		$performance = $this->company()->performances()->find($performance);
		return ($performance)
					 ? [$performance->start_at,$performance->finish_at]
					 : [0,0];
	}

	protected function customPagination($pagination, $items){
		$pagination = $pagination->toArray();

		$pageItems = [];
		$max = ($pagination['per_page'] * $pagination['current_page']) - 1;
		$min = ($pagination['per_page'] * $pagination['current_page']) - $pagination['per_page'];

		for($n = $min;$n<=$max;$n++){
			if(!array_key_exists($n, $items)){
				break;
			}
			$pageItems[] = $items[$n];
		}
		$pagination['data'] = $pageItems;
		return $pagination;
	}


	protected function raiseRels($rels, $model) {
		if($model instanceof \Illuminate\Database\Eloquent\Collection) {
			foreach($model as $m) {
				$this->raiseRels($rels,$m);
			}
			return;
		}

		foreach($rels as $rel) {
			$tmp = explode('.',$rel);
			$current = array_shift($tmp);
			$model->{$current};
			if(count($tmp) > 0 && $model->{$current}) {
				$this->raiseRels($tmp, $model->{$current});
			}
		}
	}

	protected function applyFilters($raw,array $with=[]){
		if($raw instanceof Builder){
			$raw = $raw->get();
		}
		if(!$raw){
			return $raw;
		}
		$data = [];
		$filters = $this->getInputFilters();
		foreach($raw as $k => $v){
			$filtered = $v->setAppliedFilters($filters);
			$this->raiseRels($with,$v);
			$data[] = $this->discrimineFields($filtered);
		}
		return $data;
	}


		/**
	 * Aplica los filtros de campos definidos en el request.
	 *
	 * @param  mixed $model
	 * @return void
	 */
	protected function discrimineFields($model) {
		extract($this->getModelFieldsFromRequest());
		if(!$except && !$only || (!is_array($model) && !($model instanceof Model))) {
			return $model;
		}
		$data = [];
		if($except) {
			foreach($except as $field) {
				$data = $model->toArray();
				if(array_key_exists($field, $data)) {
					unset($data[$field]);
				}
			}
		} else if ($only) {
			foreach($only as $field) {
				if (str_contains($field, '.')) {
					$tmp = $model;
					foreach(explode('.', $field) as $f) {
						$tmp->{$f};
						$tmp = $tmp->{$f};
					}
					$data[$field] = $tmp;
				} else {
					$data[$field] = $model->{$field};
				}
			}
		} else if ($appends) {
			$data = $model->toArray();
			foreach($appends as $field) {
				if (str_contains($field, '.')) {
					$tmp = $model;
					foreach(explode('.', $field) as $f) {
						$tmp->{$f};
						$tmp = $tmp->{$f};
					}
					$data[$field] = $tmp;
				} else {
					$data[$field] = $model->{$field};
				}
			}
		} else {
			$data = $model->toArray();
		}
		return $data;
	}


	/**
	 * Pagina una consulta.
	 * @param   $query Query del modelo.
	 * @return
	 */
	public function paginate($cfg=null){
		$cfg   = $this->cfg($cfg);
		foreach(['all','trashed','single'] as $field){
			if($this->request()->has($field) &&
				 ((is_string($this->input($field)) && strlen($this->input($field)) == 0) ||
				 $this->cast('boolean', $this->input($field)) === true)){
				$cfg[$field] = true;
			}
		}
		extract($cfg);
		$cfg['return'] = 'query';
		$query = $this->find(null,array_merge($cfg,['silent'=>true]));

		$filters = $this->getInputFilters();



		if(!empty($filters)){
			foreach($filters as $field => $opts){
				if($this->hasCustomFilter($field)){
					continue;
				}
				//literales (no like)
				if($opts['literal']){
					if(is_array($opts['value'])){
						$query->whereIn($field,$opts['value']);
					}else{
						$query->where($field,$value);
					}
				}
				else {
					if(is_array($opts['value'])){
						$query->where(function($q) use ($field, $opts) {
							foreach($opts['value'] as $v){
								$q->orWhere($field,'like','%'.$v.'%');
							}
						});
					}else{
						$query->where($field,'like','%'.$tmp['value'].'%');
					}
				}
			}
			foreach($filters as $filter => $opts){
				if($this->hasCustomFilter($filter)){
					$ranged = $this->applyFilters($query,$with);
					$this->callCustomFilter($filter,$query,$opts['value'],$opts['literal'],$ranged);
				}
			}
		}
		$order = $this->getInputOrder();
		if($order){
			if($this->hasCustomOrder($order[0])){
				$items = $this->{'orderBy'.$order[0]}($this->applyFilters($query->get(),$with),$order[1]);
				if($all){
					return $this->formatReturn('model','read',$items);
				}
				$paginated = $query->paginate($this->getInputLimit());
				$paginated = $this->customPagination($paginated,$items);
				if($return == 'model'){
					return $paginated;
				}
				return $this->formatReturn($return,
																	 'read',
																	 $single === true
																	 ?$paginated['data']
																	 :$paginated);
			}else{
				$query->orderBy($order[0],$order[1]);
			}
		}
		$limit = $this->getInputLimit();
		if($all){
			return $this->formatReturn('model','read',$this->applyFilters($query->get(),$with));
		}
		$paginated = $query->paginate($limit);
		$paginatedArray = $paginated->toArray();
		$paginatedArray['data'] = $this->applyFilters($paginated->items(),$with);

		if($return == 'model'){
			return $paginatedArray;
		}

		//Devolvemos la paginación
		return $this->formatReturn($return,'read',$single === true
															? $paginatedArray['data']:$paginatedArray);
	}
	/**
	 * Aplica los filtros de campos definidos en el request.
	 *
	 * @param  mixed $model
	 * @return void
	 */
	protected function applyOnlyFields($model) {
		extract($this->getModelFieldsFromRequest());
		if(!$except && !$only) {
			return $model;
		}
		if($model instanceof Model) {
			$model = $model->toArray();
		}
		if($except) {
			foreach($except as $field) {
				if(array_key_exists($model[$field])) {
					unset($model[$field]);
				}
			}
			return $model;
		}
		$data = [];
		foreach($only as $field) {
			$data = array_key_exists($field,$model) ? $model[$field] : null;
		}
		return $data;
	}


	/**
	 * Devuelve los campos a discriminar en los modelos.
	 *
	 * @return array
	 */
	protected function getModelFieldsFromRequest() {
		$data = [];
		foreach(['only', 'except', 'appends'] as $type) {
			$value = $this->request()->input($type);
			$formatted = [];
			if (is_string($value) && !empty($value)) {
				foreach(array_map('trim', explode(',',$value)) as $item) {
					if(is_string($item) && !empty($item)) {
						$formatted[] = $item;
					}
				}
			}
			$data[$type] = $formatted;
		}
		return $data;
	}
	/**
	 * Crea un nuevo registro.
	 * @return Illuminate\Http\Response|Illuminate\Database\Eloquent\Model
	 */
	public function create($cfg=null){
		return $this->process('create',$cfg);
	}
	/**
	 * Edita un registro existente.
	 * @param  Illuminate\Database\Eloquent\Model|integer $model Instancia del modelo o
	 *                                                           valor del campo primario.
	 * @return Illuminate\Http\Response|Illuminate\Database\Eloquent\Model
	 */
	public function update($id=null,$cfg=null){
		$cfg = Format::cast('array',$cfg);
		if($id instanceof Model){
			$cfg['model'] = $id;
		}else if($id !== null){
			$cfg['search'] = $id;
		}
		return $this->process('update',$cfg);
	}

	public function save($id=null,$cfg=null){
		$cfg = Format::cast('array',$cfg);
		if($id instanceof Model){
			$cfg['model'] = $id;
		}else if($id !== null){
			$cfg['search'] = $id;
		}
		return $this->process('save',$cfg);
	}


	/**
	 * Elimina un registro existente.
	 * @param  Illuminate\Database\Eloquent\Model|integer $model Instancia del modelo o
	 *                                                           valor del campo primario.
	 * @return Illuminate\Http\Response|Illuminate\Database\Eloquent\Model
	 */
	public function delete($id,$cfg=null){
		$cfg = Format::cast('array',$cfg);
		if($id instanceof Model){
			$cfg['model'] = $id;
		}else if($id !== null){
			$cfg['search'] = $id;
		}
		return $this->process('delete',$cfg);
	}

	/**
	 * Crea o elimina un registro.
	 * Si el registro ya existe lo eliminará, de lo contrario lo creará.
	 * @param  integer|null             $id  Id del registro.
	 * @param  array                    $cfg Configuración adicional.
	 * @return Illuminate\Http\Response
	 */
	public function toggle($id=null,$cfg=null){
		$this->process('toggle',$this->cfg($cfg,['search'=>$id]));
	}

	/**
	 * Restaura un registro previamente eliminado.
	 * @param  Illuminate\Database\Eloquent\Model|integer $model Instancia del modelo o
	 *                                                           valor del campo primario.
	 * @return Illuminate\Http\Response|Illuminate\Database\Eloquent\Model
	 */
	public function restore($id,$cfg=null){
		$cfg = Format::cast('array',$cfg);
		if($id instanceof Model){
			$cfg['model'] = $id;
		}else if($id !== null){
			$cfg['search'] = $id;
		}
		return $this->process('restore',$cfg);
	}


	/**
	 * Crea nuevos registros por lote.
	 * @param  array  $cfg Configuración.
	 * @return Illuminate\Http\Response|array
	 */
	public function createAll($cfg=null){
		return $this->processAll('create',$cfg);
	}

	/**
	 * Crea nuevos registros por lote.
	 * @param  array  $cfg Configuración.
	 * @return Illuminate\Http\Response|array
	 */
	public function saveAll($cfg=null){
		return $this->processAll('save',$cfg);
	}


	/**
	 * Edita registros existentes por lote.
	 * @param  array  $cfg Configuración.
	 * @return Illuminate\Http\Response|array
	 */
	public function updateAll($cfg=null){
		return $this->processAll('update',$cfg);
	}

	/**
	 * Elimina registros existentes por lote.
	 * @param  array  $cfg Configuración.
	 * @return Illuminate\Http\Response|array
	 */
	public function deleteAll($cfg=null){
		return $this->processAll('delete',$cfg);
	}

	/**
	 * Restaura un registro previamente eliminado.
	 * @param  array  $cfg Configuración.
	 * @return Illuminate\Http\Response|array
	 */
	public function restoreAll($cfg=null){
		return $this->processAll('restore',$cfg);
	}

	/**
	 * Añade, edita o elimina registros por lote de la base de datos.
	 * @param  string         $type Tipo de acción.
	 * @param  array|null     $cfg  Configuración.
	 * @return array|Response
	 */
	public function processAll($scenario,$cfg=null){
		$cfg = $this->cfg($cfg,[
			'groupLocation'=>$this->cast('pluralize',
																	 $this->model()->getCrossName())
		]);
		extract($cfg);
		$models = [];
		foreach($group as $k => $gCfg){
			$models[$k] = $this->process($scenario,$gCfg);
		}
		return $this->formatReturn($return,$scenario,$models);
	}

	/**
	 * Añade, edita o elimina un registro en la base de datos.
	 * @param  string     $scenario Tipo de acción. Puede ser 'create','update' o 'delete'.
	 * @param  array|null $cfg  Configuración.
	 * @return Model|Response
	 */
	public function process($scenario,$cfg=null){
		$cfg = $this->cfg($cfg);
		extract($cfg);
		$cfg['return'] = 'model';
		if($scenario == 'restore'){
			$cfg['withTrashed'] = true;
		}
		$exists = $this->find(null,array_merge($cfg,['silent'=>true]));
		$isDeleted = $scenario == 'restore' && can_trash($model);

		if($scenario == 'save'){
			$scenario = (!$exists) ? 'create' : 'update';
		}
		else if($scenario == 'toggle'){
			$scenario = (!$exists) ? 'create' : 'delete';
		}
		if($scenario == 'update' || $scenario == 'delete' || $scenario == 'restore'){
			if(!$exists || ($scenario == 'delete' && $isDeleted) || ($scenario == 'restore' && !$isDeleted)){
				return $this->formatReturn($return,$scenario,null);
			}
			$model = $exists;
		}

		if($validate){
			$this->check($scenario,$location,$model->id);
		}

		//Prevenimos que se creen nuevos registros si el 'cross' fue definido
		//cómo único
		if($scenario == 'create'){
			if($exists && $this->crossLookup && $this->model()->isOneCross()){
				return $this->formatReturn($return,'update',$exists);
			}
		}


		$events = ['process',$scenario];
		$ori  = clone $model;
		if($scenario == 'create' || $scenario == 'update'){
			foreach($data as $field => $value){
				if($scenario == 'update' && ($value === null || is_string($value) && empty($value))){
					continue;
				}
				$model->{$field} = $value instanceof UploadedFile
							          	 ? app('Api\Api')->upload->fromFile($value)
								           : $value;
			}
		}
		$model = &$model;
		$r = $this->trigger($events,['model'=>$model,'ori'=>$ori,'cfg'=>$cfg],'before');
		if($r === false){
			return $this->formatReturn($response,$scenario,null);
		}else if($r instanceof Response){
			return $response == 'model' ? null : $r;
		}

		switch ($scenario) {
			case 'create':
			case 'update':
				//Custodiamos que los campos relacionados sean rellenados
				if($this->crossLookup){
					if($this->model()->hasCrossParent()){
						$m = $this->model()->getCrossParent();
						$rel = $m->newQuery()
										 ->where($m->getKeyName(),$model->{$m->getCrossKeyName()})->first();
						if(!$rel){
							return $this->formatReturn($return,$scenario,null);
						}
					}
					if($this->model()->hasCrossRel()){
						$m = $this->model()->getCrossRel();
						$rel = $m->newQuery()
										 ->where($m->getKeyName(),$model->{$m->getCrossKeyName()})->first();
						if(!$rel){
							return $this->formatReturn($return,$scenario,null);
						}
					}
				}
				$model->save();
				if('create' && $with){
					$model = $model->newQuery()->with($with)->find($model->{$model->getKeyName()});
				}
				break;
			case 'delete':
				$model->delete();
				break;
			case 'restore':
				$model->restore();
				break;
		}


		$this->trigger($events,['model'=>$model,'ori'=>$ori, 'cfg'=>$cfg],'after');
		return $this->formatReturn($return,$scenario,$model);
	}

	/**
	 * Da formato a las respuestas de la instancia.
	 * @param  string                                  $type     Tipo de respuesta. Puede ser response o model.
	 * @param  string                                  $scenario Escenario de la respuesta.
	 * @param  Illuminate\Database\Eloquent\Model|null $model    Modelo a devolver.
	 * @return Illuminate\Database\Eloquent\Model|Illuminate\Http\Response
	 */
	protected function formatReturn($type,$scenario,$model=null){
		if($type == 'model'){
			return $model;
		}
		if(!$model){
			return $this->notFound();
		}
		//Devolvemos la respuesta adecuada para cada escenario:
		switch ($scenario) {
			case 'create':
				return $this->restored($model);
			case 'restore':
				return $this->restored($model);
			case 'update':
				return $this->updated($model);
			case 'delete':
				return $this->deleted($model);
			case 'read':
				$useObject = request()->input('encapsulate');
				if ($useObject === 'true' || $useObject === '1' || $useObject == 1) {
					if ($model instanceof Model) {
						$model = $model->toArray();
					}
					$model = ['data' => $model];
				}
				return $this->ok($model);
			case 'error':
				return $this->internal($model);
		}
	}

	/**
	 * Da formato a las configuraciones suministradas.
	 * @param  array  $raw Configuración a dar formato.
	 * @return array
	 */
	public function cfg($raw=null){
		$raws = func_get_args();
		$cfg = ['ignore_order' => false];
		$base = ['location','groupLocation','searchField','search','query','return','trashed','model','all','single','callback','validate'];
		foreach(['appends','conditions','guard','defaults','statics','nullable','morph'] as $name){
			$cfg[$name] = property_exists($this, $name)?$this->cast('array',$this->{$name}):[];
		}
		foreach(['with','only','except'] as $name){
			$value = app('request')->get($name);
			if(is_string($value) && !empty($value)){
				$value = array_map('trim', explode(',',$value));
			}
			$cfg[$name] = is_array($value) ? $value : [];
		}
		foreach(['with','only','except'] as $name){
			$value = property_exists($this, $name) ? $this->{$name} : null;
			if(is_string($value) && !empty($value)){
				$value = array_map('trim', explode(',',$value));
			}
			$cfg[$name] = is_array($value) ? array_merge($cfg[$name], $value) : $cfg[$name];
		}
		foreach($base as $name){
			$cfg[$name] = property_exists($this, $name) && !is_array($this->{$name})
										? $this->{$name}
										: null;
		}
		foreach($raws as $k => $raw){
			if(!is_array($raw)){
				continue;
			}
			if(isset($raw['ignore_order']) && $raw['ignore_order'] === true){
				$cfg['ignore_order'] = true;
			}
			foreach(['conditions','guard','defaults','statics','nullable','morph'] as $name){
				if(array_key_exists($name, $raw)){
					$cfg[$name] = $this->cast('array',$raw[$name]);
				}
			}
			foreach(['with'] as $name){
				$value = array_key_exists($name, $raw) ? $raw[$name] : null;
				if(is_string($value) && !empty($value)){
					$value = array_map('trim', explode(',',$value));
				}
				if(is_array($value)){
					$cfg[$name] = $value;
				}
			}
			foreach($base as $name){
				if(array_key_exists($name, $raw) && !is_array($raw[$name]) && $raw[$name] !== null){
					$cfg[$name] = $raw[$name];
				}
			}
		}
		$cfg['input'] = empty($cfg['location'])
						     		? array_merge(app('request')->all(),app('request')->allFiles())
						     		: app('request')->input($cfg['location']);


		if(property_exists($this, 'input') && $this->input !== null){
			if(is_array($cfg['input']) && is_array($this->input)){
				$cfg['input'] = array_merge($cfg['input'],$this->input);
			}else{
				$cfg['input'] = $this->input;
			}
		}
		foreach($raws as $raw){
			if(!is_array($raw)){
				continue;
			}
			if(array_key_exists('input',$raw) && $raw['input'] !== null){
				if(is_array($cfg['input']) && is_array($raw['input'])){
					$cfg['input'] = array_merge($cfg['input'],$raw['input']);
				}else{
					$cfg['input'] = $raw['input'];
				}
			}
		}
		$cfg['values'] = $this->cast('array',$cfg['input']);
		foreach($raws as $raw){
			if(!is_array($raw)){
				continue;
			}
			if(array_key_exists('values', $raw) && is_array($raw['values'])){
				$cfg['values'] = array_merge($cfg['values'],$raw['values']);
			}
		}
		$cfg['model'] = $this->model($cfg['model'] ? $cfg['model'] : null);
		$cfg['query'] = ($cfg['query']) ? $cfg['query'] : $cfg['model']->newQuery();
		$cfg['data']  = [];
		foreach($cfg['model']->getFillable() as $field){
			if($field == $cfg['searchField'] && !empty($cfg['search'])){
				$cfg['data'][$field] = $cfg['search'];
				continue;
			}
			if(array_key_exists($field,$cfg['conditions'])){
				$cfg['data'][$field] = $cfg['conditions'][$field];
				continue;
			}
			if(array_key_exists($field, $cfg['statics'])){
				$cfg['data'][$field] = $cfg['statics'][$field];
				continue;
			}
			if(in_array($field,$cfg['guard'])){
				continue;
			}
			$value = array_key_exists($field,$cfg['input']) ? $cfg['input'][$field] : null;

			if(in_array($field,$cfg['morph']) && $cfg['morph'][$field] instanceof Closure){
				$value = $cfg['morph'][$field]($value);
			}

			if(($value === null || is_string($value) && empty($value)) && array_key_exists($field,$cfg['defaults'])){
				$value = $cfg['defaults'][$field];
			}
			$cfg['data'][$field] = $value;
		}
		$cfg['validate'] = $cfg['validate'] === null || (is_string($cfg['validate']) && empty($cfg['validate']))
											 ? true
											 : $this->cast('boolean',$cfg['validate']);
		$cfg['group'] = [];
		if(!empty($cfg['groupLocation'])){
			foreach($this->input($cfg['groupLocation'],[],'array') as $k => $input){
				$gCfg = [
					'location'      => $cfg['groupLocation'] .'.' . $k,
					'groupLocation' => '',
					'return'        => 'model'
				];
				if(!is_array($input)){
					$gCfg['search'] = $input;

				}
				$cfg['group'][$k] = $this->cfg($cfg,$gCfg);
			}
		}
		ksort($cfg);

		return $cfg;
	}
}
