<?php namespace Api\Routing\Controller\Contracts;

interface Api {
	/**
	 * Devuelve la instancia de un módulo de la api.
	 * @param  string|null $module Nombre del módulo. Si no es especificado
	 *                             devolverá la instancia de la API.
	 * @return Api\Api\Module
	 */
	function api($module=null);
}
