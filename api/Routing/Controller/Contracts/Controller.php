<?php namespace Api\Routing\Controller\Contracts;
use Api\Contracts\Eventable;

interface Controller extends Api,Eventable,Request,Validation{
}
