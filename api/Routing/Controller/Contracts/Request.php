<?php namespace Api\Routing\Controller\Contracts;

interface Request {
	/**
	 * Devuelve la petición actual.
	 * @return Illuminate\Http\Request
	 */
	function request();

	/**
	 * Devuelve el valor de un campo dentro de la petición actual.
	 * @param  string $name    Nombre del campo.
	 * @param  mixed  $default Valor por defecto si el campo no existe o su valor es nulo.
	 * @return mixed
	 */
	function input($name,$default=null);
}