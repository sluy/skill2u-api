<?php namespace Api\Routing\Controller\Contracts;

interface Validation {
	/**
	 * Devuelve la instancia de una regla personalizada del sistema o de Laravel.
	 * @param  string $name Nombre de la regla.
	 * @return Object
	 */
	function rule($name);

	/**
	 * Valida los campos suministrados en la petición actual.
	 * @param  string $scenario Escenario de la validación. Para obtener las reglas
	 *                          de validación buscará ejecutar el método
	 *                          on{$scenario}rules(). Si el escenario es de guardado
	 *                          (create,add,update,edit) correrá adicionalmente el 
	 *                          método onSaveRules() antes de ejecutar {$scenario}rules().
	 * @return void
	 */
	function check($scenario);
}
