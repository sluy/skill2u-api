<?php namespace Api\Routing\Controller;
use Api\Format;

trait Request {
	/**
	 * Devuelve la petición actual.
	 * @return Illuminate\Http\Request
	 */
	public function request(){
		return app('request');
	}
	/**
	 * Devuelve el valor de un campo dentro de la petición actual.
	 * @param  string $name    Nombre del campo.
	 * @param  mixed  $default Valor por defecto si el campo no existe o su valor es nulo.
	 * @return mixed
	 */
	public function input($name,$default=null,$cast=null){
		$value = ($this->request()->file($name))
					 ? $this->request()->file($name)
					 : $this->request()->input($name,$default);

		return ($cast) ? Format::cast($cast,$value) : $value;
	}
}