<?php namespace Api\Routing\Controller;
use Api\Format;

trait Response {
	public function render($data=null,$code=null){
		return app('Api\Api')->response->render($data,$code);
	}

	public function ok($data=null){
		return app('Api\Api')->response->render($data,'ok');
	}


	public function success($data=null){
		return app('Api\Api')->response->render($data,'success');
	}

	public function created($data=null){
		return app('Api\Api')->response->render($data,'created');
	}

	public function updated($data=null){
		return app('Api\Api')->response->render($data,'updated');
	}

	public function deleted($data=null){
		return app('Api\Api')->response->render($data,'deleted');
	}

	public function restored($data=null){
		return app('Api\Api')->response->render($data,'restored');
	}

	public function bad($data=null){
		return app('Api\Api')->response->render($data,'bad');
	}

	public function error($data=null){
		return app('Api\Api')->response->render($data,'error');
	}

	public function unauthorized($data=null){
		return app('Api\Api')->response->render($data,'unauthorized');
	}

	public function forbidden($data=null){
		return app('Api\Api')->response->render($data,'forbidden');
	}

	public function notFound($data=null){
		return app('Api\Api')->response->render($data,'notfound');
	}

	public function failure($data=null){
		return app('Api\Api')->response->render($data,'failure');
	}
}