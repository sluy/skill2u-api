<?php namespace Api\Routing\Controller;
use Api\Format;

trait Route {
	/**
	 * Devuelve la ruta actual.
	 * @return array
	 */
	public function route($field=null,$default=null,$cast=null){
		$params = @Format::cast('array', Format::cast('array',app('request')->route())[2]);
		if($field === null){
			return $params;
		}

		if(is_array($field)){
			$values = [];
			foreach($field as $k => $f){
				$values[$k] = $this->route($f);
			}
			return $values;
		}
		$value = null;
		if(is_int($field) || is_string($field) && strval(intval($field)) == $field){
			$field = intval($field);
			$params = array_values($params);
		}
		if(array_key_exists($field, $params)){
			$value = $params[$field];
		}

		$value = $value === null || (is_string($value) && empty($value))
						 ? $default
						 : $value;
		return ($cast)
					 ? Format::cast($cast,$value)
					 : $value;
	}
	/**
	 * Devuelve los parámetros de la ruta.
	 * @return array
	 */
	public function routeParams(){
		$route = $this->route();
		return isset($route[2]) && is_array($route[2]) ? $route[2] : [];
	}
}