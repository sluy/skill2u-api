<?php namespace Api\Routing\Controller;

use Closure,
		Validator,
		Api\Rule,
		Api\Format;

trait Validation {

	protected $rules = [];

	/**
	 * Devuelve la instancia de una regla personalizada del sistema o de Laravel.
	 * @param  string $name Nombre de la regla.
	 * @return Object
	 */
	public function rule($name){
		$args = func_get_args();
		array_shift($args);
		$args = array_values($args);
		return empty($args)
					 ? call_user_func('Api\Rule::'.$name)
					 : call_user_func_array('Api\Rule::'.$name, $args);
	}

	public function addRules($scenario,$rules){
		if((is_int($scenario) || (is_string($scenario) && !empty($scenario))) && 
			($rules instanceof Callback || (is_array($rules) && !empty($rules)))){
			$scenario = strtolower($scenario);
			if(!isset($this->rules[$scenario]) || !is_array($this->rules[$scenario])){
				$this->rules[$scenario] = [];
			}
			$this->rules[$scenario][] = $rules;
		}
		return $this;
	}

	public function getRules($scenario,array $params,$location=null){
		if(is_array($scenario)){
			$rules = [];
			foreach($scenario as $s){
				$rules = array_merge_recursive($rules,$this->getRules($s,$params,$location));
			}
			return $rules;
		}
		if((!is_string($scenario)||empty($scenario)) && !is_int($scenario)){
			return [];
		}

		$scenario = strtolower($scenario);


		$rules = [];
		$params = Format::cast('array',$params);
		$raws = (isset($this->rules[$scenario]) && is_array($rules[$scenario]))
						 ? $this->rules[$scenario]
						 : [];
		if(method_exists($this, 'on'.$scenario.'rules')){
			array_unshift($raws,function() use ($params,$scenario){
				return call_user_func_array([$this,'on'.$scenario.'rules'],$params);
			});
		}

		foreach($raws as $raw){
			if($raw instanceof Closure){
				$raw = call_user_func_array($raw, $params);
			}
			if(is_array($raw)){
				$rules = array_merge_recursive($rules,$raw);
			}
		}
		if((is_string($location) && !empty($location) )|| is_int($location)){
			$tmp = [];
			foreach($rules as $k => $rule){
				$tmp[$location.'.'.$k] = $rule;
			}
		}
		return $rules;
	}


	/**
	 * Valida los campos suministrados en la petición actual.
	 * @param  string $scenario Escenario de la validación. Para obtener las reglas
	 *                          de validación buscará ejecutar el método
	 *                          on{$scenario}rules(). Si el escenario es de guardado
	 *                          (create,add,update,edit) correrá adicionalmente el 
	 *                          método onSaveRules() antes de ejecutar {$scenario}rules().
	 * @return void
	 */
	public function check($scenarios,$location=""){
		if(is_string($scenarios) || is_int($scenarios)){
			$scenarios = [$scenarios];
		}
		$scenarios = array_map('strtolower', Format::cast('array',$scenarios));
		if(!in_array('save',$scenarios) && (in_array('update',$scenarios) || in_array('create',$scenarios))){
			$scenarios[] = 'save';
		}
		$params = func_get_args();
		for($n=0;$n<2;$n++){
			array_shift($params);
		}
		$params = array_values($params);
		$rules = $this->getRules($scenarios,$params,$location);
		$errors = [];

		if(!empty($rules)){
			if(!empty($location)){
				$items = app('request')->input($location);
				if(!is_array($items)){
					$items = [];
				}
				if(empty($items)){
					return $this->error();
				}
				$validator = Validator::make($items,$rules);
				if($validator->fails()){
					foreach($validator->messages()->toArray() as $k => $msgs){
						$errors[$location.'.'.$k] = $msgs;
					}
				}
			}else{
				$validator = Validator::make(app('request')->all(),$rules);
				if($validator->fails()){
					$errors = $validator->messages()->toArray();
				}
			}
		}

		if($errors){
			$this->error($errors);
		}
		return true;
	}
}