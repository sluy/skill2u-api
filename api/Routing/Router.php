<?php namespace Api\Routing;
use DirectoryIterator,
		Closure,
		Api\Format;
/**
 * Provee autocarga de rutas.
 */
class Router {
	protected $cfg = [
		'namespace'  => null,
		'prefix'     => null,
		'middleware' => []
	];

	protected $path = null;

	/**
	 * Rutas en directorios asociados.
	 * @var array
	 */
	protected $nest = [];
	/**
	 * Métodos
	 * @var array
	 */
	protected $binds = [];
	/**
	 * Grupos asociados
	 * @var array
	 */
	protected $groups = [];
	/**
	 * Determina si la instancia fue ejecutada.
	 * @var boolean
	 */
	protected $executed = false;
	/**
	 * Determina si la instancia carǵo los archivos.
	 * @var boolean
	 */
	protected $loaded = false;
	/**
	 * Controlador actual.
	 * @var null
	 */
	protected $controller = null;

	/**
	 * Constructor.
	 * @param   string     $path Ruta ase a cargar.
	 * @param   array|null $cfg  Matriz de opciones.
	 * @return  void
	 */
	public function __construct($path,array $cfg=null){
		$this->path($path);
		if(is_array($cfg)){
			foreach($cfg as $key => $value){
				$this->{$key}($value);
			}
		}
	}

	/**
	 * Método mágico.
	 * @param  string $name   Nombre del método.
	 * @param  array  $params Parámetros a suministrar.
	 * @return mixed
	 */
	public function __call($name,array $params=[]){
		$ci = strtolower($name);
		if(in_array($ci, ['get','post','put','delete','patch','options'])){
			array_unshift($params, $name);
			return call_user_func_array([$this,'bind'], $params);
		}
		if(empty($params)){
			return array_key_exists($name, $this->cfg)
						 ? $this->cfg[$name]
						 : null;
		}
		$this->cfg[$name] = $params[0];
		return $this;
	}

	/**
	 * Devuelve la instancia del Router por defecto de Laravel.
	 * @return Illuminate\Routing\Router
	 */
	public function raw(){
		return app('router');
	}

	/**
	 * Crea una nueva instancia.
	 * @param   string     $path Ruta ase a cargar.
	 * @param   array|null $cfg  Matriz de opciones.
	 * @return Api\Routing\Router
	 */
	public static function newInstance($path=null,array $cfg=null){
		$class = get_called_class();
		return new $class($path,$cfg);
	}
	/**
	 * Genera y guarda un grupo en la instancia.
	 * @param  array|null  $cfg      Configuración del grupo.
	 * @param  Closure     $callback Callback a ejecutar.
	 * @return Api\Routing\Router
	 */
	public function group($cfg,Closure $callback){
		$cfg = Format::cast('array',$cfg);
		if(!isset($cfg['controller'])){
			$cfg['controller'] = $this->controller();
		}
		$group = static::newInstance(null,$cfg);
		$callback($group);
		$this->groups[] = $group;
		$this->nest($group);
		return $this;
	}

	/**
	 * Añade una nueva ruta hija a la instancia.
	 * @param  array $data Configuración de la ruta.
	 * @return void
	 */
	public function nest($data){
		if(is_array($data)){
			$data = static::newInstance(null,$data);
		}
		$this->nest[@!empty($data->prefix())?$data->prefix():null] = $data;
	}

	/**
	 * Configura los middlewares de la ruta actual.
	 * @param  array $values Middlerares por agregar.
	 * @return Api\Routing\Router
	 */
	public function middleware($values=null){
		if($values === null){
			return $this->cfg['middleware'];
		}
		if(is_string($values)){
			$values = array_map('trim',explode(',',$values));
		}
		if(is_array($values)){
			foreach($values as $value){
				if(!empty($value) && is_string($value) && !in_array($value, $this->cfg['middleware'])){
					$this->cfg['middleware'][] = $value;
				}
			}
		}
		return $this;
	}

	/**
	 * Genera las rutas para la lectua, creación y eliminación de registros.
	 * @param  boolean $withRestore Determina si se agregará la ruta para recuperación de registros.
	 * @return Api\Routing\Router
	 */
	public function crud($withRestore=false){
		$this->get('@paginate')
				 ->get('@find','/{id}')
				 ->post('@create')
				 ->put('@update','/{id}')
				 ->delete('@deleteAll');
		if($withRestore){
			$this->patch('@restoreAll');
		}
		return $this;
	}

	/**
	 * Genera las rutas para la lectura , creación y eliminación de registros cruzados.
	 * @param  string $name Nombre del registro cruzado.
	 * @return Api\Routing\Router
	 */
	public function cross($name){
		$backup = $this->controller();
		$this->controller($backup.'\\'.ucfirst($name))
				 ->get('@paginate','/{id}/'.$name)
				 ->post('@createAll', '/{id}/'.$name)
				 ->delete('@deleteAll','/{id}/'.$name);
		$this->controller($backup);
		return $this;
	}

	/**
	 * Vincula rutas a la instancia actual.
	 * @param  string      $method Método de la ruta. Puede ser : get,post,put,delete, patch y options.
	 * @param  string      $call   controlador y acción a ejecutar. Si el controlador no es definido 
	 *                             tomará el de la instancia actual.
	 * @param  string|null $path   Ruta de acceso.
	 * @return Api\Routing\Router
	 */
	public function bind($method,$call,$path=null){
		$ctrl   = '';
		$action = '';
		if(strpos($call, '@') !== false){
			list($ctrl,$action) = array_map('trim',explode('@',$call));
		}else{
			$action = $call;
		}
		if(empty($ctrl)){
			$ctrl = $this->controller();
		}

		if(!ends_with(strtolower($ctrl),'controller')){
			$ctrl .= 'Controller';
		}
		if(empty($path)){
			$path = '/';
		}
		$this->binds[] = [
			'method'=>$method,
			'route'=>$path,
			'action'=>$ctrl.'@'.$action
		];
		return $this;
	}
	/**
	 * Devuelve la configuración de la instancia actual.
	 * @return array
	 */
	public function cfg(){
		$tmp = [];
		foreach($this->cfg as $key => $value){
			if(!empty($value)){
				$tmp[$key] = $value;
			}
		}
		return $tmp;
	}

	/**
	 * Carga e inyecta las rutas de la instancia.
	 * @return Api\Routing\Router
	 */
	public function run(){
		if($this->executed){
			return $this;
		}
		$this->executed = true;
		$this->load();
		$cfg = $this->cfg();
		
		if(array_key_exists('controller',$cfg)){
			unset($cfg['controller']);
		}
		$bindCfg = $this->cfg();
		$nestCfg = [];
		if(array_key_exists('namespace', $bindCfg)){
			$nestCfg['namespace'] = $bindCfg['namespace'];
			unset($bindCfg['namespace']);
		}


		$this->raw()->group($bindCfg,function() use($nestCfg){
			foreach($this->binds as $cfg){
				$this->raw()->{$cfg['method']}($cfg['route'],$cfg['action']);
			}
			foreach($this->groups as $k => $child){
				$child->run();
			}
			$this->raw()->group($nestCfg,function(){
				foreach($this->nest as $k  => $child){
					$child->run();
				}
			});
		});
		return $this;
	}

	/**
	 * Determina si la instancia actual tiene definido una ruta de carga.
	 * @return boolean
	 */
	public function hasPath(){
		$path = $this->path();
		return is_string($path) && !empty($path) && file_exists($path) && is_readable($path)
					 && is_dir($path) || is_file($path) && strtolower(substr($path, -4)) == '.php';
	}
	/**
	 * Carga las rutas directamente de un directorio en el disco.
	 * @return Api\Routing\Router
	 */
	public function load(){
		if($this->loaded || !$this->hasPath()){
			return $this;
		}
		$this->loaded = true;
		$this->{is_dir($this->path()) ? 'loadFromFolder' : 'loadFromFile'}($this->path());

		ksort($this->binds);
		ksort($this->nest);
		return $this;
	}

	/**
	 * Carga las rutas directamente de un archivo en el disco.
	 * @param  string $path Ruta absoluta del archivo.
	 * @return void
	 */
	protected function loadFromFile($path){
		if(!$this->controller()){
			$this->controller(ucfirst($this->prefix()));
		}
		require($path);
		$dir = substr($path, 0, -4);

		if(file_exists($dir) && is_dir($dir) && is_readable($dir)){
			$this->loadFromFolder($dir);
		}
	}
	/**
	 * Carga las rutas directamente de un directorio en el disco.
	 * @param  string $path Ruta absoluta del archivo.
	 * @return void
	 */
	protected function loadFromFolder($path){
		$ignore = [];
		foreach(new DirectoryIterator($path) as $f){
			if($f->isDot() || !$f->isReadable() || !$f->isFile() || $f->getExtension() != 'php'){
				continue;
			}
			$name = substr($f->getFilename(),0,-4);
			$this->nest([
				'namespace' => ucfirst($name),
				'prefix' => $name,
				'path'   => $f->getPathname(),
			]);
			$ignore[]=$name;
		}
		foreach(new DirectoryIterator($path) as $f){
			if($f->isDot() || !$f->isReadable() || !$f->isDir() || in_array($f->getFilename(), $ignore)){
				continue;
			}
			$this->nest([
				'prefix'    => $f->getFilename(),
				'namespace' => ucfirst($f->getFilename())
			]);
		}
	}
}