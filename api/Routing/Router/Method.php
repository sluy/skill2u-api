<?php namespace Api\Routing\Router;
use Api\Routing\Router;

class Method {
	protected $cfg = [];

	public function __construct(){}

	public function bind($method,$call,$path=null){
		$ctrl   = '';
		$action = '';
		if(strpos($call, '@') !== false){
			list($ctrl,$action) = array_map('trim',explode('@',$call));
		}else{
			$action = $call;
		}
		if(empty($ctrl)){
			$ctrl = $this->controller;
		}
		if(empty($path)){
			$path = '/';
		}
		$this->binds[$method.':'.$path] = ['method'=>$method,'route'=>$path,'action'=>$ctrl.'@'.$action];
		return $this;
	}

	public function __call($name,$params){
		if(empty($params)){
			return array_key_exists($name, $this->cfg)
						 ? $this->cfg[$name]
						 : null;
		}
		$this->cfg[$name] = $params[0];
	}
}