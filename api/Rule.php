<?php namespace Api;
use ReflectionClass,
		Exception;

class Rule {
	protected static $validators = [
		'notexist'       => 'Api\Rules\NotExists',
		'upload'         => 'Api\Rules\Upload',
		'categoryexists' => 'Api\Rules\CategoryExists',
		'youtube'        => 'Api\Rules\Youtube',
		'norelations'    => 'Api\Rules\NoRelations',
		'uniquein'       => 'Api\Rules\UniqueIn',
		'existsin'       => 'Api\Rules\ExistsIn',
		'password'       => 'Api\Rules\Password',
		'comparedate'    => 'Api\Rules\CompareDate'
	];

	public static function __callStatic($name,$args){
		$ci = strtolower($name);
		if(isset(static::$validators[$ci])){
			$reflection = new ReflectionClass(static::$validators[$ci]);
			return empty($args)
						 ? $reflection->newInstance()
						 : $reflection->newInstanceArgs($args);
		}
		try {
			return empty($args) 
						 ? call_user_func('Illuminate\Validation\Rule::'.$name)
						 : call_user_func_array('Illuminate\Validation\Rule::'.$name,$args);
		} catch (Exception $e){
			throw new Exception("No existe el validador " . $name, 2);
		}
	}
}