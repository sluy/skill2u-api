<?php namespace Api\Rules;
use Carbon\Carbon;
/**
 * Valida que la dirección suministrada sea un vínculo de Youtube.
 */
class CompareDate extends Rule
{
	/**
	 * Método ejecutado al iniciar la instancia.
	 * Establece los atributos necesarios para la correcta validación.
	 * 
	 * @param  string  $model  Nombre del modelo.
	 * @param  integer $check  Nombre del campo de la búsqueda.
	 * @return 
	 */
	protected function boot($type,$check=null){
		$check = $check 
					   ?Carbon::parse($check)
					   : Carbon::now();

		$this->setAttr('check',$check)
				 ->setAttr('type',$type);
	}

  /**
   * Determina si el valor pasa la validación.
   * @param  string       $attribute  Nombre del campo.
   * @param  mixed        $value      Valor del campo.
   * @return bool|string              Puede devolver TRUE si el valor pasó la validación,
   *                                  de lo contrario una cadena que representará el
   *                                  nombre del error.
   */
  public function check($value){
  	$date = Carbon::parse($value);

  	$diff = $date->diffInDays($this->getAttr('check'),false);
  	if($this->getAttr('type') == 'max'){
  		return $diff < 0
  					 ? 'date.max'
  					 : true;
  	}

		return $diff > 0
			 ? 'date.min'
			 : true;
  }
}