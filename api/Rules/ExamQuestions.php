<?php namespace Api\Rules;
/**
 * Valida que las preguntas de un exámen estén bien formuladas.
 */
class ExamQuestions extends Rule
{
	/**
	 * Mensajes de error.
	 * @return array
	 */
	protected function messages(){
		return [];
	}


	/**
	 * Método ejecutado al iniciar la instancia.
	 * Establece los atributos necesarios para la correcta validación.
	 * @param  string $autoWeight  Determina si el "peso" de las preguntas será automático.
	 * @return void
	 */
	protected function boot($autoWeight=false){
		$this->setAttr('autoWeight',$autoWeight);
	}

  /**
   * Determina si el valor pasa la validación.
   * @param  mixed        $value      Valor del campo.
   * @return bool|string              Puede devolver TRUE si el valor pasó la validación,
   *                                  de lo contrario una cadena que representará el
   *                                  nombre del error.
   */
	public function check($questions){
		$autoWeight = $this->getAttr('autoWeight',false,'boolean');
		$error = null;
		$weight = 0;
		foreach($questions as $qKey => $q){
			$weight += intval(isset($q['weight'])?$q['weight']:0);
			$aQuantity = intval(isset($q['multi'])?$q['multi']:0);
			$aQuantity = $aQuantity > 1 ? $aQuantity : 1;
			//Verificamos que el # de respuestas sea válido
			if(count($q['answers']) < $aQuantity){
				$error = 'La pregunta '.$qKey.' debe tener al menos ' . $aQuantity . ' respuestas.';
				break;
			}
			$correctAQuantity = intval(isset($q['multi']) ? $q['multi'] : 1);
			$correctAQuantity = $correctAQuantity > 0 ? $correctAQuantity : 1;
			//Verificamos que el # de respuestas correctas sea válido.
			$correct = 0;
			foreach($q['answers'] as $aKey => $a){
				if(isset($a['correct']) && $a['correct'] == true){
					$correct++;
				}
			}
			if($correct < $correctAQuantity){
				$error = 'La pregunta '.$qKey.' debe tener al menos ' . $correctAQuantity . 
																									' respuesta(s) correcta(s).';
				break;
			}
		}
		if($autoWeight){
			if($weight != 100){
				$error = 'La sumatoria del puntaje de todas las preguntas debe ser exactamente igual a 100.';
			}
		}
		return !$error ? true : $error;
	}
}