<?php namespace Api\Rules;
use Illuminate\Database\Eloquent\Model;
/**
 * Valida que un registro exista en una tabla de la base de datos.
 */
class ExistsIn extends Rule
{

	/**
	 * Método ejecutado al iniciar la instancia.
	 * Establece los atributos necesarios para la correcta validación.
	 * @param  string $table  Nombre de la tabla.
	 * @param  array  $static Matriz con los campos y valores fijos.
	 * @param  string $check  Nombre del campo a validar. Por defecto tomará el id.
	 * @return void
	 */
	protected function boot($table=null,$static=[],$check=null){
		if($table instanceof Model){
			$table = $table->getTable();
		}
		$this->setAttr('table',$table)
				 ->setAttr('static',$static)
				 ->setAttr('check',$check);
	}

	protected function messages(){
		return ['exists_in'=>'El registro no existe.'];
	}
  /**
   * Determina si el valor pasa la validación.
   * @param  mixed        $value      Valor del campo.
   * @return bool|string              Puede devolver TRUE si el valor pasó la validación,
   *                                  de lo contrario una cadena que representará el
   *                                  nombre del error.
   */
  public function check($value)
  {
  	$where = $this->getAttr('static',[],'array');
  	$where[$this->getAttr('check',$this->getAttr('field'),'string')] = $value;
  	$query = app('db')->table($this->getAttr('table','','string'));
  	foreach($where as $field => $value){
  		$query->where($field,$value);
  	}
  	return ($query->first() !== null)
  				 ? true
  				 : 'exists_in';
  }
}