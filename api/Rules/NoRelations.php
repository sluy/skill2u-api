<?php namespace Api\Rules;
use Illuminate\Database\Eloquent\Model as EModel,
		Api\Database\Eloquent\Model,
		Illuminate\Database\Eloquent\Relations\HasMany,
		Illuminate\Database\Eloquent\Relations\HasOne,
		ReflectionClass,
		Exception;
/**
 * Valida que un modelo no tenga relaciones activas.
 */
class NoRelations extends Rule
{
	/**
	 * Método ejecutado al iniciar la instancia.
	 * Establece los atributos necesarios para la correcta validación.
	 * 
	 * @param  string  $model  Nombre del modelo.
	 * @param  integer $check  Nombre del campo de la búsqueda.
	 * @return 
	 */
	protected function boot($model,$check='id'){
		$this->setAttr('model',$model);
		$this->setAttr('check',$check);
	}
	/**
	 * Mensajes de error.
	 * @return array
	 */
	protected function messages(){
		return ['No se puede eliminar, tiene vinculos con otros registros.'];
	}

  /**
   * Determina si el valor pasa la validación.
   * @param  string       $attribute  Nombre del campo.
   * @param  mixed        $value      Valor del campo.
   * @return bool|string              Puede devolver TRUE si el valor pasó la validación,
   *                                  de lo contrario una cadena que representará el
   *                                  nombre del error.
   */
  public function check($value){
  	$model = $this->getAttr('model');
  	if(is_string($model)){
  		$model = new $model;
  	}
  	if($model instanceof Model){
  		$record = $model->where($this->getAttr('check'),$value)->first();
  		if($record){
  			if($this->hasRelations($record)){
  				return 'model.delete.relation';
  			}
  		}
  	}
  	return true;
  }
  /**
   * Determina si el modelo suministrado tiene relaciones.
   * @param  [type]  $model Instancia del modelo.
   * @return boolean
   */
  protected function hasRelations(EModel $model){
  	if($model instanceof Model && is_array($model->rels)){
  		foreach($model->rels as $rel){
  			$rel = $model->$rel();
  			if(($rel instanceof HasMany || $rel instanceof HasOne) && $rel->first()){
					return true;
				}
  		}
  		return false;
  	}
  	$class = get_class($model);
  	$reflection = new ReflectionClass($class);
  	foreach($reflection->getMethods() as $method){
  		if($class == $method->class){
  			try {
  				$rel = call_user_func([$model,$method->name]);
  				if(($rel instanceof HasMany || $rel instanceof HasOne) && $rel->first()){
  					return true;
  				}
  			} catch (Exception $e) {
  				
  			}
  		}
  	}
  	return false;
  }
}