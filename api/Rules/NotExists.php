<?php namespace Api\Rules;
use Illuminate\Database\Eloquent\Model as EModel,
		Api\Database\Eloquent\Model,
		Illuminate\Database\Eloquent\Relations\HasMany,
		Illuminate\Database\Eloquent\Relations\HasOne,
		ReflectionClass,
		Exception;
/**
 * Valida que un valor no exista en el campo de una tabla.
 */
class NotExists extends Rule
{
	/**
	 * Método ejecutado al iniciar la instancia.
	 * Establece los atributos necesarios para la correcta validación.
	 * 
	 * @param  string  $model  Nombre del modelo.
	 * @param  integer $check  Nombre del campo de la búsqueda.
	 * @return 
	 */
	protected function boot($table,$field='id'){
		$this->setAttr('table',$table);
		$this->setAttr('field',$field);
	}

  /**
   * Determina si el valor pasa la validación.
   * @param  string       $attribute  Nombre del campo.
   * @param  mixed        $value      Valor del campo.
   * @return bool|string              Puede devolver TRUE si el valor pasó la validación,
   *                                  de lo contrario una cadena que representará el
   *                                  nombre del error.
   */
  public function check($value){
  	$count = DB::table($this->getAttr('table'))
				       ->where($this->getAttr('field'), '=', $value)
				       ->count();
  	return $count > 1
  				 ? 'validation.not_exists'
  				 : true;
  }
}