<?php namespace Api\Rules;
use Illuminate\Support\Facades\Hash,
		Illuminate\Database\Eloquent\Model,
		Illuminate\Support\Facades\DB,
		App\Models\User\Log;
/**
 * Valida que la dirección suministrada sea un vínculo de Youtube.
 */
class Password extends Rule
{
	/**
	 * Método ejecutado al iniciar la instancia.
	 * Establece los atributos necesarios para la correcta validación.
	 * 
	 * @param  string  $model  Nombre del modelo.
	 * @param  integer $check  Nombre del campo de la búsqueda.
	 * @return 
	 */
	protected function boot($table,
													$identifier='email',
													$column='password',
													$log=true){
		$this->setAttr('identifier',$identifier);
		$this->setAttr('column',$column);
		$this->setAttr('log',$log);
		if($table instanceof Model){
			$this->setAttr('model',$table);
		}else{
			$record = DB::table($table)
									->where($identifier,app('request')
									->input($identifier,''))
									->first();
			if($record){
				$this->setAttr('model',$record);
			}
		}
	}

  /**
   * Determina si el valor pasa la validación.
   * @param  string       $attribute  Nombre del campo.
   * @param  mixed        $value      Valor del campo.
   * @return bool|string              Puede devolver TRUE si el valor pasó la validación,
   *                                  de lo contrario una cadena que representará el
   *                                  nombre del error.
   */
  public function check($value){
  	$model = $this->getAttr('model');
	$column = $this->getAttr('column');
  	if($model){
  		if(Hash::check($value,$model->{$column})){
  			return true;
  		}
  		if($this->getAttr('log',false,'bool')){
  			$log = new Log();
	  		$log->user_id = $model->id;
	  		$log->code = 'signin.fail';
	  		$log->save();
  		}
  	}
  	return 'password.compare';
  }
}