<?php namespace Api\Rules;
use Api\Options;
use Illuminate\Contracts\Validation\Rule as RuleContract;
/**
 * Clase base para las reglas de validación personalizadas del sistema.
 */
abstract class Rule implements RuleContract{
	/**
	 * Atributos de la instancia.
	 * @var Api\Options
	 */
	protected $_attrs;

	/**
	 * Ǘltimo mensaje de error.
	 * @var string
	 */
	protected $_message;

	/**
	 * Constructor.Inicializa la instancia.
	 */
	public function __construct(){
		//Iniciamos el método "boot"
		if(method_exists($this, 'boot')){
			$args = func_get_args();
			empty($args)
			? call_user_func([$this,'boot'])
			: call_user_func_array([$this,'boot'], $args);
		}
	}

	public function __get($name){
		return $this->attrs()->has($name)
					 ? $this->attrs()->get($name)
					 : null;
	}

	public function __set($name,$value){
		$this->attrs()->set($name,$value);
	}

  /**
   * Determina si el valor pasa la validación.
   * @param  string  $attribute  Nombre del campo.
   * @param  mixed   $value      Valor del campo.
   * @return bool    Para verificar si el valor pasa la validación, llamará al método 
   *                 check($attr,$value). Si el resultado de este es TRUE 
   *                 devolverá verdadero, de lo contrario devolverá FALSE.
   */
	public function passes($attribute, $value){
  	$this->setAttr('field',$attribute)
  			 ->setAttr('attribute',$attribute)
  			 ->setAttr('value',$value);

  	if(method_exists($this, 'beforeCheck')){
  		if($this->onCheck() === false){
  			return false;
  		}
  	}
  	$result = $this->check($value);
  	if($result === true){
  		return true;
  	}
  	$this->_message = $result;
  	return false;
  }
  /**
   * Devuelve el mensaje de error de la validación.
   *
   * @return string
   */
  public function message()
  {
  	$message =  $this->_message;
  	if(empty($message)){
  		$message = 'error';
		}
		$vars = [];
		foreach($this->allAttrs() as $name => $value){
    	if(is_array($value)){
    		$value = implode(', ',$value);
			}
			$vars[$name] = is_scalar($value) ? $value : '[object]';
		}
  	$message = trans('validation.'.$message,$vars);
	  foreach($this->allAttrs() as $name => $value){
		if (!is_object($value) && !is_array($value)) {
			$message = str_replace('{{'.$name.'}}', $value, $message);
		}
	}
    return $message;
  }

  public function attrs(){
  	if(!$this->_attrs){
  		$this->_attrs = new Options;
  	}
  	return $this->_attrs;
  }

  /**
   * Devuelve el valor de un atributo.
   * @param  string $name    Nombre del atributo.
   * @param  mixed  $default Valor por defecto.
   * @param  string $cast    Tipo de datos.
   * @return mixed
   */
	public function getAttr($name,$default=null,$cast=null){
		return $this->attrs()->get($name,$default,$cast);
	}

	/**
	 * Establece el valor de un atributo.
	 * @param   string $name  Nombre del atributo.
	 * @param   mixed  $value Valor.
	 * @return  Api\Rules\Rule
	 */
	public function setAttr($name,$value){
		$this->attrs()->set($name,$value);
		return $this;
	}
	/**
	 * Determina la existencia de un atributo.
	 * @param   string  $name  Nombre del atributo.
	 * @return  boolean
	 */
	public function hasAttr($name){
		return $this->attrs()->has($name);
	}

	/**
	 * Devuelve los valores de todos los atributos de la instancia.
	 * @return array
	 */
	public function allAttrs(){
		return $this->attrs()->all();
	}

  /**
   * Determina si el valor pasa la validación.
   * @param  mixed        $value      Valor del campo.
   * @return bool|string              Puede devolver TRUE si el valor pasó la validación,
   *                                  de lo contrario una cadena que representará el
   *                                  nombre del error.
   */
	abstract public function check($value);
}