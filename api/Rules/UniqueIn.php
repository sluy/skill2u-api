<?php namespace Api\Rules;
use Illuminate\Database\Eloquent\Model;
/**
 * Valida que un valor sea único en los registros de una tabla en la base de datos.
 */
class UniqueIn extends Rule
{

	/**
	 * Método ejecutado al iniciar la instancia.
	 * Establece los atributos necesarios para la correcta validación.
	 * @param  string $table  Nombre de la tabla.
	 * @param  array  $static Matriz con los campos y valores fijos.
	 * @param  array  $ignore Matriz con los campos y valores en cuyo caso de coincidir
	 *                        con un registro existente, se ignorará el mismo.
	 * @param  string $check  Nombre del campo a validar. Por defecto tomará el id.
	 * @return void
	 */
	protected function boot($table=null,$static=[],$ignore=null,$check=null){
		if($table instanceof Model){
			$table = $table->getTable();
		}
		$this->setAttr('table',$table)
				 ->setAttr('static',$static)
				 ->setAttr('ignore',$ignore)
				 ->setAttr('check',$check);
	}

	protected function messages(){
		return ['unique_in'=>'El valor ya está registrado.'];
	}
  /**
   * Determina si el valor pasa la validación.
   * @param  mixed        $value      Valor del campo.
   * @return bool|string              Puede devolver TRUE si el valor pasó la validación,
   *                                  de lo contrario una cadena que representará el
   *                                  nombre del error.
   */
  public function check($value){
  	$field = $this->getAttr('check',$this->getAttr('field'),'string');
  	if(strpos($field, '.') !== -1){
  		$field = @end(explode('.',$field));
  	}
  	$ignore = $this->getAttr('ignore',[],'array');
  	$where = $this->getAttr('static',[],'array');
  	$where[$field] = $value;
  	$query = app('db')->table($this->getAttr('table','','string'));
  	foreach($where as $field => $value){
  		$query->where($field,$value);
  	}
  	$rows = $query->get();
  	if($rows){
  		foreach($rows as $row){
  			$exists = true;
  			foreach($ignore as $name => $value){
  				if($value !== null && property_exists($row, $name) && $row->{$name} == $value){
  					$exists = false;
  					break;
  				}
  			}
  			if($exists){
  				return 'unique_in';
  			}
  		}
  	}
  	return true;
  }
}