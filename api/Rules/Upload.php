<?php namespace Api\Rules;
use Illuminate\Http\UploadedFile;
/**
 * Valida que el archivo suministrado sea un upload válido.
 */
class Upload extends Rule
{
	/**
	 * Método ejecutado al iniciar la instancia.
	 * Establece los atributos necesarios para la correcta validación.
	 * 
	 * @param  string  $mimes Mimes válidos.
	 * @param  integer $size  Tamaño máximo (en MB) del upload.
	 * @return 
	 */
	protected function boot($mimes=null,$size=null,$strSize=255){
		$this->setAttr('mime',$mimes)
				 ->setAttr('size',$size)
				 ->setAttr('strSize',$size);
	}

	/**
	 * Mensajes de error.
	 * @return array
	 */
	protected function messages(){
		return [
			'size'          => 'El archivo debe ser menor a {{size}} MB',
			'mime'          => 'El tipo de archivo debe ser "{{mime}}."',
			'route_length'  => 'La cadena con la ruta del archivo debe ser menor o igual a {{strSize}} caracteres.',
			'route_invalid' => 'La ruta {{value}} no existe.'
		];
	}

  /**
   * Determina si el valor pasa la validación.
   * @param  string       $attribute  Nombre del campo.
   * @param  mixed        $value      Valor del campo.
   * @return bool|string              Puede devolver TRUE si el valor pasó la validación,
   *                                  de lo contrario una cadena que representará el
   *                                  nombre del error.
   */
  public function check($value){
  	//Validamos la ruta suministrada:
  	if(is_string($value) && !empty($value)){
  		//Si no existe el archivo
  		if(!app('Api\Api')->upload->exists($value)){
  			return 'upload.invalid';
  		}
  		//Si la longitud de la ruta es mayor a la especificada
  		if(strlen($value) > $this->getAttr('strSize',255,'integer')){
  			return 'upload.length';
  		}
  		return true;
  	}

  	if($value instanceof UploadedFile){
  		if($this->checkMime($value->getMimeType())){
  			if($this->checkSize($value->getSize())){
  				return true;
  			}
  			return 'upload.size';
  		}
  	}
  	return 'upload.mime';
  }

  /**
   * Verifica que el mime suministrado sea válido.
   * @param  string  $mime Mime a validar.
   * @return boolean
   */
  protected function checkMime($mime){
  	$mime = is_string($mime) ? trim(strtolower($mime)) : '';
  	$valids = strtolower(trim($this->getAttr('mime','','string')));
  	//No hay que validar
  	if(empty($valids)){
  		return true;
  	}
  	//Hay que validad y no se definió el mime
		if(empty($mime)){
			return false;
		}
		//Recorremos los mimes válidos
		foreach(explode(',',$valids) as $valid){
			if($mime >= $valid && starts_with($mime,$valid)){
				return true;
			}
		}
  	return false;
  }
  /**
   * Verifica que el tamaño del archivo sea válido.
   * @param  integer $size Tamaño (en MB) del archivo.
   * @return boolean
   */
  protected function checkSize($size){
  	$valid = $this->getAttr('size',0,'integer');
  	if($valid < 1){
  		$mime  = strtolower(trim($this->getAttr('mime','','string')));
  		$valid = $mime == 'image'
  						 ? 2
  						 : 64;
  		$this->setAttr('size',$valid);
  	}
  	$validBytes = ($valid * 1024) * 1024;
  	return ($validBytes >= $size);
  }
}