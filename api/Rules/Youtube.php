<?php namespace Api\Rules;
/**
 * Valida que la dirección suministrada sea un vínculo de Youtube.
 */
class Youtube extends Rule
{
	/**
	 * Mensajes de error.
	 * @return array
	 */
	protected function messages(){
		return ['El valor suministrado no es un vínculo a youtube válido.'];
	}

  /**
   * Determina si el valor pasa la validación.
   * @param  string       $attribute  Nombre del campo.
   * @param  mixed        $value      Valor del campo.
   * @return bool|string              Puede devolver TRUE si el valor pasó la validación,
   *                                  de lo contrario una cadena que representará el
   *                                  nombre del error.
   */
  public function check($value){
  	$regex_pattern = "/(youtube.com|youtu.be)\/(watch)?(\?v=)?(\S+)?/";
		$match;
		if(is_string($value) && !empty($value) && preg_match($regex_pattern, $value, $match)){
			return true;  
		}
		return 'youtube';
  }
}