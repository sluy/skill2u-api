<?php namespace Api\Traits;
use Closure,
		Api\Contracts\Messageable as MessageableContract;
/**
 * Trait para iterar lotes de datos.
 */
trait Batch{

	/**
	 * Ejecuta un método de la instancia actual repetidas veces.
	 * 
	 * @param  string|Closure $fn              Nombre del método o instancia de la función a ejecutar.
	 * @param  array          $data            Datos a iterar.
	 * @param  Closure        $paramsFormatter Función para dar formato a los parámetros antes de 
	 *                                         ser enviados al método a ejecutar.
	 * @return array
	 */
	protected function batch($fn,array $data,Closure $paramsFormatter=null){
		if($this instanceof MessageableContract){
			$this->clearMessage();
		}
		$params = func_get_args();
		unset($params[0]);
		unset($params[1]);
		unset($params[2]);
		$params = array_values($params);
		$items = [];
		$errors = [];
		foreach($data as $k => $v){
			$newParams = array_merge($params,[$v,$k]);
			if($paramsFormatter){
				$formatted = call_user_func_array($paramsFormatter, $newParams);
				if(is_array($formatted) && !empty($formatted)){
					$newParams = $formatted;
				}
			}
			if(is_string($fn)){
				$items[$k] = call_user_func_array([$this,$fn], $newParams);
			}else if($fn instanceof Closure){
				$items[$k] = call_user_func_array($fn, $pass);
			}
			if($this instanceof MessageableContract){
				if($this->hasMessage()){
					$errors[$k] = $this->getMessage();
				}
				$this->clearMessage();
			}
		}
		if($errors && $this instanceof MessageableContract){
			$this->setMessage($errors);
		}
		return $items;
	}

}