<?php namespace Api\Traits;
use Closure,
		Api\Format;

trait Eventable {	
	protected $events = [];

	/**
	 * Dispara los eventos suministrados.
	 * @param  string|array $events    Matriz con los nombres de los eventos.
	 * @param  array        $arguments Argumentos a suministrar a los callbacks.
	 * @param  string       $prefix    Prefijo por agregar a los nombres de los eventos.
	 * @param  string       $suffix    Susfijo por agregar a los nombres de los eventos.
	 * @return boolean|Illuminate\Http\Response Si alguno de los callbacks asociados al evento devuelve
	 *                                          false o la instancia de Illuminate\Http\Response, devolverá
	 *                                          su respuesta (y detentrá los otros procesos en cola), de 
	 *                                          lo contrario ejecutará todos los callbacks y devolverá
	 *                                          true.
	 */
	public function trigger($events,$arguments=null,$prefix=null,$suffix=null){
		if(is_string($events) && !empty($events)){
			$events = array_map('trim',explode(',',$events));
		}
		$events = Format::cast('array',$events);
		$arguments =Format::cast('array',$arguments);
		if(in_array('update',$events) || in_array('create',$events)){
			$events[] = 'save';
		}


		foreach($events as $name){
			$name = strtolower((is_string($prefix)?$prefix:'') . $name . (is_string($suffix)?$suffix:''));
			$r = null;
			if(method_exists($this, $name)){
				$r = !empty($arguments)
						 ? call_user_func_array([$this,$name], $arguments)
						 : call_user_func([$this,$name]);
			}
			if($r === false || $r instanceof Response){
				return $r;
			}
			if(isset($this->events[$name])){
				foreach($this->events[$name] as $callback){
					$r = !empty($arguments)
							 ? call_user_func_array([$this,$name], $arguments)
							 : call_user_func([$this,$name]);
					if($r  === false || $r instanceof Response){
						return $r;
					}
				}
			}
		}
		return true;
	}

	public function on($eventName,Closure $callback){
		if(is_string($eventName) && !empty($eventName)){
			if(!isset($this->events[$eventName])){
				$this->events[$eventName] = [];
			}
			$this->events[$eventName][] = $callback;
		}

		return $this; 
	}

}