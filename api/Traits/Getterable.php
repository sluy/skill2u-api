<?php namespace Api\Traits;

trait Getterable {

	public function getProperty($name){
		if(is_string($name) && strtolower($name) != 'property' && method_exists($this, 'get'.$name)){
			return call_user_func([$this,'get'.$name]);
		}
		return null;
	}

	public function __get($name){
		return $this->getProperty($name);
	}
}