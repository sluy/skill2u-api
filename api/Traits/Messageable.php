<?php namespace Api\Traits;
/**
 * Trait para el manejo de mensajes de error dentro de la instancia.
 */
trait Messageable {
	/**
	 * Mensaje de la instancia.
	 * @var Array|null
	 */
	protected $_message;
	/**
	 * Establece el mensaje de la instancia.
	 * @param   string|array             $message Mensaje.
	 * @return  Api\Api\Module
	 */
	public function setMessage($message){
		if(!is_array($message)){
			$message = [$message];
		}
		$this->_message = $message;
		return $this;
	}
	/**
	 * Determina si existe un mensaje en la instancia.
	 * @return boolean
	 */
	public function hasMessage(){
		return is_array($this->_message) && !empty($this->_message);
	}
	/**
	 * Devuelve el último mensaje de la instancia.
	 * @return array|null
	 */
	public function getMessage(){
		return $this->_message;
	}

	/**
	 * Limpia el mensaje de la instancia.
	 * @return Api\Api\Module
	 */
	public function clearMessage(){
		$this->_message = null;
	}
}