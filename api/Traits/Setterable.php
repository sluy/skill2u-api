<?php namespace Api\Traits;

trait Setterable {

	public function setProperty($name,$value){
		if(is_string($name) && strtolower($name) != 'property' && method_exists($this, 'set'.$name)){
			return call_user_func([$this,'get'.$name],[$value]);
		}
		return null;
	}

	public function __set($name,$value){
		return $this->setProperty($name,$value);
	}
}