<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CheckPeriods extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:periods';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Comprueba los períodos del sistema.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    	app('Api\Api')->company->checkAllPeriods();

    	//$path = storage_path() . "/logs/schedule.log";
    	//file_put_contents($path, date('Y-m-d H:i:s').PHP_EOL , FILE_APPEND | LOCK_EX);
    }
}
