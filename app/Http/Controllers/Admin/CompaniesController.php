<?php namespace App\Http\Controllers\Admin;
use Carbon\Carbon,
		Illuminate\Support\Facades\Hash;
/**
 * Controlador para la administración de compañías en el sistema.
 */
class CompaniesController extends Controller
{
	public $with = [];

	protected $baseLevels = [
		[
			'title'   => 'Bronce',
			'options' => '{"info":"Primer nivel","color":"red","icon":"fa fa-user"}',
		],
		[
			'title'   => 'Plata',
			'options' => '{"info":"Segundo nivel","color":"red","icon":"fa fa-user"}',
		],
		[
			'title'   => 'Oro',
			'options' => '{"info":"Tercer nivel","color":"red","icon":"fa fa-user"}',
		]
	];

	protected $defaultOptions = [
		'info' => '',
		'user_eval_weight'  => '20.00',
		'notify_users'      => true,
		'notify_courses'    => true,
		'notify_comments'   => true,
		'notify_news'       => true
	];

	public function beforeSave($company,$ori){
		foreach($this->defaultOptions as $option => $default){
			if(!$company->getOptions()->has($option)){
				$company->getOptions()->set($option,$default);
			}
		}
	}

	public function afterCreate($company){
		foreach($this->baseLevels as $order => $cfg){
			$level = $this->model('App\Models\Level');
			$level->company_id = $company->id;
			$level->order = $order;
			foreach($cfg as $key => $value){
				$level->{$key} = $value;
			}
			$level->save();
		}
		$data = $this->input('admin');
		$user = null;
		if(isset($data['id'])){
			$user = $this->model('App\Models\User')->find($data['id']);
			$user->company_id = $company->id;
			$user->save();
		}else{
			$user = $this->model('App\Models\User');
			$originalPassword = isset($data['password']) && !empty($data['password'])
												? $data['password']
												: $user->generatePassword();
			$user->company_id = $company->id;
			$user->company_admin = true;
			$user->system_admin  = false;
			$user->level_id      = 0;
			$user->email = $data['email'];
			$user->password = Hash::make($originalPassword);
			$user->options = ['allow_multiple_sessions' => false];
			$user->save();
			$user->password = $originalPassword;
			$profile = $this->model('App\Models\User\Profile');
			$profile->user_id = $user->id;
			$profile->birthday = '1900-01-01';
			$profile->social   = [];
			$profile->save();
		}
		if($user){
			$user->profile;
			$user = $user->toArray();
			$this->api('notify')->success(
					trans('notification.company_register.title'),
					trans('notification.company_register.message'),
					[
						'user'     => $user,
						'company'  => $company
					],
					true);
		}
	}

	public function onCreateRules(){
		return [
			'admin'                => 'required|array',
      'admin.email'          => [ 'email',
    							      				  'max:255',
    										      	  $this->rule('unique','users','email')
    											      ],
      'admin.password'       => 'min:8',
      'admin.id'             => ['integer',
    														 $this->rule('exists','users','id')->where(
    														 function($q){
    														 	return $q->where('company_id','<',1);
    														 })]
		];
	}

	public function onSaveRules($id=null){
		return [
			'name'   =>[
										'required',
										'string',
										'max:64',
									],
			'logo'                     => [$this->rule('upload','image',1024)],
			'options'                  => 'array',
			'options.info'             => 'string|max:500',
			'options.user_eval_weight' => 'numeric|max:100',
		  "options.notify_users"     => 'boolean',
			"options.notify_courses"   => 'boolean',
			"options.notify_comments"  => 'boolean',
			"options.notify_news"      => 'boolean'
		];
	}
}
