<?php namespace App\Http\Controllers\Admin;
use Api\Routing\Controller\CRUD;
/**
 * Controlador base para el backend
 */
class Controller extends CRUD {
	public $crossLookup = false;
	public $forceKeySearch = false;
}