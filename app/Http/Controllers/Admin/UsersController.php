<?php namespace App\Http\Controllers\Admin;
use Illuminate\Support\Facades\Hash;
/**
 * Controlador para la administración de usuarios en el sistema.
 */
class UsersController extends Controller
{
	public $with = [];

	protected $originalPassword = null;

	public function beforeDelete($model){
		if($model->unique_company_admin || $model->unique_system_admin){
			return $this->forbidden();
		}
	}

	public function beforeCreate($model){
		if(empty($model->password)){
			$model->password = $model->generatePassword();
		}
		$this->originalPassword = $model->password;
	}

	public function beforeUpdate($model,$ori){
		//Si ya está establecida la compañía y se cambió por otra, volvemos a la original.
		if($ori->company_id != 0 && $model->company_id != $ori->company_id){
			$model->company_id = $ori->company_id;
		}
	}

	public function beforeSave($model,$ori){
		if($model->company_id){
			if(!$model->level_id){
				$lvl = $model->company->levels()
						        ->orderBy('order')
						        ->first();
				$model->level_id = $lvl->id;
			}
		}
		if($model->password != $ori->password){
			$model->password = Hash::make($model->password);
		}
		if(!empty($model->id)){
			//Nos aseguramos que no se quite el estado de administrador de la compañía si
			//el usuario es el único.
			if($ori->company_admin && !$model->company_admin && $ori->unique_company_admin){
				$model->company_admin = true;
			}
			//Nos aseguramos que no se quite el estado de administrador del sistema si
			//el usuario es el único.
			if($ori->system_admin && !$model->system_admin && $ori->unique_system_admin){
				$model->system_admin = true;
			}
		}
	}

	public function afterCreate($model){
		$profile = $this->model('App\Models\User\Profile');
		$profile->user_id  = $model->id;
		$profile->birthday = '1900-01-01';
		$profile->social   = [];
		$profile->save();
		$user = $model->toArray();
		$user['password'] = $this->originalPassword;
		$user['profile'] = $profile->toArray();
		if(!$model->company_id){
			$this->api('notify')->success(
						trans('notification.sysop_register.title'),
						trans('notification.sysop_register.message'),
						[
							'user'     => $user
						],
						true);
			return;
		}
		$company = $model->company;
		$title = trans('notification.user_register.title');
		$message = trans('notification.user_register.message');
		$titleview = $company->views()->where('name','user.new.title')->first();
		$messageview = $company->views()->where('name','user.new.message')->first();
		if($titleview){
			$title = $titleview->value;
		}
		if($messageview){
			$message = $messageview->value;
		}
		$this->api('notify')->success(
			$title,
			$message,
			[
				'user'     => $user,
				'company'  => $company
			],
			true);
	}


	public function onSaveRules($id=null){
		$rules = array(
      'email'          => [ 'required',
    											  'email',
    											  'max:255',
    											  $this->rule('unique','users','email')->where(function($q) use($id){{
    											  	return $q->where('id','!=',$id);
    											  }})
    											],
    	'company_id'     => ['integer',$this->rule('exists','companies','id')],
    	'level_id'       => ['integer'],
      'password'       => 'min:8',
      'options'        => 'array',
      'system_admin'   => 'boolean',
      'company_admin'  => 'boolean'
		);
		$company = null;
		if($this->request()->has('company_id')){
			$company = $this->model()->find($this->input('company_id'));
		}else if($id){
			$user = $this->model('App\Models\User')->find($id);
			if($user){
				$company = $user->company;
			}
		}
		if($this->request()->has('level_id') && $company){
			$rules['level_id'][] = $this->rule('exists','levels','id')->where(
															 function($q) use($company){
																	return $q->where('company_id',$company->id);
															 });
		}
		return $rules;
	}


	/**
	 * Cambia los datos del perfíl de la cuenta autenticada.
	 * @return Illuminate\Http\Response
	 */
	public function profileChange($id){
		$this->check('profileChange');
		$profile = $this->model()->findOrFail($id)->profile;
		dd($profile->getFillable());
		foreach($profile->getFillable() as $field){
			$value = ($field == 'photo')
							 ? $this->upload->fromRequest('photo')
							 : $this->input($field,'');
			if($field == 'user_id' || empty($value)){
				continue;
			}
			$profile->{$field} = $value;
		}
		$profile->save();
		return $this->response->updated($profile);
	}


	/**
	 * Reglas al cambiar el perfil.
	 * @return array
	 */
	public function onProfileChangeRules(){
		return array(
      'name'               => 'max:64',
      'lastname'           => 'max:64',
      'phone'              => 'max:64',
      'birthday'           => 'date',
      'photo'              => $this->rule('upload','image',1024),
      'bio'                => '',
      'sex'                => ['max:1',$this->rule('in',['m','f'])],
      'social'             => 'array',
      'social.facebook'    => 'url|max:255',
      'social.twitter'     => 'url|max:255',
      'social.skype'       => 'max:255'
  	);
	}
}
