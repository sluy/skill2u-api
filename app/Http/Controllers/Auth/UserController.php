<?php namespace App\Http\Controllers\Auth;
use Illuminate\Support\Facades\Hash,
		App\Http\Controllers\Controller,
		App\Models\User\Session,
		App\Models\User\Recovery;
/**
 * Controlador dedicado a la autenticación del usuario y cambio de los 
 * datos asociados al mismo.
 */
class UserController extends Controller{
	public $includeCompany = false;
	/**
	 * Inicia sesión en el sistema.
	 * @return Illuminate\Http\Response
	 */
	public function signin(){
		$this->check('signin');
		$this->auth->user($this->input('email'));
		return $this->response->ok(['token'=>$this->auth->session()->token]);
	}
	/**
	 * Cierra la sesión de la cuenta autenticada.
	 * @return Illuminate\Http\Response
	 */
	public function signout(){
		$user = $this->user();
		$this->auth->session(false);
		//Si se deben cerrar todas las sesiones.
		if($this->request()->has('all') && 
			 (strlen($this->input('all')) === 0 || $this->input('all',[],'boolean'))){
			Session::where('user_id',$user->id)->delete();
		}
		return $this->response->deleted();
	}
	/**
	 * Devuelve la información de la cuenta autenticada.
	 * @return Illuminate\Http\Response
	 */
	public function current(){
		$user = $this->user();
		$user->profile;
		$user->level;

		foreach($user->segmentations as $k => $usegmentation) {
			$user->segmentations[$k]->segmentation;
		}
		
		foreach($user->performances as $k => $uperformance){
			$user->performances[$k]->performance;
		}
		return $user;
		/*
		$c = $this->user();
		$c->profile;
		$c = $c->toArray();
		$srank = 1;
		$rank  = 1;
		foreach($this->company()->users as $user){
			if($user->id == $c['id']){
				continue;
			}
			if($user->performance > $c['performance']){
				$rank++;
			}
			if($user->system_performance > $c['system_performance']){
				$srank++;
			}
		}
		$c['system_rank'] = $srank;
		$c['rank'] = $rank;
		return $this->response->ok($c);*/
	}
	/**
	 * Cambia la contraseña de la cuenta autenticada.
	 * @return Illuminate\Http\Response
	 */
	public function passwordChange(){
		$this->check('passwordChange');
		$user = $this->auth->user();
		$user->password = Hash::make($this->input('password'));
		$user->save();
		$this->auth->log('data.password.success');
		return $this->response->updated();
	}
	/**
	 * Comprueba que una recuperación existe.
	 *
	 * @param  string $token
	 * @return void
	 */
	public function recoveryCheck($token) {
		if (is_string($token) && Recovery::where('token',$token)->first()) {
			return $this->response->ok();
		}
		return $this->response->notfound();
	}
	/**
	 * Inicio del proceso de recuperación.
	 *
	 * @return void
	 */
	public function recoveryStart(){
		$this->check('recoveryStart');
		$user = $this->model()->where('email', $this->request()->input('email'))->first();
		if(!$user) {
			return $this->response->notfound();
		}
		$recovery = new Recovery;
		$recovery->user_id = $user->id;
		while(1) {
			$recovery->token = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(64/strlen($x)) )),1,64);
			if (!Recovery::where('token', $recovery->token)->first()) {
				break;
			}
		}
		$message = trans('notification.user_register.message');
		$this->auth->log('data.recovery.start.success', null, null, $user);
		
		$n = $this->api('notify')->success(
			 trans('notification.user_recovery_start.title'),
			 trans('notification.user_recovery_start.message', ['token'=>$recovery->token]),
			[
				'user'     => $user,
				'company'  => $user->company
			],
			true);
		dd($n);
		return $this->response->created();
	}
	/**
	 * Finalización del proceso de recuperación.
	 *
	 * @param string $token
	 * @return void
	 */
	public function recoveryFinish($token) {
		if (is_string($token) && !($recovery = Recovery::where('token',$token)->first())) {
			return $this->response->notfound();
		}
		$this->check('recoveryFinish');
		$user = $recovery->user;
		$user->password = Hash::make($this->input('password'));
		$user->save();
		$this->auth->log('data.recovery.fininish.success', null, null, $user);
		$recovery->delete();
		//Enviar por correo
				$this->api('notify')->success(
			 trans('notification.user_recovery_finish.title'),
			 trans('notification.user_recovery_finish.message'),
			[
				'user'     => $user,
				'company'  => $user->company
			],
			true);
		return $this->response->updated();
	}

	/**
	 * Cambia los datos del perfíl de la cuenta autenticada.
	 * @return Illuminate\Http\Response
	 */
	public function profileChange(){
		$this->check('profileChange');
		$profile = $this->auth->user()->profile;
		foreach($profile->getFillable() as $field){
			$value = ($field == 'photo')
							 ? $this->upload->fromRequest('photo')
							 : $this->input($field,'');
			if($field == 'user_id' || empty($value)){
				continue;
			}
			$profile->{$field} = $value;
		}
		$profile->save();
		$this->auth->log('data.profile.success');
		return $this->response->updated($profile);
	}
	/**
	 * Cambia las opciones de la cuenta autenticada.
	 * @return Illuminate\Http\Response
	 */
	public function optionsChange(){
		$this->check('optionsChange');
		$user = $this->auth->user();
		$user->options = $this->request()->all();
		$user->save();
		$this->auth->log('data.options.success');
		return $this->response->updated($user->options);
	}

	/**
	 * Reglas al inciar sesión.
	 * @return array
	 */
	public function onSigninRules(){
		return array(
			'email'    => 'required|email|exists:users',
	    'password' => ['required',$this->rule('password','users','email')]
    );
	}
	/**
	 * Reglas al cambiar la contraseña.
	 * @return array
	 */
	public function onPasswordChangeRules(){
		return array(
			'current'     => ['required',$this->rule('password',$this->auth->user())],
      'password'    => 'required|min:8|different:current',
		);
	}

	public function onRecoveryStartRules() {
		return array ('email' => ['required', 'email']);
	}

	public function onRecoveryFinishRules() {
		return array ('password' => 'required|min:8');
	}


	/**
	 * Reglas al cambiar el perfil.
	 * @return array
	 */
	public function onProfileChangeRules(){
		return array(
      'name'               => 'max:64',
      'lastname'           => 'max:64',
      'phone'              => 'max:64',
      'birthday'           => 'date',
      'photo'              => $this->rule('upload','image',1),
      'bio'                => '',
      'sex'                => ['max:1',$this->rule('in',['m','f'])],
      'social'             => 'array',
      'social.facebook'    => 'url|max:255',
      'social.twitter'     => 'url|max:255',
      'social.skype'       => 'max:255'
  	);
	}
	/**
	 * Reglas al cambiar las opciones de la cuenta.
	 * @return array
	 */
	public function onOptionsChange(){
		return array(
			'allow_multiple_sessions' => 'boolean',
			'locale'                  => $this->rule('locale'),
		);
	}
}

