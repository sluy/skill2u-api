<?php namespace App\Http\Controllers\Backend;
use Carbon\Carbon;
/**
 * Controlador para la administración de banners de la empresa.
 */
class BannersController extends Controller
{
    /**
	 * Reglas de validación al guardar un banner.
	 * @param  integer|null $id Id del banner. Será NULL si se trata de la creación
	 *                          de un nuevo registro.
	 * @return array
	 */
	protected function onSaveRules($id=null){
		$rules = [
  		'location'  => ['string',$this->rule('in',['login','profile','dashboard', 'course', 'lib', 'wall'])],
        'type'     => ['required', 'string', $this->rule('in',['picture','youtube','video'])],
  		'resource' => ''
		];
		switch ($this->input('type')) {
			case 'picture':
				$rules['resource'] = ['required',$this->rule('upload','image',1024,255)];
				break;
			case 'video':
				$rules['resource'] = ['required',$this->rule('upload','video',1024,255)];
				break;
			case 'youtube':
				$rules['resource'] = ['required','string','url','max:255',$this->rule('youtube')];
				break;
		}
		return $rules;
	}
}