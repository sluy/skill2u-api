<?php namespace App\Http\Controllers\Backend;
/**
 * Controlador para la administración de categorías de la compañía activa.
 */
class CategoriesController extends Controller
{
	public $with         = [];

	public function beforeDelete($model){
		//Eliminamos las relaciones.
		foreach(['libs', 'posts'] as $name) {
			foreach($model->{$name} as $rel) {
				$rel->delete();
			}
		}
	}

	protected function onSaveRules($id=null){

		return [
			'title'   =>[
										'required',
										'string',
										'max:64',
										$this->rule('unique','categories')->where(function($query) use($id){
											return $query->where('company_id',$this->company()->id)
																	 ->where('id','!=',$id);
										})
										/*
										$this->rule(
											'uniqueIn',
											$this->model(),
											['company_id'=>$this->company()->id],
											['id' => $id]
										)*/
									],
			'info'    => 'string|min:5',
			'picture' => [$this->rule('upload','image',1024)]
		];
	}
}
