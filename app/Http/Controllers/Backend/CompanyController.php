<?php namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;
/**
 * Controlador para el manejo de la compañía actual.
 */
class CompanyController extends Controller
{
	/**
	 * Campos protegidos.
	 * @var array
	 */
	public $guard = ['status'];

	/**
	 * Relaciones a mostrar
	 * @var array
	 */
	public $with = [];

	/**
	 * Constructor.
	 * Configura e inicializa la instancia.
	 */
	public function __construct(){
		$this->model = $this->company();
	}

	/**
	 * Reglas de validación al actualizar el registro.
	 * @param  integer $id Id del registro.
	 * @return array
	 */
	protected function onUpdateRules(){
		return [
			'status'       => 'boolean',
			'name'         => 'required|string|max:64',
      'logo'         => [$this->rule('upload','image',1024)],
			'options'      => 'array',
      'options.info' => 'string|max:500',
      'options.copyright' => 'string',
			'options.user_eval_weight' => 'numeric|max:100',
			'options.auto_performance' => 'boolean',
			'options.use_user_eval' => 'boolean',
			'options.timezone' => 'in:' . implode(',', timezone_identifiers_list())
		];
	}
}