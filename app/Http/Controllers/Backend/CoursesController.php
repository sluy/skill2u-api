<?php namespace App\Http\Controllers\Backend;
use Carbon\Carbon;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
/**
 * Controlador para la administración de cursos de la compañía activa.
 */
class CoursesController extends Controller
{
	public $with         = [];

  public function singleXls($id) {
    $course = $this->company()->courses()->where('id', $id)->first();
    dd($course);
  }

  public function xls() {
    $spreadsheet = new Spreadsheet();
    $excelWriter = new Xls($spreadsheet);
    $spreadsheet->setActiveSheetIndex(0);
    $activeSheet = $spreadsheet->getActiveSheet();
    $titles = ['id', 'title', 'info', 'type', 'resource'];
    $letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O'];
    $counter = 1;
    foreach($titles as $index => $title) {
      $cell = $letters[$index] . $counter;
      $activeSheet->setCellValue($cell, $title)->getStyle($cell)->getFont()->setBold(true);
    }
    foreach($this->company()->courses as $row) {
      $counter++;
      $item = [];
      foreach(['id','title', 'info', 'type', 'resource'] as $field) {
        $item[] = $row->{$field};
      }
      foreach($item as $index => $value) {
        $cell = $letters[$index] . $counter;
        $activeSheet->setCellValue($cell, $value);
      }
    }
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="courses.xls"');
    header('Cache-Control: max-age=0');
    $excelWriter->save('php://output');
    die();
  }

  public function csv() {
    $data = [];
    foreach($this->company()->courses as $row) {
      $item = [];
      $item['id'] = $row->id;
      foreach(['title', 'info', 'type', 'resource'] as $field) {
        $item[$field] = $row->{$field};
      }
      $data[] = $item;
    }
		header("Content-type: text/csv");
		header("Content-Disposition: attachment; filename=result_file.csv");
		header("Pragma: no-cache");
		header("Expires: 0");
		$output = fopen("php://output", "w");
		foreach ($data as $row) {
			fputcsv($output, $row);
		}
		fclose($output);
		die();
  }

	public function onSaveRules($id=null){
		$rules = [
			'title'   =>[
										'required',
										'string',
										'max:64',
									],
			'exam_id'  => [
										  'required',
										  'integer',
											$this->rule('exists','exams','id')->where(function($query){
												return $query->where('company_id',$this->company()->id);
											})
										],
			'info'     => 'string|min:5',
			'type'     => ['required',
										'string',
										$this->rule('in',['adobe','youtube', 'video','link'])],
			'picture'  => [$this->rule('upload','image')],
			'start_at' => 'date_format:Y-m-d H:i:s',
			'finish_at'=> 'date_format:Y-m-d H:i:s'
		];

		$ctype = trim(strval($this->input('type')));
		$ctype = empty($ctype) ? 'free' : $ctype;
		switch ($ctype) {
			//Lógica de videos de youtube
			case 'youtube':
				$rules['resource'] = ['required','max:255',$this->rule('youtube')];
				break;
			case 'link':
				$rules['resource'] = 'required|url|max:255';
				break;
			case 'video':
				$rules['resource'] = ['required',$this->rule('upload','video',64000,255)];
				break;
			//Logica de adobe acá
			case 'adobe':
				$rules['resource'] = 'required|string|max:255';
		}
		return $rules;
	}
}
