<?php
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request,
    App\Http\Controllers\Controller,
    Carbon\Carbon;

/**
 * Controlador para la administración de categorías de la compañía activa.
 */
class DashboardController extends Controller
{
	/**
	 * Devuelve los usuarios con mejor puntuación en el sistema.
	 * @return 
	 */
	public function ranking(Request $request){
		return $this->company
												 ->users()
												 ->with('user.profile')
												 ->orderBy('score','desc')
												 ->simplePaginate($request->input('limit',10));
	}

	/**
	 * Devuelve los usuarios más activos.
	 * @return 
	 */
	public function hot(Request $request){
		return $this->company
												 ->users()
												 ->with('user.profile')
												 ->orderBy('created_at','desc')
												 ->simplePaginate($request->input('limit',10));
	}

	/**
	 * Devuelve los últimos usuarios registrados.
	 * @return 
	 */
	public function last(Request $request){
		return $this->company
												 ->users()
												 ->with('user.profile')
												 ->orderBy('created_at','desc')
												 ->simplePaginate($request->input('limit',10));

	}

	/**
	 * Devuelve los usuarios en línea.
	 * @return 
	 */
	public function online(Request $request){
		$query = $this->company()->users();
		$query->whereHas('user', function($q){
			$q->whereHas('sessions', function($q){
				$q->where('updated_at','>=',Carbon::now()->subMinute(5)->toDateTimeString());
			});
		});
		return $query->with('user.profile')
								 ->simplePaginate($request->input('limit',10));		
	}

	/**
	 * Datos generales de la empresa.
	 * @param  Request $request
	 * @return
	 */
	public function general(Request $request){
		return [
			'users_count'          => $this->company()->users()->count(),
			'segmentations_count'  => $this->company()->segmentations()->count(),
			'bigger_segmentation'  => ['name' => 'Operadores','count' => 2],
			'more_connected_users' => ['date' => '2017-11-29 19:49:27','count'=>2],
			'most_downloaded'      => ['name' => 'recurso de prueba', 'count' => 5],
			'last_exam'            => ['date'=>'2017-11-29 20:50:11','name' => 'Exámen sorpresa']
		];
	}
}