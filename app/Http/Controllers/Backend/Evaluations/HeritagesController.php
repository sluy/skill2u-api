<?php namespace App\Http\Controllers\Backend\Evaluations;
use App\Http\Controllers\Backend\SingleRels;

class HeritagesController extends SingleRels {
	public $with = [];

	public $searchField = 'parent_id';
}
