<?php namespace App\Http\Controllers\Backend\Evaluations;
use App\Http\Controllers\Backend\SingleRels;
/**
 * Controlador para la administración de los niveles asociados a una evaluación de la compañía.
 */
class LevelsController extends SingleRels {
	public $with = [];
}
