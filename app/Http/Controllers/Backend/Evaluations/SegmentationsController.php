<?php namespace App\Http\Controllers\Backend\Evaluations;
use App\Http\Controllers\Backend\SingleRels;
/**
 * Controlador para la administración de segmentaciones para las evaluaciones de la compañía.
 */
class SegmentationsController extends SingleRels {
	public $with = [];
}
