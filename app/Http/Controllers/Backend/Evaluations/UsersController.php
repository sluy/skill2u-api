<?php namespace App\Http\Controllers\Backend\Evaluations;
use App\Http\Controllers\Backend\SingleRels;
/**
 * Controlador para la administración de usuarios asociados a las evaluaciones de la compañía.
 */
class UsersController extends SingleRels {
	public $crossLookup = false;

	public $model = 'App\Models\User\Evaluation';

	public $searchField = 'user_id';

	public $with = [];

	public function boot(){
		$courseId = $this->route('cid',0,'integer');
		if(!$this->company()->evals()->find($courseId)){
			return $this->notFound();
		}
		$this->conditions['evaluation_id'] = $courseId;
	}
}
