<?php namespace App\Http\Controllers\Backend;
use Carbon\Carbon;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use App\Models\User;

ini_set('max_execution_time', 0);
set_time_limit(0);
/**
 * Controlador para la administración de evaluaciones en la compañía activa.
 */
class EvaluationsController extends Controller
{
	public $with         = [];

	/**
	 * Devuelve en un xls los y sus calificaciones
	 */
	public function xls($id) {
		$eval = $this->company()->evals()->where('id', $id)->firstOrFail();
		$show = [];

		foreach(explode('|', strtolower(strval($this->input('show')))) as $item) {
			$item = trim($item);
			if (($item !== 'approved' && $item !== 'reproved') || in_array($item, $show)) {
				continue;
			}
			$show[] = $item;
		}

		if (empty($show)) {
			$show = ['approved', 'reproved'];
		}

		$spreadsheet = new Spreadsheet();
    $excelWriter = new Xls($spreadsheet);
    $spreadsheet->setActiveSheetIndex(0);
		$activeSheet = $spreadsheet->getActiveSheet();
		$activeSheet->setCellValue('A1', $eval->title)->getStyle('A1')->getFont()->setBold(true);
		$titles = ['name', 'lastname', 'email', 'score', 'status', 'date'];
    $letters = ['A','B','C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O'];

		$counter = 3;
    foreach($titles as $index => $title) {
      $cell = $letters[$index] . $counter;
      $activeSheet->setCellValue($cell, $title)->getStyle($cell)->getFont()->setBold(true);
		}

		foreach($show as $type) {
			foreach($eval->{$type} as $score) {
				$counter ++;
				$item = [];

				//Usuario eliminado
				if (!$score->user) {
					for($n=0;$n<3;$n++) {
						$item[] = '-';
					}
				} else {
					$item[] = $score->user->profile->name;
					$item[] = $score->user->profile->lastname;
					$item[] = $score->user->email;
				}
				$item[] = $score->score;
				$item[] = $type;
				$item[] = strval($score->updated_at);
				foreach($item as $index => $value) {
					$cell = $letters[$index] . $counter;
					$activeSheet->setCellValue($cell, $value);
				}
			}
		}


		header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$eval->title.'.xls"');
    header('Cache-Control: max-age=0');
    $excelWriter->save('php://output');
    die();
	}

	protected function filterByParent(&$query,$values){
		$ids = [];
		foreach($values as $value){
			$parent = $this->company()->evals()->find($value);
			if($parent){
				foreach($parent->childs as $eval){
					if(!in_array($eval->id,$ids)){
						$ids[] = $eval->id;
					}
				}
			}
		}
		$query = $this->model()->whereIn('id',$ids);
	}

	protected function filterBySegmentation(&$query,$values){
		$ids = [];
		$all = $query->get();
		foreach($all as $eval){
			foreach($eval->segmentations as $segmentation){
				if(in_array($segmentation->id,$values)){
					if(!in_array($eval->id,$ids)){
						$ids[] = $eval->id;
					}
					break;
				}
			}
		}
		$query = $this->model()->whereIn('id',$ids);
	}

	protected function filterByLevel(&$query,$levels){
		$ids = [];
		$all = $query->get();
		$values = array_map('intval',$levels);
		foreach($all as $eval){
			foreach($eval->levels as $level){
				if(in_array($level->id, $levels)){
					$ids[] = $eval->id;
					break;
				}
			}
		}
		$query = $this->model()->whereIn('id',$ids);
	}

	protected function filterByDateRange(&$query,$values,$type,$all){
		$ids = [];
		$all = $query->get();
		foreach($all as $eval){
			if($eval->start_at >= $values[0] && $eval->start_at <= $values[1]){
				if(!in_array($eval->id, $ids)){
					$ids[] = $eval->id;
				}
			}
		}
		$query = $this->model()->whereIn('id',$ids);
	}


	protected function orderBySubscribed($items,$type){
		return $this->orderBy('subscribed',$items,$type === 'desc');
	}

	protected function orderByApproved($items,$type){
		return $this->orderBy('approved',$items,$type==='desc');
	}

	protected function orderBy($type,$items,$desc=false){
		$pos = [];
		$sorted = [];
		foreach($items as $k => $item){
			$count = $item->{$type.'_count'};
			if(!isset($pos[$count])){
				$pos[$count] = [];
			}
			$pos[$count][] = $item->toArray();
		}
		$desc ? krsort($pos) : ksort($pos);
		foreach($pos as $items){
			foreach($items as $item){
				$sorted[] = $item;
			}
		}
		return $sorted;
	}


	public function beforeSave($model){
		if(!$model->start_at){
			$model->start_at = Carbon::now()->toDateString();
		}
	}

	public function afterSave($model){
		//Segmentaciones
		if($this->request()->has('segmentations')){
			$segmentations = $this->input('segmentations',[],'array');
			foreach($model->segmentations as $segmentation){
				$segmentation->delete();
			}
			foreach($segmentations as $sid){
				$rel = $this->model('App\Models\Evaluation\Segmentation');
				$rel->evaluation_id = $model->id;
				$rel->segmentation_id = $sid;
				$rel->save();
			}
		}
		//Niveles
		if($this->request()->has('levels')){
			$levels = $this->input('levels',[],'array');
			foreach($model->levels as $levels){
				$levels->delete();
			}
			foreach($levels as $lid){
				$rel = $this->model('App\Models\Evaluation\Level');
				$rel->evaluation_id = $model->id;
				$rel->level_id = $lid;
				$rel->save();
			}
		}

    /*
		if(!$model->is_open){
			return;
		}
		$title = 'Se ha publicado un nuevo curso';
		$message = 'Se ha publicado el nuevo curso <b>[course.title]</b> que puede ser'.
							 ' de su interés. Inscríbase ya para mejorar sus conocimientos y capacitaciones!.';

		$titleview   = $this->company()->views()->where('name','course.new.title')->first();
		$messageview = $this->company()->views()->where('name','course.new.message')->first();

		if($titleview){
			$title = $titleview->value;
		}
		if($messageview){
			$message = $messageview->value;
		}
		foreach($this->company()->users as $user){
			if($this->company->canEvaluate($user,$model)){
				$user->profile;
				$this->api('notify')->info($title,$message,['user'=>$user,'course'=>$model],true);
			}
		}*/
	}

	public function onSaveRules($id=null){
		$rules = [
			'title'   =>[
										'string',
										'max:64',
									],
			'level_id' => [
										 'integer',
										 $this->rule('exists','level','id')->where(function($query){
										 	return $query->where('company_id',$this->company()->id);
										 })
										],
			'free'         => 'required|boolean',
			'resource_id'  => [
										  'required',
										  'integer',
										],

			'info'     => 'string|min:5',
			'picture'  => [$this->rule('upload','image',1024)],
			'start_at' => ['date',
										],
			'finish_at'=> [
											'required',
											'date',
										 $this->rule('CompareDate','min')
										],
			'segmentations' => 'array',
			'segmentations.*' => [
  													 'integer',
  													 $this->rule('exists','segmentations','id')->where(
  													 	function($query){
																return $query->where('company_id',$this->company()->id);
														 }),
  												 ],

			'levels' => 'array',
			'levels.*' => [
  													 'integer',
  													 $this->rule('exists','levels','id')->where(
  													 	function($query){
																return $query->where('company_id',$this->company()->id);
														 }),
  												 ],
		];

		$isFree = $this->input('free',false,'boolean');
		$rules['resource_id'][] = $this->rule('exists',
																					($isFree)?'exams':'courses',
																					'id')
															->where(function($query){
																return $query->where('company_id',$this->company()->id);
															});
		return $rules;
	}
}
