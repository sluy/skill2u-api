<?php namespace App\Http\Controllers\Backend;
use Api\Rules\ExamQuestions;
/**
 * Controlador para la administración de exaḿenes de la compañía activa.
 */
class ExamsController extends Controller
{
	public $with = [];

	public function beforeDelete($model){
		foreach (['evals', 'courses'] as $name) {
			if ($model->{$name}) {
				foreach($model->{$name} as $rel) {
					$rel->delete();
				}
			}
		}
	}

	public function beforeSave($model,$old){
		if($model->questions_quantity){
			$model->auto_weight = true;
		}
		$counter=100;
		if(!$model->auto_weight){
			$counter = 0;
			foreach($model->questions as $question){
				$counter += isset($question['weight']) ? $question['weight'] : 0;
			}
		}
		if($counter != 100){
			$this->error(['questions' => ['La sumatoria de los "pesos" de las preguntas debe ser exactamente igual a 100.']]);
		}
	}

	protected function onSaveRules($id){
		$rules = [
			'title'   =>[
										'required',
										'string',
										'max:64',
										$this->rule('unique','exams')->where(function($query) use($id){
											return $query->where('company_id',$this->company()->id)
																	 ->where('id','!=',$id);
										})
									],
			'info'                           => 'string|min:5',
			'auto_weight'                    => 'boolean',
      'approved_percent'               => 'required|integer|max:100',
      'random'                         => 'boolean',
			'picture'                        => [$this->rule('upload','image',1024)],
			//matriz de preguntas
			'questions'                      => 'required|array',
			//pregunta dentro de la matriz
			'questions.*'                    => 'array',
			//Texto de la pregunta
			'questions.*.text'               => 'required|string',
			//Matriz de las respuestas correctas (mínimo uno)
			'questions.*.correct_answers'    => 'required|array',
			//La matriz de respuestas correctas sólo tendrá el id de la respuesta
			'questions.*.correct_answers.*'  => 'integer',
			//Cantidad de respuestas a mostrar
			'questions.*.answers_quantity'   => 'integer',
			//Matriz de respuestas
			'questions.*.answers'            => 'required|array',
			//Cada respuesta será sólo el texto a mostrar
			'questions.*.answers.*'          => 'required|string'
		];
		$qQuantity   = $this->input('questions_quantity',0,'integer');
		$autoWeight  = $this->input('auto_weight',false,'boolean');
		//La cantidad de preguntas fue definida:
		//Inhabilitamos el "peso" automático y hacemos requerido el mismo
		//en cada pregunta. A su vez forzamos a que la cantidad de preguntas
		//sea cómo mínimo la establecida.
		if($qQuantity){
			$autoWeight = false;
			$rules['questions'] .= '|min:'.$qQuantity;
			$rules['questions.*.weight'] = 'required|integer|max:100';
		}else{
			if(!$autoWeight){
				$rules['questions.*.weight'] = 'required|integer|max:100';
			}
		}
		foreach($this->input('questions',[],'array') as $k => $question){
			$correct   = $this->input('questions.'.$k.'.correct_answers',[],'array');
			$aQuantity = $this->input('questions.'.$k.'.answers_quantity',0,'integer');

			if($aQuantity){
				if($correct){
					$aQuantity = count($correct) + 1;
					$rules['questions.'.$k.'.answers_quantity'] = 'required|integer|min:'.$aQuantity;
				}
				$rules['questions.'.$k.'.answers'] = 'required|array|min:'.$aQuantity;
			}
			if($correct){
				foreach($correct as $i => $v){
					$rules['questions.'.$k.'.answers.'.$i] = 'required';
				}
			}
		}
		return $rules;
	}
}
