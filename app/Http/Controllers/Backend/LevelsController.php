<?php namespace App\Http\Controllers\Backend;
/**
 * Controlador para la administración de los "niveles" en el sistema.
 */
class LevelsController extends Controller {

	protected function sortLevels(){
		$levels = [];
		$arr    = [];
		foreach($this->company()->levels()->orderBy('order')->get() as $level){
			$levels[$level->id] = $level;
			$arr[$level->id] = $level->order;
		}
		asort($arr);
		$order = 0;
		foreach($arr as $lid => $o){
			$levels[$lid]->order = $order;
			$levels[$lid]->save();
			$order++;
		}
	}

	public function beforeSave($model){
		//Si no se especificó el orden, lo establecemos como el último
		if(strval(intval($model->order)) != strval($model->order) || 
			 $model->order < 0){
			$last = $this->company()->levels()->orderBy('order','desc')->first();
			$model->order = ($last) ? ($last->order + 1) : 0;
		}

		if(empty($model->down)){
			$model->down = 50;
		}

		if(empty($model->up)){
			$model->up = 80;
		}

		if($model->up <= $model->down){
			$model->up = ($model->down +1);
		}

		$exists = $this->company()
									 ->levels()
									 ->where('order',$model->order)
									 ->first();
		//Si ya hay un nivel con el orden establecido y este no es el modelo
		//actual, hacemos que todos los niveles después de él tengan el orden 
		//siguiente.
		if($exists && $exists->id != $model->id){
			$change = $this->company()->levels()
																->where('order','>=',$model->order)
																->get();
			foreach($change as $level){
				$level->order = ($level->order + 1);
				$level->save();
			}
		}
	}

	public function afterSave($model){
		$this->sortLevels();
	}

	public function beforeDelete($model){
		//Prevenimos que sea eliminado si el nivel tiene asociaciones activas.
		if(count($model->users) || count($model->evaluations) || 
			 count($model->fromPerformances) || count($model->toPerformances)){
			return $this->forbidden();
		}
		//Prevenimos que sea eliminado si es el único nivel existente en la empresa.
		if(count($this->model()->all()) == 1){
			return $this->forbidden();
		}
	}

	public function afterDelete($model){
		$this->sortLevels();
	}

	/**
	 * Reglas de validación al guardar el registro.
	 * @param  integer $id Id del registro.
	 * @return array
	 */
	public function onSaveRules($id=null){
		return [
			'title'   => [
				'required',
				'string',
				'max:64',
				$this->rule('unique','levels')->where(function($query) use($id){
					return $query->where('company_id',$this->company()->id)
											 ->where('id','!=',$id);
				})
			],
			'picture' => [$this->rule('upload','image')],
			'order'   => 'integer',
			'options' => 'array',
			'up'      => 'numeric|max:99',
			'down'    => 'numeric|max:100'
		];
	}
}