<?php namespace App\Http\Controllers\Backend\Libs;
use App\Http\Controllers\Backend\SingleRels;
/**
 * Controlador para la administración de categorías de los recursos en la librería.
 */
class LevelsController extends SingleRels {
	public $with = [];
}
