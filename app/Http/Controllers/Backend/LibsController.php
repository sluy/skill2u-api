<?php namespace App\Http\Controllers\Backend;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
/**
 * Controlador para la administración de los recursos de la biblioteca en la compañía activa.
 */
class LibsController extends Controller
{
	public $with         = [];

  public function xls() {
    $spreadsheet = new Spreadsheet();
    $excelWriter = new Xls($spreadsheet);
    $spreadsheet->setActiveSheetIndex(0);
    $activeSheet = $spreadsheet->getActiveSheet();
    $titles = ['id', 'title', 'downloads count', 'categories'];
    $letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O'];
    $counter = 1;
    foreach($titles as $index => $title) {
      $cell = $letters[$index] . $counter;
      $activeSheet->setCellValue($cell, $title)->getStyle($cell)->getFont()->setBold(true);
    }
    foreach($this->company()->libs as $row) {
      $counter++;
      $item = [];
      foreach(['id', 'title', 'downloads_count'] as $field) {
        $item[] = $row->{$field};
      }
      $categories = [];
      foreach($row->categories as $cat) {
        $categories[] = $cat->category->title;
      }
      $item[] = implode(' ', $categories);
      foreach($item as $index => $value) {
        $cell = $letters[$index] . $counter;
        $activeSheet->setCellValue($cell, $value);
      }
    }
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="libs.xls"');
    header('Cache-Control: max-age=0');
    $excelWriter->save('php://output');
    die();
  }

  public function csv() {
    $data = [];
    foreach($this->company()->libs as $row) {
      $item = [];
      $item['id'] = $row->id;
      foreach(['title', 'downloads_count'] as $field) {
        $item[$field] = $row->{$field};
      }
      $categories = [];
      foreach($row->categories as $cat) {
        $categories[] = $cat->category->title;
      }
      $item['categories'] = implode(' ', $categories);
      $data[] = $item;
    }
		header("Content-type: text/csv");
		header("Content-Disposition: attachment; filename=result_file.csv");
		header("Pragma: no-cache");
		header("Expires: 0");
		$output = fopen("php://output", "w");
		foreach ($data as $row) {
			fputcsv($output, $row);
		}
		fclose($output);
		die();
  }


	protected function filterByCategory(&$query,$values){
		$ids = [];
    $all = $query->get();
		$values = array_map('intval',$values);
		foreach($all as $lib){
			foreach($lib->categories as $row){
				if(in_array($row->category_id,$values)){
					if(!in_array($lib->id,$ids)){
						$ids[] = $lib->id;
					}
					break;
				}
			}
    }
		$query = $this->model()->whereIn('id',$ids);
	}

	protected function filterByTag(&$query,$values){
		$ids = [];
		$all = $query->get();
		$values = array_map('intval',$values);
		foreach($all as $lib){
			foreach($lib->tags as $row){
				if(in_array($row->tag_id,$values)){
					if(!in_array($lib->id,$ids)){
						$ids[] = $lib->id;
					}
					break;
				}
			}
		}
		$query = $this->model()->whereIn('id',$ids);
	}


	protected function filterBySegmentation(&$query,$values){
		$ids = [];
		$all = $query->get();
		$values = array_map('intval',$values);
		foreach($all as $lib){
			foreach($lib->segmentations as $segmentation){
				if(in_array($segmentation->id,$values)){
					if(!in_array($lib->id,$ids)){
						$ids[] = $lib->id;
					}
					break;
				}
			}
		}
		$query = $this->model()->whereIn('id',$ids);
	}


	protected function filterByLevel(&$query,$values){
		$ids = [];
		$all = $query->get();
		$values = array_map('intval',$values);
		foreach($all as $lib){
			foreach($lib->levels as $level){
				if(in_array($level->id,$values)){
					if(!in_array($lib->id,$ids)){
						$ids[] = $lib->id;
					}
					break;
				}
			}
		}
		$query = $this->model()->whereIn('id',$ids);
	}

	protected function filterByDateRange(&$query,$values,$type,$all){
		$ids = [];
		$all = $query->get();
		foreach($all as $post){
			if($post->created_at >= $values[0] && $post->created_at <= $values[1]){
				if(!in_array($post->id, $ids)){
					$ids[] = $post->id;
				}
			}
		}
		$query = $this->model()->whereIn('id',$ids);
	}

	public function afterSave($model){
		foreach(['segmentations','levels','categories','tags'] as $item){
			if($this->request()->has($item) && is_array($this->input($item))){
				$values = $this->input($item);
				foreach($model->{$item} as $prev){
					$prev->delete();
				}
				foreach($values as $id){
					$instance = $this->company()->{$item}()->where('id',$id)->first();
					if($instance){
						$name = @end(explode('\\',get_class($instance)));
						$rel = $this->model('App\Models\Lib\\'.$name);
						$rel->lib_id = $model->id;
						$rel->{lcfirst($name).'_id'} = $instance->id;
						$rel->save();
					}
				}
			}
		}
	}


	protected function orderByDownloads($items,$type){
		$pos = [];
		$sorted = [];
		foreach($items as $k => $item){
			$count = $item->{'downloads_count'};
			if(!isset($pos[$count])){
				$pos[$count] = [];
			}
			$pos[$count][] = $item->toArray();
		}
		$type == 'desc' ? krsort($pos) : ksort($pos);
		foreach($pos as $items){
			foreach($items as $item){
				$sorted[] = $item;
			}
		}
		return $sorted;
	}

	protected function orderByViews($items,$type){
		$pos = [];
		$sorted = [];
		foreach($items as $k => $item){
			$count = $item->{'views_count'};
			if(!isset($pos[$count])){
				$pos[$count] = [];
			}
			$pos[$count][] = $item->toArray();
		}
		$type == 'desc' ? krsort($pos) : ksort($pos);
		foreach($pos as $items){
			foreach($items as $item){
				$sorted[] = $item;
			}
		}
		return $sorted;
	}


	protected function onSaveRules($id){
		$rules = [
			'title'    =>[
				'string',
				'max:64'
			],
			'content'  => 'string|min:5',
			'file'    => [],
			'picture' => [$this->rule('upload','image',1024)],
		];
		if(!$id){
			foreach(['title','file'] as $type){
				array_unshift($rules[$type], 'required');
			}
		}
		return $rules;
	}
}
