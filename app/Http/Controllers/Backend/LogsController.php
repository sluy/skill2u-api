<?php namespace App\Http\Controllers\Backend;
/**
 * Controlador para la administración de los recursos de la biblioteca en la compañía activa.
 */
class LogsController extends Controller
{
	public $with         = [];

	public $model = 'App\Models\User\Log';

	public $crossLookup = false;

	public $forceKeySearch = false;


	public function find($id=null,$cfg=null){
		$cfg = is_array($cfg) ? $cfg : [];
		$cfg['conditions'] = isset($cfg['conditions']) && is_array($cfg['conditions'])
												 ? $cfg['conditions']
												 : [];
		$ids = [];
		foreach($this->company()->users as $user){
			$ids[] = $user->id;
		}
		$cfg['conditions']['user_id'] = $ids;
		return parent::find($id,$cfg);
	}
}
