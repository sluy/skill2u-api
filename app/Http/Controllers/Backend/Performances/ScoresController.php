<?php namespace App\Http\Controllers\Backend\Performances;
use App\Http\Controllers\Backend\Rels;
/**
 * Controlador para la administración performances de los usuarios en la compañía.
 */
class ScoresController extends Rels{
	public $searchField = 'user_id';
	public $crossLookup = false;
	public $model = 'App\Models\User\Performance';
	public $performanceModel = null;

	public function boot(){
		$this->performanceModel = $this->company()->performances()
																							->find($this->route('pid',0,'integer'));
		if(!$this->performanceModel){
			return $this->notFound();
		}
		$this->conditions['performance_id'] = $this->performanceModel->id;
	}

	public function beforeSave($model){
		$model->value = number_format($model->value, 2, '.', '');
	}

	public function store($id=null){
		$this->check('store');
		$user = $this->company()->users()->find($id);
		if(!$user){
			return $this->notFound();
		}
		$value = $this->request()->has('value')
						 ? $this->input('value')
						 : false;
		if(is_string($value) && strlen(trim($value)) == 0){
			$value = null;
		}
		$performance = $this->company->calculateUserPerformance($this->performanceModel,$user,$value);
		
		if(!$performance){
			return $this->forbidden();
		}
		return $this->ok($performance);
	}

	public function import(){
		$this->check('import');
		$file = $this->request()->file('file');
		$fh = fopen($file->getPathname(),'r+');
		$models = [];
		while(($row = fgetcsv($fh, 8192)) !== FALSE){
			if(empty($row)){
				continue;
			}
			$user = filter_var($row[0], FILTER_VALIDATE_EMAIL)
							? $this->company()->users()->where('email',$row[0])->first()
							: $this->company()->users()->find($row[0]);
			if(!$user){
				continue;
			}
			$score = !isset($row[1])
							 ? false
							 : $row[1];
			$models[] = $this->company->calculateUserPerformance($this->performanceModel,$user,$score);
		}
		$this->ok($models);
	}

	public function onStoreRules($id=null){
		return [
			'value' => 'numeric|min:0|max:100',
		];
	}

	public function onImportRules($id=null){
		return array(
			'file' => ['required',$this->rule('upload','text',1024)],
		);
	}
}