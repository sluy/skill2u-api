<?php namespace App\Http\Controllers\Backend;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xls;

/**
 * Controlador para la administración del performance en el sistema.
 */
class PerformancesController extends Controller {
	public $with = [];

  public function scores($id) {
    $performance = $this->company()->performances()->where('id', $id)->first();
    $scores = [];

    foreach($this->company()->users as $user) {
      $user->profile;
      $tmp = $user->toArray();
      $score = ($performance)
        ? $performance->scores()->where('user_id', $user->id)->first()
        : null;
      if ($score) {
        $tmp['score_id'] = $score->id;
        $tmp['from_level'] = $score->fromLevel->toArray();
        $tmp['to_level'] =$score->toLevel->toArray();
				$tmp['system_eval'] = $score->system_eval;
        $tmp['user_eval'] = $score->user_eval;
        $tmp['total'] = $score->total;
      } else {
        $tmp['score_id'] = null;
        $tmp['from_level'] = null;
        $tmp['to_level'] = null;
        $tmp['system_eval'] = null;
        $tmp['user_eval'] = null;
        $tmp['total'] = null;
      }
      $scores[] = $tmp;
    }
    app('Api\Api')->get('response')->render($scores, 200);
    die();
  }

  public function xls($id) {
    $spreadsheet = new Spreadsheet();
    $excelWriter = new Xls($spreadsheet);
    $spreadsheet->setActiveSheetIndex(0);
    $activeSheet = $spreadsheet->getActiveSheet();
    $titles = ['user id', 'user email', 'user name', 'user lastname', 'user rank', 'system score', 'sell score'];
    $letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O'];
    $counter = 1;
    foreach($titles as $index => $title) {
      $cell = $letters[$index] . $counter;
      $activeSheet->setCellValue($cell, $title)->getStyle($cell)->getFont()->setBold(true);
    }
    $performance = $this->company()->performances()->where('id', $id)->first();

    foreach($this->company()->users as $user) {
      $counter++;
      $item = [];
      $item[] = $user->id;
      $item[] = $user->email;
      $item[] = $user->profile->name;
      $item[] = $user->profile->lastname;
			$item[] = $user->rank;
      $score = ($performance)
        ? $performance->scores()->where('user_id', $user->id)->first()
        : null;

      if ($score) {
				$item[] = $score->system_eval === null ? '-' : $score->system_eval;
				$item[] = $score->user_eval;
      } else {
        $item[] = '-';
        $item[] = '-';
        $item[] = '-';
      }
      foreach($item as $index => $value) {
        $cell = $letters[$index] . $counter;
        $activeSheet->setCellValue($cell, $value);
      }
    }
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="users.xls"');
    header('Cache-Control: max-age=0');
    $excelWriter->save('php://output');
    die();
  }

	/**
	 * Exporta el performance suministrado en formato CSV.
	 */
	public function csv($id) {
		$data = [];
		$performance = $this->company()->performances()->where('id', $id)->first();
		if ($performance) {
			foreach ($performance->scores as $score) {
				$item = [];
				$item[] = $score->user->profile->name . ' ' . $score->user->profile->lastname;
				$item[] = $score->user->rank;
				$item[] = $score->system_eval === null ? '0.00' : $score->system_eval;
				$item[] = $score->user_eval;
				$data[] = $item;
			}
		}
		header("Content-type: text/csv");
		header("Content-Disposition: attachment; filename=result_file.csv");
		header("Pragma: no-cache");
		header("Expires: 0");
		$output = fopen("php://output", "w");
		foreach ($data as $row) {
			fputcsv($output, $row);
		}
		fclose($output);
		die();
	}


	public function beforeDelete($model){
		//No permitimos que se elimine el modelo si este tiene relaciones.
		return count($model->scores)
					 ? $this->forbidden()
					 : null;
	}

	public function beforeSave($model,$ori){
		if($model->user_eval_weight != $ori->user_eval_weight){
			$model->user_eval_weight = number_format($model->user_eval_weight,2,'.','');
		}
	}

	public function afterSave($model,$ori){
		if($model->finished){
			$this->company->calculateUsersPerformance($model);
		}
	}

	public function onSaveRules($id=null){
		return [
			'title'            => 'sometimes|required|string|max:64',
			'picture'          => [$this->rule('upload','image',1024)],
			'finished'         => 'boolean',
			'user_eval_weight' => 'numeric|max:100',
		];
	}
}
