<?php namespace App\Http\Controllers\Backend;
use Carbon\Carbon,
		App\Models\Company,
		App\Models\Performance;

/**
 * Controlador para la administración de categorías de la compañía activa.
 */
class PeriodsController extends Controller
{

	public function afterSave(){
		app('Api\Api')->company->checkAllPeriods();
	}

	protected function onSaveRules($id=null){
		return [
			'title'    => 'required|string|max:64',
			'start_at' => 'required|date',
			'months'   => 'required|integer',
			'picture'  => [$this->rule('upload','image',1024)]
		];
	}

	/**
	 * Comprueba los períodos.
	 * Mismo trabajo que el cronjob; sólo creado para testing.
	 *
	 * @return void
	 */
	public function checkAllPeriods() {
		app('Api\Api')->company->checkAllPeriods();
	}
}