<?php namespace App\Http\Controllers\Backend;

use Carbon\Carbon;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
/**
 * Controlador para las encuestas.
 */
class PoolsController extends Controller {


  public function xls() {
    dd('hello');
  }

  public function csv() {
    dd('hello csv');
  }

  public function afterSave($model){
		//Segmentaciones
		if($this->request()->has('segmentations')){
			$segmentations = $this->input('segmentations',[],'array');
			foreach($model->segmentations as $segmentation){
				$segmentation->delete();
			}
			foreach($segmentations as $sid){
				$rel = $this->model('App\Models\Pool\Segmentation');
				$rel->pool_id = $model->id;
				$rel->segmentation_id = $sid;
				$rel->save();
			}
		}
  }
  /**
	 * Reglas de validación al guardar el registro.
	 * @param  integer $id Id del registro.
	 * @return array
	 */
	public function onSaveRules($id=null){
		return [
			'title'   => [
				'string',
				'max:64',
				$this->rule('unique','levels')->where(function($query) use($id){
					return $query->where('company_id',$this->company()->id)
											 ->where('id','!=',$id);
				})
			],
      'question' => 'required|string|max:255',
      'answers'  => 'required|array',
      'multi'    => 'boolean',
      'results'  => 'in:votes,percent,both',
      'start_at' => 'date',
      'finish_at'=> ['required', 'date', $this->rule('CompareDate','min')],
      'segmentations' => 'array',
			'segmentations.*' => [
				 'integer',
				 $this->rule('exists','segmentations','id')->where(
				 	function($query){
						return $query->where('company_id',$this->company()->id);
				 }),
			 ],
		];
	}
}
