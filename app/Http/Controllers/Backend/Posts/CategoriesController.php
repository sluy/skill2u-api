<?php namespace App\Http\Controllers\Backend\Posts;
use App\Http\Controllers\Backend\SingleRels;
/**
 * Controlador para la administración de categorías de las publicaciones.
 */
class CategoriesController extends SingleRels {
	public $with = [];
}
