<?php namespace App\Http\Controllers\Backend\Posts;
use App\Http\Controllers\Backend\SingleRels;
/**
 * Controlador para la administración de categorías de los recursos en la librería.
 */
class LevelsController extends SingleRels {
	public $with = [];
}
