<?php namespace App\Http\Controllers\Backend;
use Carbon\Carbon;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
/**
 * Controlador para la administración de publicaciones en la compañía activa.
 */
class PostsController extends Controller
{
	public $with = [];

	protected $notify = false;

	public function boot(){
		$this->statics['user_id'] = $this->auth->user()->id;
		$this->defaults['date']   = Carbon::now()->toDateTimeString();
	}

  public function xls() {
    $spreadsheet = new Spreadsheet();
    $excelWriter = new Xls($spreadsheet);
    $spreadsheet->setActiveSheetIndex(0);
    $activeSheet = $spreadsheet->getActiveSheet();
    $titles = ['id', 'is news', 'title', 'type', 'likes', 'comments count', 'writer id' ,'writer email', 'writer name', 'writer lastname'];
    $letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O'];
    $counter = 1;
    foreach($titles as $index => $title) {
      $cell = $letters[$index] . $counter;
      $activeSheet->setCellValue($cell, $title)->getStyle($cell)->getFont()->setBold(true);
    }
    foreach($this->company()->posts as $row) {
      $counter++;
      $item = [];
      $item[] = $row->id;
      $item[] = ($row->blog || $row->news) ? 'yes' : 'no' ;
      foreach(['title', 'type', 'likes_count', 'comments_count'] as $field) {
        $item[] = $row->{$field};
      }
      $item[] = $row->user->id;
      $item[] = $row->user->email;
      $item[] = $row->user->profile->name;
      $item[] = $row->user->profile->lastname;
      foreach($item as $index => $value) {
        $cell = $letters[$index] . $counter;
        $activeSheet->setCellValue($cell, $value);
      }
    }
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="posts.xls"');
    header('Cache-Control: max-age=0');
    $excelWriter->save('php://output');
    die();
  }

  public function csv() {
    $data = [];
    foreach($this->company()->posts as $row) {
      $item = [];
      $item['id'] = $row->id;
      $item['is_news'] = ($row->blog || $row->news) ? 'yes' : 'no' ;
      foreach(['title', 'type', 'likes_count', 'comments_count'] as $field) {
        $item[$field] = $row->{$field};
      }
      $item['user_id'] = $row->user->id;
      $item['email'] = $row->user->email;
      $item['user_name'] = $row->user->profile->name;
      $item['user_lastname'] = $row->user->profile->lastname;
      $data[] = $item;
    }
		header("Content-type: text/csv");
		header("Content-Disposition: attachment; filename=result_file.csv");
		header("Pragma: no-cache");
		header("Expires: 0");
		$output = fopen("php://output", "w");
		foreach ($data as $row) {
			fputcsv($output, $row);
		}
		fclose($output);
		die();
  }

    protected function filterByCategory(&$query,$values){
		$ids = [];
		$all = $query->get();
			$values = array_map('intval',$values);
			foreach($all as $post){
				foreach($post->categories as $row){
					if(in_array($row->category_id,$values)){
						if(!in_array($post->id,$ids)){
							$ids[] = $post->id;
						}
						break;
					}
				}
		}
		$query = $this->model()->whereIn('id',$ids);
	}

	protected function filterByTag(&$query,$values){
		$ids = [];
		$all = $query->get();
		$values = array_map('intval',$values);
		foreach($all as $post){
			foreach($post->tags as $row){
				if(in_array($row->tag_id,$values)){
					if(!in_array($post->id,$ids)){
						$ids[] = $post->id;
					}
					break;
				}
			}
		}
		$query = $this->model()->whereIn('id',$ids);
	}

	protected function filterBySegmentation(&$query,$values){
		$ids = [];
		$all = $query->get();
		$values = array_map('intval',$values);
		foreach($all as $post){
			foreach($post->segmentations as $segmentation){
				if(in_array($segmentation->id,$values)){
					if(!in_array($post->id,$ids)){
						$ids[] = $post->id;
					}
					break;
				}
			}
		}
		$query = $this->model()->whereIn('id',$ids);
	}

	protected function filterByLevel(&$query,$values){
		$ids = [];
		$all = $query->get();
		$values = array_map('intval',$values);
		foreach($all as $post){
			foreach($post->levels as $level){
				if(in_array($level->id,$values)){
					if(!in_array($post->id,$ids)){
						$ids[] = $post->id;
					}
					break;
				}
			}
		}
		$query = $this->model()->whereIn('id',$ids);
	}


	protected function filterBySex(&$query,$values,$literal,$all){
		$ids = [];
		foreach($all as $post){
			if(in_array($post->user->profile->sex,$values)){
				if(!in_array($post->id,$ids)){
					$ids[] = $post->id;
				}
			}
		}
		$query = $this->model()->whereIn('id',$ids);
	}

	protected function filterByDateRange(&$query,$values,$type,$all){
		$ids = [];
		$all = $query->get();
		foreach($all as $post){
			if($post->created_at >= $values[0] && $post->created_at <= $values[1]){
				if(!in_array($post->id, $ids)){
					$ids[] = $post->id;
				}
			}
		}
		$query = $this->model()->whereIn('id',$ids);
	}



	protected function orderByActivity($items,$type){
		$pos = [];
		$sorted = [];
		foreach($items as $k => $item){
			$count = $item->{'activity'};
			if(!isset($pos[$count])){
				$pos[$count] = [];
			}
			$pos[$count][] = $item->toArray();
		}
		$type == 'desc' ? krsort($pos) : ksort($pos);
		foreach($pos as $items){
			foreach($items as $item){
				$sorted[] = $item;
			}
		}
		return $sorted;
	}


	protected function orderByLikes($items,$type){
		$pos = [];
		$sorted = [];
		foreach($items as $k => $item){
			$count = $item->{'likes_count'};
			if(!isset($pos[$count])){
				$pos[$count] = [];
			}
			$pos[$count][] = $item->toArray();
		}
		$type == 'desc' ? krsort($pos) : ksort($pos);
		foreach($pos as $items){
			foreach($items as $item){
				$sorted[] = $item;
			}
		}
		return $sorted;
	}


	protected function orderByComments($items,$type){
		$pos = [];
		$sorted = [];
		foreach($items as $k => $item){
			$count = $item->{'comments_count'};
			if(!isset($pos[$count])){
				$pos[$count] = [];
			}
			$pos[$count][] = $item->toArray();
		}
		$type == 'desc' ? krsort($pos) : ksort($pos);
		foreach($pos as $items){
			foreach($items as $item){
				$sorted[] = $item;
			}
		}
		return $sorted;
	}


	protected function orderByViews($items,$type){
		$pos = [];
		$sorted = [];
		foreach($items as $k => $item){
			$count = $item->{'views_count'};
			if(!isset($pos[$count])){
				$pos[$count] = [];
			}
			$pos[$count][] = $item->toArray();
		}
		$type == 'desc' ? krsort($pos) : ksort($pos);
		foreach($pos as $items){
			foreach($items as $item){
				$sorted[] = $item;
			}
		}
		return $sorted;
	}

	public function beforeSave($model,$ori){
		if($model->news && !$ori->news){
			$this->notify = true;
		}
		if(!$model->news){
			$model->title = '';
		}
		if(empty($model->type)){
			$model->resource = '';
		}
	}

	public function afterCreate($model){
		$this->auth->log('post.create',$model->id);
	}

	public function afterUpdate($model,$ori){
		$this->auth->log('post.update',$model->id,$ori->toArray());
	}

	public function afterDelete($model){
		$this->auth->log('post.delete',$model->id,$model->toArray());
	}

	public function afterSave($model){
		foreach(['segmentations','levels'] as $item){
			if($this->request()->has($item) && is_array($this->input($item))){
				$values = $this->input($item);
				foreach($model->{$item} as $prev){
					$prev->delete();
				}
				foreach($values as $id){
					$instance = $this->company()->{$item}()->where('id',$id)->first();
					if($instance){
						$name = @end(explode('\\',get_class($instance)));
						$rel = $this->model('App\Models\Post\\'.$name);
						$rel->post_id = $model->id;
						$rel->{lcfirst($name).'_id'} = $instance->id;
						$rel->save();
					}
				}
			}
		}
		if(!$this->notify || !$this->company()->getOptions('notify_news')){
			return;
		}

		$title = 'Se ha publicado una nueva noticia';
		$message = 'Se ha publicado la nueva noticia <b>[post.title]</b> que puede ser'.
							 ' de su interés.';

		$titleview   = $this->company()->views()->where('name','news.new.title')->first();
		$messageview = $this->company()->views()->where('name','news.new.message')->first();

		if($titleview){
			$title = $titleview->value;
		}

		if($messageview){
			$message = $messageview->value;
		}

		$users = $this->company()->users;

		foreach($users as $user){
			$user->profile;
			$this->api('notify')->info($title,$message,['user'=>$user,'post'=>$model],true);
		}
	}

	protected function onSaveRules($id=null){
		$rules = [
		'news'            => 'boolean',
		'blog'             => 'boolean',
  		'type'            => ['string',$this->rule('in',['picture','youtube','video'])],
		'content'         => 'string|min:5',
  		'date'            => 'date_format:Y-m-d H:i:s',
  		'resource'        => '',
  		'segmentations'   => 'array',
  		'segmentations.*' => [
								'integer',
								$this->rule('exists','segmentations','id')->where(
								function($query) use($id){
										return $query->where('company_id',$this->company()->id);
									}),
							]
		];
		if($this->cast('boolean',$this->input('news',false))){
			$rules['title'] = 'required|string|max:64';
		}
		$type = $this->input('type');
		switch ($type) {
			case 'picture':
				$rules['resource'] = [$this->rule('upload','image',1,255)];
				break;
			case 'video':
				$rules['resource'] = [$this->rule('upload','video',64,255)];
				break;
			case 'youtube':
				$rules['resource'] = ['string','url','max:255',$this->rule('youtube')];
				break;
		}
		if($id){
			$item = $this->model()->newQuery()->find($id);
			if($item && $type && $type != $item->type){
				array_unshift($rules['resource'], 'required');
			}
		}else{
			if($type){
				array_unshift($rules['resource'], 'required');
			}
		}
		return $rules;
	}
}
