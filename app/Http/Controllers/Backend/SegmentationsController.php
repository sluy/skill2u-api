<?php namespace App\Http\Controllers\Backend;
/**
 * Controlador para la administración de compañías para los usuarios del sistema.
 */
class SegmentationsController extends Controller {
	/**
	 * Relaciones a levantar antes de devolver el modelo.
	 * @var array
	 */
	public $with         = [];

	protected function resolveWith($parent=null,$prepend=''){
		$childs = !$parent
							? $this->company()->segmentations()->where('parent_id',0)->get()
							: $parent->childs;
		$name = trim($prepend.'.childs','.');
		$max  = $name;
		foreach($childs as $c){
			if($c->childs){
				$tmp = $this->resolveWith($c,$name);
				if(strlen($tmp) > $max){
					$max = $tmp;
				}
			}
		}
		return $max;
	}

	public function boot(){
		$this->with[] = $this->resolveWith();
	}

	public function beforeSave($model){
		$parentId = request()->input('parent_id');
		if($parentId == '0'){
			$model->parent_id = 0;
		}
	}


	public function beforeDelete($model){
		foreach(['childs', 'users', 'libs'] as $name) {
			if ($model->{$name}) {
				foreach($model->{$name} as $rel) {
					$rel->delete();
				}
			}
		}
	}

	/**
	 * Reglas de validación al guardar el registro.
	 * @param  integer $id Id del registro.
	 * @return array
	 */
	public function onSaveRules($id=null){
		$rules = [
			'status' => 'boolean',
			'name'   => [
										'required',
										'string',
										'max:64',
										$this->rule('unique','segmentations')->where(function($query) use($id){
											return $query->where('company_id',$this->company()->id)
																	 ->where('id','!=',$id);
										})
									],
			'picture' => [$this->rule('upload','image',1024)],
			'parent_id' => ['integer'],
			'options' => 'array'
		];
		$parent = intval($this->input('parent_id'));
		if($id) {
			$rules['parent_id'][] = $this->rule('notIn',[$id]);
		}
		if($parent != 0) {
			$rules['parent_id'][] =
				$this->rule('exists','segmentations','id')
						->where(function($query) use ($id){
					return $query->where('company_id',$this->company()->id)
											->where('id','!=',$id);
				});
		}
		return $rules;
	}
}
