<?php namespace App\Http\Controllers\Backend;
use Api\Format;
/**
 * Controlador base para las relaciones internas de la compañía.
 * A direferncia del controlador Rels, edita directamente un campo. 
 * Util para tablas intermedias sin información adicional.
 */
class SingleRels extends Rels {

}