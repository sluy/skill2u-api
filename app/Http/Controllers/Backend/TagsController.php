<?php namespace App\Http\Controllers\Backend;
/**
 * Controlador para la administración de categorías de la compañía activa.
 */
class TagsController extends Controller
{
	public $with         = [];

	public function beforeDelete($model){
		//Eliminamos las relaciones.
		foreach(['libs', 'posts'] as $name) {
			foreach($model->{$name} as $rel) {
				$rel->delete();
			}
		}
	}


	protected function onSaveRules($id){
		return [
			'title'   =>[
										'required',
										'string',
										'max:64',
										$this->rule('unique','tags')->where(function($query) use($id){
											return $query->where('company_id',$this->company()->id)
																	 ->where('id','!=',$id);
										})
									],
			'info'    => 'string|min:5'
		];
	}
}
