<?php namespace App\Http\Controllers\Backend;
use Carbon\Carbon;

class TopsController extends Controller {
	protected $limit = 5;

	public function __construct(){
		$this->limit = $this->input('limit',5,'integer');
	}

	public function onlineUsers(){
		$users = [];
		$now = Carbon::now()->subMinutes(5)->toDateTimeString();
		foreach($this->company()->users as $user){
			if($user->sessions()->where('updated_at','>',$now)->first()){
				$user->profile;
				$users[] = $user;
			}
		}
		return $users;
	}

	public function likedPosts(){
		$this->ok($this->sortFeedback('like'));
	}

	public function  commentedPosts(){
		$this->ok($this->sortFeedback('comment'));
	}

	public function commenterUsers(){
		$models   = [];
		$top  = [];
		foreach($this->company()->users as $user){
			$count = count($user->comments);
			if($count > 0){
				$user->profile;
				$models[$user->id] = $user;
				$top[$user->id] = $count;
			}
		}
		$this->ok($this->limit($models,$top));
	}

	public function performance(){
		$models = [];
		$top = [];
		foreach($this->company()->users as $user){
			$user->profile;
			$models[$user->id] = $user;
			$top[$user->id] = $user->performance;
		}
		$this->ok($this->limit($models,$top));
	}

	public function systemPerformance(){
		$models = [];
		$top = [];
		foreach($this->company()->users as $user){
			$user->profile;
			$models[$user->id] = $user;
			$top[$user->id] = $user->system_performance;
		}
		$this->ok($this->limit($models,$top));
	}


	protected function sortFeedback($type){
		$models = [];
		$top    = [];
		foreach($this->company()->posts as $post){
			$counter = 0;
			$likes = $this->model('App\\Models\\User\\Feedback')
										->where('name','post')
										->where('type',$type)
										->where('primary_key',$post->id)
										->get();
			$counter += count($likes);
			if($counter > 0){
				$models[$post->id] = $post;
				$top[$post->id] = $counter;
			}
		}
		return $this->limit($models,$top);
	}

	protected function limit($models,$top){
		arsort($top);
		$c=0;
		$return = [];
		foreach($top as $id => $count){
			$c++;
			if($c > $this->limit){
				break;
			}
			$return[] = ['count'=>$count, 'model'=>$models[$id]];
		}
		return $return;
	}
}