<?php namespace App\Http\Controllers\Backend\Users;
use App\Http\Controllers\Backend\Rels;
/**
 * Controlador para la administración de segmentaciones de los usuarios en la compañía.
 */
class ProfileController extends Rels{
	public $searchField = 'user_id';

	public function onSaveRules($id=null){
		return [
    	'name'             => 'max:64',
      'lastname'         => 'max:64',
      'phone'            => 'max:64',
      'birthday'         => 'date',
      'bio'              => '',
      'sex'              => ['max:1',$this->rule('in',['m','f'])],
      'social'           => 'array',
      'social.facebook'  => 'url|max:255',
      'social.twitter'   => 'url|max:255',
      'social.skype'     => 'max:255'
		];
	}
}