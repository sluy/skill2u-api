<?php namespace App\Http\Controllers\Backend\Users;
use App\Http\Controllers\Backend\SingleRels;
/**
 * Controlador para la administración de segmentaciones de los usuarios en la compañía.
 */
class SegmentationsController extends SingleRels {
	public $with = [];
}
