<?php namespace App\Http\Controllers\Backend;
use Illuminate\Support\Facades\Hash;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
/**
 * Controlador para la administración de categorías de la compañía activa.
 */
class UsersController extends Controller
{
	public $with = [];
	protected $originalPassword = null;


  public function xls() {
    $spreadsheet = new Spreadsheet();
    $excelWriter = new Xls($spreadsheet);
    $spreadsheet->setActiveSheetIndex(0);
    $activeSheet = $spreadsheet->getActiveSheet();
    $titles = ['id', 'email', 'name', 'lastname', 'birthday', 'sex', 'phone'];
    $letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O'];
    $counter = 1;
    foreach($titles as $index => $title) {
      $cell = $letters[$index] . $counter;
      $activeSheet->setCellValue($cell, $title)->getStyle($cell)->getFont()->setBold(true);
    }
    foreach($this->company()->users as $user) {
      $counter++;
      $item = [];
      $item[] = $user->id;
      $item[] = $user->email;
      foreach(['name', 'lastname', 'birthday', 'sex', 'phone'] as $field) {
        $item[] = $user->profile->{$field};
      }
      foreach($item as $index => $value) {
        $cell = $letters[$index] . $counter;
        $activeSheet->setCellValue($cell, $value);
      }
    }
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="users.xls"');
    header('Cache-Control: max-age=0');
    $excelWriter->save('php://output');
    die();
  }

  public function csv() {
    $data = [];
    foreach($this->company()->users as $user) {
      $item = [];
      $item['id'] = $user->id;
      //$item['level'] = $user->level->title;
      $item['email'] = $user->email;
      foreach(['name', 'lastname', 'birthday', 'sex', 'phone'] as $field) {
        $item[$field] = $user->profile->{$field};
      }
      $data[] = $item;
    }
		header("Content-type: text/csv");
		header("Content-Disposition: attachment; filename=result_file.csv");
		header("Pragma: no-cache");
		header("Expires: 0");
		$output = fopen("php://output", "w");
		foreach ($data as $row) {
			fputcsv($output, $row);
		}
		fclose($output);
		die();
  }

	public function beforeDelete($model){
		if($model->unique_company_admin || $model->unique_system_admin){
			return $this->forbidden();
		}
	}

	protected function filterByFullname(&$query, $values) {
		$ids = [];
		if (empty($values)) {
			return;
		}
		if (!is_array($values)) {
			$values = [$values];
		}
		$all = $query->get();
		foreach($all as $user) {
			$fullname = strtolower($user->profile->name . ' ' . $user->profile->lastname);
			foreach($values as $value) {
				if(str_contains($fullname, strtolower($value))) {
					if (!in_array($user->id, $ids)) {
						$ids[] = $user->id;
					}
					break;
				}
			}
		}
		$query = $this->model()->whereIn('id',$ids);
	}

	protected function filterBySegmentation(&$query,$segmentations){
		$ids = [];
		if(!is_array($segmentations)){
			$segmentations = [$segmentations];
		}
		$all = $query->get();

		foreach($all as $user){
			foreach($segmentations as $segmentation){
				foreach($user->segmentations as $usegmentation){
					if($usegmentation->segmentation_id == $segmentation){
						$ids[] = $user->id;
						break;
					}
				}
			}
		}
		$query = $this->model()->whereIn('id',$ids);
	}

	protected function filterByLevel(&$query,$levels){
		$ids = [];
		$all = $query->get();
		foreach($all as $user){
			foreach($levels as $level){
				if($level == $user->level_id){
					$ids[] = $user->id;
					break;
				}
			}
		}
		$query = $this->model()->whereIn('id',$ids);
	}

	protected function filterBySex(&$query,$values,$literal,$all){
		$ids = [];
		foreach($all as $user){
			if(in_array($user->profile->sex,$values)){
				if(!in_array($user->id,$ids)){
					$ids[] = $user->id;
				}
			}
		}
		$query = $this->model()->whereIn('id',$ids);
	}


	protected function orderByActivity($items,$type){
		$pos = [];
		$sorted = [];
		foreach($items as $k => $item){
			$count = $item->{'activity'};
			if(!isset($pos[$count])){
				$pos[$count] = [];
			}
			$pos[$count][] = $item->toArray();
		}
		$type == 'desc' ? krsort($pos) : ksort($pos);
		foreach($pos as $items){
			foreach($items as $item){
				$sorted[] = $item;
			}
		}
		return $sorted;
	}

	protected function orderBySubscribed($items,$type){
		$pos = [];
		$sorted = [];
		foreach($items as $k => $item){
			$count = $item->{'subscribed_count'};
			if(!isset($pos[$count])){
				$pos[$count] = [];
			}
			$pos[$count][] = $item->toArray();
		}
		$type == 'desc' ? krsort($pos) : ksort($pos);
		foreach($pos as $items){
			foreach($items as $item){
				$sorted[] = $item;
			}
		}
		return $sorted;
	}







	public function import(){
		$this->check('import');
		$file = $this->request()->file('file');
		$fh = fopen($file->getPathname(),'r+');
		$models = [];
		while(($row = fgetcsv($fh, 8192)) !== FALSE){
			if(empty($row) || (!filter_var($row[0], FILTER_VALIDATE_EMAIL)) ||
				 $this->model()->where('email',$row[0])->first()){
				continue;
			}
			$email = $row[0];
			$password = isset($row[1]) && !empty($row[1])
									? $row[1]
									: null;
			$ori                 = $this->model();
			$user                = $this->model();
			$user->company_id    = $this->company()->id;
			$user->system_admin  = false;
			$user->company_admin = false;
			$user->email         = $email;
			$user->password      = $password;
			$user = &$user;
			$this->trigger('create',
										 ['model'=>$user,'ori'=>$ori,'cfg'=>[]],'before');
			$user->save();
			$this->trigger('create',
										 ['model'=>$user,'ori'=>$ori,'cfg'=>[]],'after');
			$user->profile;
			$models[] = $user;
		}
		$this->created($models);
	}

	public function beforeCreate($model){
		if(empty($model->password)){
			$model->password = $model->generatePassword();
		}
		$this->originalPassword = $model->password;
	}

	public function beforeSave($model,$ori){
		if(!$model->level_id){
			$lvl = $this->model('App\Models\Level')
					        ->where('company_id',$this->company()->id)
					        ->orderBy('order')
					        ->first();
			$model->level_id = $lvl->id;
		}

		if($model->password != $ori->password){
			$model->password = Hash::make($model->password);
		}
		$model->system_admin = false;
		if(!empty($model->id)){
			//Nos aseguramos que no se quite el estado de administrador de la compañía si
			//el usuario es el único.
			if($ori->company_admin && !$model->company_admin && $ori->unique_company_admin){
				$model->company_admin = true;
			}
			//Nos aseguramos que no se quite el estado de administrador del sistema si
			//el usuario es el único.
			if($ori->system_admin && !$model->system_admin && $ori->unique_system_admin){
				$model->system_admin = true;
			}
		}
	}

	public function afterCreate($model){
		$profile = $this->model('App\Models\User\Profile');
		$profile->user_id  = $model->id;
		$profile->birthday = '1900-01-01';
		$profile->social   = [];
		$profile->save();
		if(!$this->company()->getOptions('notify_users')){
			return;
		}
		$user = $model->toArray();
		$user['profile'] = $profile->toArray();
		$user['password'] = $this->originalPassword;
		//Acá enviar el mail con la contraseña $this->originalPassword.

		$title = 'Bienvenido a Skill2U';
		$message = 'Su cuenta de skill2u ha sido creada por la compañía [company.name]:' .
							 '<br/>Credenciales: <br/><br/>'.
							 'Usuario: [user.email]<br/>'.
							 'Contraseña: [user.password]<br/>';
		$titleview = $this->company()->views()->where('name','user.new.title')->first();
		$messageview = $this->company()->views()->where('name','user.new.message')->first();

		if($titleview){
			$title = $titleview->value;
		}
		if($messageview){
			$message = $messageview->value;
		}
		$this->api('notify')->success(
			$title,
			$message,
			[
				'user'     => $user
			],
			true);
	}

	public function onSaveRules($id=null){
		return array(
			'level_id'       => ['integer',
													 $this->rule('exists','levels','id')
													 ->where(function($query){
													 return $query->where('company_id',
													 											$this->company()->id);
													 })],
      'email'          => [ 'required',
    											  'email',
    											  'max:255',
    											  $this->rule('unique','users')->ignore($id)
    											],
      'password'       => 'min:8',
      'options'        => 'array',
      'company_admin'  => 'boolean',
      'adobe_role'     => 'string'
		);
	}


	/**
	 * Cambia los datos del perfíl de la cuenta autenticada.
	 * @return Illuminate\Http\Response
	 */
	public function profileChange($id){
		$this->check('profileChange');
		$profile = $this->model()->newQuery()->findOrFail($id)->profile;
		foreach($profile->getFillable() as $field){
			$value = ($field == 'photo')
							 ? $this->upload->fromRequest('photo')
							 : $this->input($field,'');

			if($field == 'user_id' || empty($value)){
				continue;
			}
			$profile->{$field} = $value;
		}
		$profile->save();
		return $this->response->updated($profile);
	}

	/**
	 * Reglas para la importación de archivos.
	 * @return array
	 */
	public function onImportRules(){
		return array(
			'file' => ['required',$this->rule('upload','text',2)],
		);
	}

	/**
	 * Reglas al cambiar el perfil.
	 * @return array
	 */
	public function onProfileChangeRules(){
		return array(
      'name'               => 'max:64',
      'lastname'           => 'max:64',
      'phone'              => 'max:64',
      'birthday'           => 'date',
      'photo'              => $this->rule('upload','image',1),
      'bio'                => '',
      'sex'                => ['max:1',$this->rule('in',['m','f'])],
      'social'             => 'array',
      'social.facebook'    => 'url|max:255',
      'social.twitter'     => 'url|max:255',
      'social.skype'       => 'max:255'
  	);
	}
}
