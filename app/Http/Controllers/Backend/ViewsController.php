<?php namespace App\Http\Controllers\Backend;
/**
 * Controlador para la administración de "vistas" de la compañía.
 */
class ViewsController extends Controller {

	/**
	 * Reglas de validación al guardar el registro.
	 * @param  integer $id Id del registro.
	 * @return array
	 */
	public function onSaveRules($id=null){
		return [
			'name'    => [
										'required',
										'string',
										'max:64',
										$this->rule('unique','views')->where(function($query) use($id){
											return $query->where('company_id',$this->company()->id)
																	 ->where('id','!=',$id);
										})
									],
			'value'   => 'required|string'
		];
	}
}