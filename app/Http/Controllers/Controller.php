<?php namespace App\Http\Controllers;
use Api\Routing\Controller\CRUD;
/**
 * Controlador base para el sistema
 */
class Controller extends CRUD {

	public function __construct(){
		if($this->company()){
			if(in_array('company_id', $this->model()->getFillable())){
				$this->conditions['company_id'] = $this->company()->id;
			}
			if($this->crossLookup && $this->model()->hasCrossParent()){
				//Buscamos si el registro padre del modelo actual pertenece a la compañía.
				$this->model()->getCrossParent()
											->newQuery()
											->where('company_id',$this->company()->id)
											->where($this->model()->getCrossParent()->getKeyName(),
															$this->route(0,0,'integer'))
											->firstOrFail();
			}
		}
		if($this->crossLookup && $this->model()->hasCrossRel()){
			$rel = $this->model()->getCrossRel();
			//Nos aseguramos que el registro editado pertenezca a la empresa.
			$this->on('beforeProcess', function($model,$ori) use ($rel){
				if($ori->{$rel->getCrossKeyName()} && 
					 $ori->{$rel->getCrossKeyName()} != $model->{$rel->getCrossKeyName()}){
					$rel->where($rel->getKeyName(),$ori->{$rel->getCrossKeyName()})
							->where('company_id',$this->company()->id)
							->firstOrFail();
				}
				$rel->where($rel->getKeyName(),$ori->{$rel->getCrossKeyName()})
						->where('company_id',$this->company()->id)
						->firstOrFail();
			});
		}
		parent::__construct();
	}
}