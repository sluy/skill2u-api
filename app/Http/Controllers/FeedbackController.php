<?php namespace App\Http\Controllers;
/*
 * Controlador base para retroalimentación de los usuarios con recursos.
 */
class FeedbackController extends Controller
{
	public $crossLookup = false;

	public $with      = ['user'];

	public $model     = 'App\Models\User\Feedback';

	public $makeUnique = false;

	public function boot(){
		$this->statics['user_id']        = $this->user()->id;
		$this->conditions['name']        = $this->feedbackName();
		$this->conditions['type']        = $this->feedbackType();
		$this->conditions['primary_key'] = $this->route('fid',0,'integer');
		$this->injectEvents();
		$this->injectRules();
	}


	protected function injectEvents(){
		$this->on('beforeSave',function($model,$old){
			if($old->user_id != $this->user()->id){
				$this->forbidden();
			}
		});
		$this->on('beforeDelete', function($model){
			if($model->user_id != $this->user()->id){
				$this->forbidden();
			}
		});
		$this->on('beforeRestore', function($model){
			if($model->user_id != $this->user()->id){
				$this->forbidden();
			}
		});
	}

	protected function injectRules(){
		$this->addRules('save',function($id=null){
			$rules = [
				'primary_key' => [
					'required',
					$this->rule('exists', $this->feedbackModel()->getTable(),'id')
				],
			];
			if($this->makeUnique){
				$rules['primary_key'][] = $this->rule('unique',$this->model()->getTable())
				->where(function($q) use($id){
					$q->where('user_id', '!=',$this->user()->id)
					  ->where('type', $this->feedbackType())
					  ->where('name', $this->feedbackName())
					  ->where('primary_key', $id);
				});
			}
			return $rules;
		});
	}

	public function feedbackType(){
		if(property_exists($this, 'feedbackType')){
			return $this->feedbackType;
		}
		$type = @strtolower(end(explode('\\',get_class($this))));

		if(ends_with($type,'controller')){
			$type = substr($type, 0,-10);
		}
		return $this->cast('singularize',$type);
	}

	public function feedbackName(){
		if(property_exists($this, 'feedbackName')){
			return $this->feedbackName;
		}
		$name = explode('\\',get_class($this));
		return $this->cast('singularize',strtolower($name[count($name)-2]));
	}

	public function feedbackModel($id=null){
		$model = property_exists($this, 'feedbackModel')
						 ? $this->model($this->feedbackModel)
						 : $this->model('App\\' . $this->feedbackName());
		if(!$id){
			return $model;
		}
		return $model->newQuery()->where('type',$this->feedbackType())
														 ->where('name',$this->feedbackName())
														 ->where('id',$id)
														 ->first();
	}

}