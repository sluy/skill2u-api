<?php namespace App\Http\Controllers\Frontend;

ini_set('max_execution_time', 300);

use App\Http\Controllers\Controller,
		App\Models\Course\Member,
		App\Models\Exam,
		Carbon\Carbon;
/**
 * Controlador para la administración de publicaciones en la compañía activa.
 */
class EvaluationsController extends Controller
{
	public $with = [];

	public function paginate($cfg=null){
		/*if ($this->user()->company_admin) {
			return parent::paginate($cfg);
    }*/
		$cfg = $this->cast('array',$cfg);
		$cfg['query'] = $this->company->availableEvals($this->user(),true);
		return parent::paginate($cfg);
	}
	/**
	 * Inscribe al usuario en la evaluación suministrada.
	 * @param  [type] $id Id de la evaluación.
	 * @return
	 */
	public function join($id){
		$eval = $this->find($id,['return' => 'model']);
		$user = $this->user();
		if(!$eval){
			return $this->notFound();
		}
		if(!$this->company->canEvaluate($user,$eval)){
			return $this->forbidden();
		}

		$ueval = $user->evals()->where('evaluation_id',$eval->id)->first();
		if($ueval){
			return $this->ok($ueval);
		}
		$ueval = $this->model('App\Models\User\Evaluation');
		$ueval->user_id = $user->id;
		$ueval->evaluation_id = $eval->id;
		$ueval->presented = false;
		$ueval->approved_percent = $eval->exam->approved_percent;
		$ueval->score = '0.00';
		$ueval->exam = $this->makeQuestions($eval->exam);
		$ueval->save();
		//Logueamos que se inscribió al curso/evaluación
		$this->auth->log('eval.join',$eval->id);
		return $this->created($ueval);
	}

	/**
	 * Evalúa las respuestas del exámen.
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function evaluate($id){
		$eval = $this->find($id,['return'=>'model']);
		if(!$eval){
			return $this->notFound();
		}
		$ueval = $this->user()->evals()->where('evaluation_id',$eval->id)->first();
		if(!$ueval){
			return $this->notFound();
		}
		if($ueval->presented){
			return $this->forbidden();
		}

		$ueval->presented = true;
		$ueval->score     = '0.00';
		$questions    = $ueval->exam;
		$uanswers     = $this->input('answers',[],'array');

    $qCount = count($questions);
    $qWeight = (100/$qCount);

		foreach($questions as $qKey => $question){
			$correct = $question['correct_answers'];
			$weight  = ($qWeight / count($correct));
			foreach($question['answers'] as $aKey => $answer){
				$answer['correct'] = in_array($answer['id'],$correct);
				$answer['selected'] = isset($uanswers[$qKey]) && is_array($uanswers[$qKey]) &&
															in_array($aKey,$uanswers[$qKey]);
				if($answer['selected'] && $answer['correct']){
					$ueval->score += $weight;
				}
				//Acá comprobar si el usuario lo seteó.
				$question['answers'][$aKey] = $answer;
			}
			$questions[$qKey] = $question;
		}
		$ueval->exam = $questions;
		$ueval->score = number_format($ueval->score, 2, '.', '');
		$ueval->save();
		$this->auth->log('eval.presented',$ueval->evaluation_id);
		return $this->updated($ueval);
	}

	public function makeQuestions(Exam $exam){
		$questions = $exam->questions;
		$count     = count($questions);
		$auto      = $exam->autoWeight;
		$limit     = intval($exam->questions_quantity);
		$weight    = 0;
		//Si no hay cantidad literal, tomamos todos.
		if(!$limit){
			$limit = $count;
		}else{
			$auto = true;
		}
		//Si el "peso" es automático
		if($auto){
			$weight = (100/$limit);
		}
		$unsorted = [];
    $ignore = [];

    if (!$exam->random) {
      foreach($questions as $qKey => $question) {
        $question['id'] = $qKey;
        $question['weight'] = $weight;
        $question['answers'] = $this->makeAnswers($question, false);
        $question['select'] = count($question['correct_answers']);
        unset($question['answers_quantity']);
        ksort($question);
        $unsorted[$qKey] = $question;
      }
    } else {
      for($n=0;$n<$limit;$n++){
        $qKey = null;
        while(1){
          $qKey = mt_rand(0,($count-1));
          if(!in_array($qKey, $ignore)){
            break;
          }
        }
        $ignore[] = $qKey;
        $question = $questions[$qKey];
        $question['id'] = $qKey;
        if($weight){
          $question['weight'] = $weight;
        }
        $question['answers'] = $this->makeAnswers($question, true);
        $question['select'] = count($question['correct_answers']);
        unset($question['answers_quantity']);
        ksort($question);
        $unsorted[$n] = $question;
      }
    }
		return $unsorted;
	}

	public function makeAnswers($question, $random=false){
		$rawAnswers = $question['answers'];
		$aCount       = count($rawAnswers);
		$correctCount = count($question['correct_answers']);
		$aQuantity    = count($rawAnswers);

		if(isset($question['answers_quantity']) && is_int($question['answers_quantity']) &&
			 $question['answers_quantity'] > 0){
			$aQuantity = $question['answers_quantity'];
		}
		$answers = [];
    $ignore  = [];

    if ($random === false) {
      foreach($rawAnswers as $aKey => $row) {
        $answers[$aKey] = [
          'id'   => $aKey,
          'text' => $row
        ];
      }
    } else {
      foreach($question['correct_answers'] as $aKey){
        $pos = null;
        while(1){
          $pos = mt_rand(0,$aQuantity-1);
          if(!array_key_exists($pos, $answers)){
            break;
          }
        }
        $answers[$pos] = [
          'id'   => $aKey,
          'text' => $rawAnswers[$aKey]
        ];
        $ignore[] = $aKey;
      }
      for($pos=0;$pos < $aQuantity;$pos++){
        if(array_key_exists($pos, $answers)){
          continue;
        }
        while(1){
          $aKey = mt_rand(0,$aCount-1);
          if(!in_array($aKey, $ignore)){
            break;
          }
        }
        $answers[$pos] = [
          'id'   => $aKey,
          'text' => $rawAnswers[$aKey]
        ];
        $ignore[] = $aKey;
      }
    }

		ksort($answers);
		return $answers;
	}
}
