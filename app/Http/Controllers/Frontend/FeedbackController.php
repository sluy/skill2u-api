<?php namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Controller;
use App\Models\User\Feedback;
use App\Models\Media;
use Illuminate\Http\UploadedFile;

class FeedbackController extends Controller{

  public $crossLookup = false;

  public $model     = 'App\Models\User\Feedback';

  protected $uniqueTypes = ['like'];

  protected $validTypes = ['comment', 'like', 'rate'];

  protected $validNames = ['post', 'feedback'];

  public $with = [];

	public function boot(){
    if ($this->getType() == 'comment') {
      $this->with[] = 'media';
    }

    $this->statics['user_id'] = $this->user()->id;
    $this->conditions['name'] = $this->getName();
    $this->conditions['type'] = $this->getType();
    $this->conditions['primary_key'] = $this->getPrimaryKey();
    if (!$this->isValidType() || !$this->isValidName()) {
      $this->notFound();
    }
    $this->injectRules();
  }

  public function getName() {
    return $this->route('name', '', 'string');
  }

  public function getType() {
    return $this->route('type', '', 'string');
  }

  public function getPrimaryKey() {
    return $this->route('pk', '', 'integer');
  }

  protected function isUnique() {
    return in_array($this->getType(), $this->uniqueTypes);
  }

  public function isValidType() {
    return in_array($this->getType(),$this->validTypes);
  }

  public function isValidName() {
    return in_array($this->getName(), $this->validNames);
  }

  protected function beforeRestore($model) {
    //Si no es el usuario que realizó el "feedback" y no es administrador
    //Denegamos el acceso.
    if($model->user_id != $this->user()->id && !$this->user()->company_admin){
      $this->forbidden();
    }
  }

  protected function beforeDelete($model) {
    //Si no es el usuario que realizó el "feedback" y no es administrador
    //Denegamos el acceso.
    if($model->user_id != $this->user()->id && !$this->user()->company_admin){
      $this->forbidden();
    }
  }

  protected function beforeSave($model) {
    //Si no es el usuario que realizó el "feedback" y no es administrador
    //Denegamos el acceso.
    if($model->user_id != $this->user()->id && !$this->user()->company_admin){
      $this->forbidden();
    }
    $rel = $model->rel;

    if ($this->getType() == 'rate') {
      $max = (isset($rel->options['feedback']['rate']['max'])) ? (int) $rel->options['feedback']['rate']['max'] : 5;
      if ($max < 2) {
        $max = 5;
      }
      $model->data = (int) $model->data;
      if ($model->data > $max) {
        $model->data = $max;
      }
    }
    if (!$model->id) {
      $current = $this->feedbackModel($model->primary_key);
      if($current) {
        switch($this->getType()) {
          case 'like':
            $current->delete();
            return $this->ok($current);
            //Informar ?
          case 'rate':
            $current->data = $model->data;
            $current->save();
            return $this->ok($current);
            //Informar ?
        }
      }
    }
  }

  protected function afterSave($model) {
    if ($this->getType() === 'comment' && app()->request->has('resource') && app()->request->resource instanceof UploadedFile) {
      $media = new Media();
      $media->type = "feedback";
      $media->primary_key = $model->id;
      $media->resource = app('Api\Api')->upload->fromFile(app()->request->resource);
      $media->save();
    }
  }


	public function feedbackModel($pk=null){
		return Feedback::where('type',$this->getType())
                    ->where('name',$this->getName())
                    ->where('primary_key',$pk)
                    ->first();
	}

  protected function injectRules(){
		$this->addRules('save',function($id=null){
      $ruleName = 'get' . ucfirst($this->getType()) . 'Rules';
      return $this->{$ruleName}($id);

		});
  }

  protected function getLikeRules($id=null) {
    return [
      'primary_key' => [
        'required'
      ],
    ];
  }

  protected function getCommentRules($id=null) {
    return [
      'primary_key' => [
        'required',
      ],
      'data' => 'required|string',
      'resource' => 'file'
    ];
  }

  protected function getRateRules($id=null) {
    return [
      'primary_key' => [
        'required',
      ],
      'data' => 'integer'
    ];
  }
}
