<?php namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Controller;
use App\Models\User\Feedback;
use App\Models\Media;
use Illuminate\Http\UploadedFile;

class MediaController extends Controller{
  
  public $crossLookup = false;

  public $model     = 'App\Models\Media';
  
	public function boot(){
    $this->conditions['type'] = $this->getType();
    $this->conditions['primary_key'] = $this->getPrimaryKey();
  }
  

  public function getType() {
    return $this->route('type', '', 'string');
  }

  public function getPrimaryKey() {
    return $this->route('pk', '', 'integer');
  }

  public function onSaveRules() {
    return [
      'resource' => 'required'
    ];
  }

}