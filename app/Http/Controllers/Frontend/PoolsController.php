<?php namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Controller;

class PoolsController extends Controller {

  public $crossLookup = false;

  protected function filterByAvailables(&$query, $values) {
		$ids = [];
		if (empty($values)) {
			return;
		}
		if (!is_array($values)) {
			$values = [$values];
		}
		$all = $query->get();
		foreach($all as $pool) {
      if($pool->can_vote && !in_array($pool->id, $ids)) {
        $ids[] = $pool->id;
      }
		}
		$query = $this->model()->whereIn('id',$ids);
	}

  public function vote($id) {
    $model = $this->model()->findOrFail($id);
    // No está activa
    if (!$model->is_open) {
      return $this->notFound();
    }
    // Ya votó o la segmentación no corresponde.
    if (!$model->can_vote) {
      return $this->forbidden();
    }
    $raw = $this->input('value',[],'array');
    $value = [];
    for($n=0;$n<$model->valid_answers;$n++) {
      if (array_key_exists($n, $raw)) {
        $raw[$n] = (int) $raw[$n];
        if (array_key_exists($raw[$n], $model->answers) && !in_array($raw[$n], $value)) {
          $value[] = $raw[$n];
        }
      }
    }
    // Si la respuesta está vacía...
    if (empty($value)) {
      return $this->error();
    }


    $vote = $this->model('App\Models\Pool\Vote');
    $vote->value = $value;
    $vote->user_id = $this->user()->id;
    $vote->pool_id = $model->id;

    // Creamos el voto
    $this->model('App\Models\Pool\Vote')->create([
      'user_id' => $this->user()->id,
      'pool_id' => $model->id,
      'value'   => $value
    ]);
    $data = $model->toArray();
    $data['answers_votes_count'] = $model->answer_votes_count;
    $data['answers_votes_percent'] = $model->answer_votes_percent;
    $data['votes_total'] = $model->votes_total;
    return $this->created($data);
  }
}
