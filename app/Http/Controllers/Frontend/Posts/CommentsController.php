<?php namespace App\Http\Controllers\Frontend\Posts;
use App\Http\Controllers\FeedbackController;

class CommentsController extends FeedbackController{
	
	public function afterUpdate($model,$ori){
		$model->user->profile;
		$this->auth->log('post.comment.update',$model->primary_key,$ori->toArray());
	}

	public function afterDelete($model){
		$this->auth->log('post.comment.delete',$model->primary_key,$model->toArray());
	}

	public function afterCreate($model){
		$model->user->profile;
		if(!$this->company()->getOptions('notify_comments')){
			return;
		}

		$comment = [
			'user' => $this->model('App\Models\User')->find($model->user_id),
			'message' => $model->data,
			'created_at' => $model->created_at,
			'updated_at' => $model->updated_at
		];
		$post = $this->model('App\Models\Post')->find($model->primary_key);
		$user = $post->user;
		$this->auth->log('post.comment.create',$model->primary_key,$model->toArray());
		//NO notificamos si el usuario que comentó es el mismo que el que hizo la publicación
		if($comment['user']->id == $user->id){
			return;
		}

		$user->profile;
		$comment['user']->profile;
		$comment['user'] = $comment['user']->toArray();

		$data = [
			'comment' => $comment,
			'post'    => $post->toArray(),
			'user'    => $user->toArray()
		];

		$title = 'Se ha comentado tu publicación [post.title]';
		$message = '[comment.user.profile.name] [comment.user.profile.lastname] ha comentado'.
							 ' tu publicación [post.title]: <br/><br/> [comment.message]';
		$titleview   = $this->company()->views()->where('name','comment.new.title')->first();
		$messageview = $this->company()->views()->where('name','comment.new.message')->first();
		
		if($titleview){
			$title = $titleview->value;
		}
		if($messageview){
			$message = $messageview->value;
		}
		$this->api('notify')->info($title,$message,$data,true);
	}

	public function onSaveRules($id=null){
		return [
			'data' => 'required|string|min:5|max:500',
		];
	}
}