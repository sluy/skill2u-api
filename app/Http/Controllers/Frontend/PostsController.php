<?php namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Controller,
		Carbon\Carbon;
/**
 * Controlador para la administración de publicaciones en la compañía activa.
 */
class PostsController extends Controller
{
	/**
	 * Relaciones a levantar en los modelos devueltos.
	 * @var array
	 */
	public $with = [];

	protected function filterByCategory(&$query,$values){
		$ids = [];
		$all = $query->get();
			$values = array_map('intval',$values);
			foreach($all as $post){
				foreach($post->categories as $row){
					if(in_array($row->category_id,$values)){
						if(!in_array($post->id,$ids)){
							$ids[] = $post->id;
						}
						break;
					}
				}
		}
		$query = $this->model()->whereIn('id',$ids);
	}

	protected function filterByTag(&$query,$values){
		$ids = [];
		$all = $query->get();
		$values = array_map('intval',$values);
		foreach($all as $post){
			foreach($post->tags as $row){
				if(in_array($row->tag_id,$values)){
					if(!in_array($post->id,$ids)){
						$ids[] = $post->id;
					}
					break;
				}
			}
		}
		$query = $this->model()->whereIn('id',$ids);
	}
	protected function filterByDateRange(&$query,$values,$type,$all){
		$ids = [];
		$all = $query->get();
		foreach($all as $post){
			if($post->created_at >= $values[0] && $post->created_at <= $values[1]){
				if(!in_array($post->id, $ids)){
					$ids[] = $post->id;
				}
			}
		}
		$query = $this->model()->whereIn('id',$ids);
	}

	/**
	 * Método al inicio del controlador.
	 * Establece los campos estáticos de la instancia.
	 * @return void
	 */
	public function boot(){
		$this->statics['user_id'] = $this->auth->user()->id;
		$this->statics['date']    = Carbon::now()->toDateTimeString();
		$this->statics['news']    = false;
		$this->statics['title']   = '';
	}

	public function like($id){
		$post = $this->company()->posts()->find($id);
		if(!$post){
			$this->notFound();
		}
		$user = $this->user();
		$like = $this->model('App\Models\User\Feedback')->where('name','post')
																				        		->where('type','like')
																						        ->where('user_id',$user->id)
						        												        ->where('primary_key',$id)
										        								        ->first();
		if(!$like){
			$like = $this->model('App\Models\User\Feedback');
			$like->user_id = $user->id;
			$like->name = 'post';
			$like->type = 'like';
			$like->primary_key = $id;
			$like->save();
			$this->auth->log('post.like.create',$post->id);
			return $this->created();
		}
		$like->delete();
		$this->auth->log('post.like.delete',$post->id);
		return $this->deleted();
	}

	/**
	 * Pagina las publicaciones de la compañía.
	 * @param  array|null $cfg Configuración adicional de la búsqueda.
	 * @return Illuminate\Http\Response Devolverá las publicaciones paginadas.
	 *                                  De los recursos discriminará aquellos cuya
	 *                                  segmentación no esté habilitada para el usuario
	 *                                  actualmente autenticado.
	 */
	public function paginate($cfg=null){
		$cfg = $this->cast('array',$cfg);
		$cfg['query'] = $this->model()->allowed($this->user());
		return parent::paginate($cfg);
	}


	public function afterFound($model){
		$this->auth->log('post.view',$model->id);
	}

	public function afterCreate($model){
		$this->auth->log('post.create',$model->id);
	}

	public function afterUpdate($model,$ori){
		$this->auth->log('post.update',$model->id,$ori->toArray());
	}

	public function afterDelete($model){
		$this->auth->log('post.delete',$model->id,$model->toArray());
	}


	/**
	 * Evento disparado antes de guardar (crear,actualizar) una publicación.
	 * Elimina el recurso si el tipo de publicación es sólo texto.
	 * @param  Illuminate\Database\Eloquent\Model &$model Instancia del modelo a guardar.
	 * @return void
	 */
	public function beforeSave($model){
		if(empty($model->type)){
			$model->resource = '';
		}
	}
	/**
	 * Evento ejecutado antes de eliminar el registro.
	 * @param  Illuminate\Database\Eloquent\Model $model
	 * @return Illuminate\Http\Response|null             Si el usuario que elimina el
	 * registro no es el qué lo creó (y no es administrador), enviará forbidden, de lo
	 * contrario NULL.
	 */
	public function beforeDelete($model){
		$user = $this->user();
		if($model->user_id != $user->id && !$user->admin){
			return $this->response->forbidden();
		}
	}

	/**
	 * Reglas de validación al guardar (crear/editar) una publicación.
	 * @param  integer|null $id Id de la publicación. Será NULL si se trata de la creación
	 *                          de un nuevo registro.
	 * @return array
	 */
	protected function onSaveRules($id=null){
		$rules = [
  		'type'     => ['string',$this->rule('in',['picture','youtube','video'])],
		'content'  => 'string|min:5',
  		'resource' => ''
		];
		switch ($this->input('type')) {
			case 'picture':
				$rules['resource'] = ['required',$this->rule('upload','image',1,255)];
				break;
			case 'video':
				$rules['resource'] = ['required',$this->rule('upload','video',64,255)];
				break;
			case 'youtube':
				$rules['resource'] = ['required','string','url','max:255',$this->rule('youtube')];
				break;
		}
		return $rules;
	}
}
