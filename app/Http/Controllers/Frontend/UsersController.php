<?php
namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Controller;
/**
 * Controlador para la devolución de recursos de la librería.
 */
class UsersController extends Controller
{
	protected $limit = 5;

	public function boot(){
		$this->limit = $this->input('limit',10,'integer');
	}

	public function ranking(){
		$performance = $this->company()->performances()
												->where('finished',true)
												->orderBy('finish_at','desc')
												->first();
		if($performance){
			return $performance->scores()
												->orderBy('total','desc')
												->limit($this->limit)
												->with('user.profile')
												->get();
		}
		return [];
	}

	public function scores(){
		return $this->user()->evals()
												->where('presented',true)
												->orderBy('updated_at','desc')
												->with('evaluation')
												->get();
	}
}