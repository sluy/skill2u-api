<?php namespace App\Http\Controllers\Misc;
use Laravel\Lumen\Routing\Controller;
use AdobeConnectClient\Connection\Curl\Connection;
use AdobeConnectClient\Client;

/**
 * Controlador para Adobe Connect.
 */
class AdobeController extends Controller {
  
  protected $baseUrl = 'https://skill2u.adobeconnect.com';
  protected $username = 'VERONICAFORCINITI@GMAIL.COM';
  protected $password = '85ekh4k';


  public function getConn() {
    return new Connection($this->baseUrl);
  }

  public function getClient(Connection $connection = null, $login = false) {
    if (!$connection) {
      $connection = $this->getConn();
    }
    $client = new Client($connection);
    if ($login){
      $client->login($this->username, $this->password);
    }
    return $client;
  }

  public function session() {
    $client = $this->getClient(null, true);
    $token = $client->getSession();
    header("Content-type: text/xml");
    echo '<?xml version="1.0" encoding="utf-8"?>
<results>
  <status code="ok"/>
  <OWASP_CSRF_TOKEN>
    <token>'.$token.'</token>
  </OWASP_CSRF_TOKEN>
</results>';
    die();
  }

  public function make() {
    extract(request()->only('token','action','params'));
    if (!is_array($params)) {
      $params = [];
    }
    $client = $this->getClient(null);
    $client->setSession($token);
    $params['session'] = $token;
    $params['action'] = $action;
    $append = [];
    foreach($params as $key => $value) {
      $append[] = $key . '=' . $value;
    }
    $url = $this->baseUrl . '/api/xml' . '?' . implode('&',$append);

    $ch = curl_init($url);
    curl_setopt($ch,CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    echo curl_exec($ch);
    curl_close($ch);
    die();
  }
}