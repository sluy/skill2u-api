<?php namespace App\Http\Controllers\Misc;
use Laravel\Lumen\Routing\Controller;
use App\Models\Banner;
/**
 * Controlador para el procesamiento de urls externas.
 */
class BannersController extends Controller {
  
    public function all() {
        app('Api\Api')->get('response')->render(Banner::all()->toArray(), 200);
    }
}