<?php namespace App\Http\Controllers\Misc;
use Laravel\Lumen\Routing\Controller;
use App\Models\Company;
/**
 * Controlador para el procesamiento de urls externas.
 */
class CompaniesController extends Controller {
  
    public function all() {
        app('Api\Api')->get('response')->render(Company::all(), 200);
    }
}