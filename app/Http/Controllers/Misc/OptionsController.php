<?php namespace App\Http\Controllers\Misc;
use Laravel\Lumen\Routing\Controller;
use App\Models\Option;
/**
 * Controlador para el procesamiento de urls externas.
 */
class OptionsController extends Controller {
  
    public function all() {
        app('Api\Api')->get('response')->render(Option::all(), 200);
    }

    public function show($name) {
      $option = Option::where('name', $name)->first();
      if (!$option) {
        app('Api\Api')->get('response')->render(null, 404);
      }
      app('Api\Api')->get('response')->render($option, 200);
    }

    public function store() {
      $tmp = request()->options;
      if (is_string($tmp)) {
        $tmp = json_decode($tmp);
      }
      if (!is_array($tmp)) {
        $tmp = [];
      }
      foreach($tmp as $name => $value) {
        $option = Option::where('name', $name)->first();
        if (!$option) {
          $option = new Option;
          $option->name = $name;
        }
        $option->value = $value;
        $option->save();
      }
      return app('Api\Api')->get('response')->render(null, 200);
    }

    public function delete() {
      $tmp = request()->options;
      if (is_string($tmp)) {
        $tmp = json_decode($tmp);
      }
      if (!is_array($tmp)) {
        $tmp = [];
      }
      foreach($tmp as $name) {
        $option = Option::where('name', $name)->first();
        if ($option) {
          $option->delete();
        }
      }
      return app('Api\Api')->get('response')->render(null, 200);
    }



}