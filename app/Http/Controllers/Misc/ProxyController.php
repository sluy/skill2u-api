<?php namespace App\Http\Controllers\Misc;
use Laravel\Lumen\Routing\Controller;
use AdobeConnectClient\Connection\Curl\Connection;
use AdobeConnectClient\Client;

/**
 * Controlador para el procesamiento de urls externas.
 */
class ProxyController extends Controller {
  public function curl() {
    extract(request()->only('url','method','params'));
    if(filter_var($url, FILTER_VALIDATE_URL) === false) {
      die('Invalid url');
    }
    //$url .= (is_array($params) && !empty($params)) ? ('?'. http_build_query($params)) : '';
    if (is_array($params) && !empty($params)) {
      $append = [];
      foreach($params as $key => $value) {
        $append[] = $key . '=' . $value;
      }
      $url .= '?' . implode('&',$append);
    }
    
    if(!$method || !is_string($method)) {
      $method = 'get';
    }
    $method = strtolower($method);
    //echo file_get_contents($url);

    $ch = curl_init($url);
    curl_setopt($ch,CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

    echo curl_exec($ch);
    curl_close($ch);
    die();
  }
}