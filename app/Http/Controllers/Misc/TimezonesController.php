<?php namespace App\Http\Controllers\Misc;
use Laravel\Lumen\Routing\Controller;
use App\Models\Banner;
/**
 * Controlador para las zonas horarias.
 */
class TimezonesController extends Controller {
  
    public function all() {
        app('Api\Api')->get('response')->render(timezone_identifiers_list(), 200);
    }
}