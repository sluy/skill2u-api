<?php namespace App\Http\Controllers;
use Illuminate\Http\Request,
		Illuminate\Support\Facades\Hash,
		App\Company,
		App\User,
		App\UserProfile;
/**
 * Configura datos base (o de prueba) del sistema.
 */
class SetupController extends Controller
{
	/**
	 * Configura el grupo de administración y usuario administrador del sistema.
	 * @return Void
	 */
  public function auth(){

  	$company = new Company;
  	$company->name = 'Skill2U';
  	$company->status = true;
  	$company->options = [
  		'info' => 'Primera compañía del sistema.'
  	];
  	$company->save();

		//Usuario
  	$user = new User;
  	$user->email      = 'admin@skill2u.com';
  	$user->password   = Hash::make('112321Asd');
  	$user->company_id = $company->id;
  	$user->admin      = true;
  	$user->options    = ['allow_multiple_connections' => true];
  	$user->save();

  	$profile = new UserProfile;
  	$profile->user_id  = $user->id;
  	$profile->name     = 'Admin';
  	$profile->lastname = 'Admin';
  	$profile->sex      = 'm';
  	$profile->birthday = '1983-12-26';
  	$profile->bio      = 'Administrador de Skill2u';
  	$profile->phone    = '+584261120951';
  	$profile->social   = [
		  'facebook' => 'https://facebook.com/fbId',
      'twitter'  => 'https://twitter.com/twId',
      'skype'  => 'skypeId',
    ];
    $profile->save();
  }
}