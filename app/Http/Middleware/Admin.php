<?php
namespace App\Http\Middleware;
use Api\Http\Middleware\ApiMiddleware,
		Closure;
/**
 *  Middleware para la verificación de usuarios autenticados.
 */
class Admin extends ApiMiddleware
{
  /**
   * Maneja la petición entrante.
   * Si el usuario no está autenticado devolverá error 401.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \Closure  $next
   * @return mixed
   */
  public function handle($request, Closure $next,$guard=null)
  {
  	$user = $this->api->auth->user();
		if($user && $user->company && $user->company_admin){
      if(isset($user->company->options['timezone'])) {
        config('app.timezone', $user->company->options['timezone']);
      }
      return $next($request);
  	}
  	return $this->api->response->forbidden();
  }
}
