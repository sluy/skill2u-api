<?php
namespace App\Http\Middleware;
use Api\Http\Middleware\ApiMiddleware,
		Closure;
/**
 *  Middleware para la verificación de usuarios autenticados.
 */
class Sysop extends ApiMiddleware
{
  /**
   * Maneja la petición entrante.
   * Si el usuario no está autenticado devolverá error 401.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \Closure  $next
   * @return mixed
   */
  public function handle($request, Closure $next,$guard=null)
  {
		if(!$this->api->auth->guess() && $this->api->auth->user()->system_admin){
			return $next($request);
  	}
  	return $this->api->response->forbidden();
  }
}
