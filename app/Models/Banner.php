<?php namespace App\Models;

class Banner extends Model
{
	protected $fillable = [ 'company_id',
                            'type',
                            'resource',
                            'location',
                            'options'];
                            
  public function getOptionsAttribute($raw) {
    if (is_string($raw) && !empty($raw)) {
      return json_decode($raw);
    }
    return [];
  }

  public function setOptionsAttribute($value) {
    if (is_array($value) || is_object($value)) {
      $value = json_encode($value);
    } else {
      $vlue = null;
    }
    $this->attributes['options'] = $value;
  }
}