<?php
namespace App\Models;
class Category extends Model
{
	protected $fillable = ['company_id','title','info','picture', 'color','options'];

  public function getOptionsAttribute($raw) {
    if (is_string($raw) && !empty($raw)) {
      return json_decode($raw);
    }
    return [];
  }

  public function setOptionsAttribute($value) {
    if (is_array($value) || is_object($value)) {
      $value = json_encode($value);
    } else {
      $vlue = null;
    }
    $this->attributes['options'] = $value;
  }

  public function company(){
  	return $this->belongsTo('App\Models\Company','company_id');
  }

  public function libs(){
  	return $this->hasMany('App\Models\Lib\Category','category_id');
  }

  public function posts() {
    return $this->hasMany('App\Models\Post\Category', 'category_id');
  }
}
