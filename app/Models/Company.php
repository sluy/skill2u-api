<?php
namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;
use Api\Database\Eloquent\Configurable;

class Company extends Model
{
	use SoftDeletes,Configurable;

	protected $fillable = ['name','logo','favicon', 'options'];

	protected $fillableOptions = [
    'info',
    'copyright',
		'user_eval_weight',
		'use_user_eval',
		'auto_performance',
		'notify_courses',
		'notify_comments',
		'notify_news',
		'notify_users',
		'timezone'
	];

	protected $castsOptions = [
		'info'             => 'string',
    'copyright'        => 'string',
		'user_eval_weight' => 'float',
		'auto_performance' => 'boolean',
		'notify_courses'   => 'boolean',
		'notify_comments'  => 'boolean',
		'notify_news'      => 'boolean',
		'notify_users'     => 'boolean'
	];

	protected $casts = [
		'status' => 'boolean'
	];

	public $rels = ['users','segmentations','posts','categories','tags','libs','exams','courses'];

	public function levels(){
		return $this->hasMany('App\Models\Level','company_id');
	}

  public function users(){
  	return $this->hasMany('App\Models\User','company_id');
  }

  public function segmentations(){
  	return $this->hasMany('App\Models\Segmentation', 'company_id');
  }

  public function posts(){
  	return $this->hasMany('App\Models\Post', 'company_id');
  }

  public function categories(){
  	return $this->hasMany('App\Models\Category','company_id');
  }

  public function tags(){
  	return $this->hasMany('App\Models\Tag','company_id');
  }

  public function libs(){
  	return $this->hasMany('App\Models\Lib','company_id');
  }
  
  public function exams(){
  	return $this->hasMany('App\Models\Exam','company_id');
  }

  public function courses(){
  	return $this->hasMany('App\Models\Course','company_id');
  }

  public function evals(){
  	return $this->hasMany('App\Models\Evaluation','company_id');
  }

  public function openEvals(){
  	$now = Carbon::now()->toDateString();
  	return $this->hasMany('App\Models\Evaluation','company_id')
  						  ->where('start_at','<=',$now)
  						  ->where('finish_at','>=',$now);
  }


  public function performances(){
  	return $this->hasMany('App\Models\Performance','company_id');
  }

  public function views(){
  	return $this->hasMany('App\Models\View', 'company_id');
  }

  public function periods(){
  	return $this->hasMany('App\Models\Period','company_id');
  }
}
