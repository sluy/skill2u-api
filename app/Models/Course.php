<?php
namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes,
		App\Models\Course\Session as CourseSession,
		Carbon\Carbon;

class Course extends Model
{
	use SoftDeletes;

	protected $fillable = [
		'company_id',
		'exam_id',
		'title',
		'info',
		'picture',
		'type',
    'resource',
    'options'
	];

  public function getOptionsAttribute($raw) {
    if (is_string($raw) && !empty($raw)) {
      return json_decode($raw);
    }
    return [];
  }

  public function setOptionsAttribute($value) {
    if (is_array($value) || is_object($value)) {
      $value = json_encode($value);
    } else {
      $vlue = null;
    }
    $this->attributes['options'] = $value;
  }

	public function delete() {
		foreach(['childs', 'evals'] as $name) {
			if($this->{$name}) {
				foreach($this->{$name} as $rel) {
					$rel->delete();
				}
			}
		}
		parent::delete();
	}

	public function company(){
		return $this->belongsTo('App\Models\Company','company_id');
	}

	public function exam(){
		return $this->belongsTo('App\Models\Exam', 'exam_id');
	}

	public function parents(){
		return $this->hasMany('App\Models\Course\Heritage','course_id');
	}

	public function childs(){
		return $this->hasMany('App\Models\Course\Heritage', 'parent_id');
	}

	public function evals(){
		return $this->hasMany('App\Models\Evaluation','resource_id')->where('free',false);
	}

	/**
	 * Devuelve la cantidad de días en que el curso está "abierto".
	 * @return integer
	 */
	public function openEvals(){
		$now = Carbon::now()->toDateString();
		return $this->hasMany('App\Models\Evaluation','resource_id')
								->where('free',false)
								->where('finish_at','>=',$now)
								->where('start_at', '<=',$now);
	}
}
