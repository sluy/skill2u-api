<?php
namespace App\Models\Course;
use App\Models\Model;

class Heritage extends Model
{
	public $crossRelModel = 'App\Models\Course';

	public $crossRelAlias  = 'parent';

	protected $fillable = ['course_id',
												 'parent_id'];

  public function parent(){
  	return $this->belongsTo('App\Models\Course','parent_id');
  }

  public function course(){
  	return $this->belongsTo('App\Models\Course','course_id');
  }

  public function child(){
  	return $this->course();
  }
}