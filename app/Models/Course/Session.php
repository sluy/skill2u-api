<?php
namespace App\Models\Course;

use App\Models\Model,
		Carbon\Carbon;

class Session extends Model
{
	protected $fillable = ['course_id','start_at','finish_at'];

	protected $appends = ['days','open'];

  public function getDaysAttribute(){
  	if($this->start_at && $this->finish_at){
  		$now = Carbon::now();
  		$finish = Carbon::parse($this->finish_at);
  		return $finish->diffInDays($now);
  	}
  	return -1;
  }

	public function getOpenAttribute(){
		return $this->days >= 0;
	}

  public function course(){
  	return $this->belongsTo('App\Models\Course','course_id');
  }

  public function evals(){
  	return $this->hasMany('App\Models\User\Evaluation','course_session_id');
  }
}