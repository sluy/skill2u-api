<?php
namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes,
		App\Models\User,
		Carbon\Carbon;

class Evaluation extends Model
{
	protected $fillable = [
		'company_id',
		'free',
		'resource_id',
		'title',
		'info',
		'picture',
		'start_at',
    'finish_at',
    'options'
	];

	protected $casts = [
		'free' => 'boolean'
	];
	/*
	protected $appends = [
		'is_open',
		'open_days',
		'approved_count',
		'reproved_count',
		'availables_count',
		'subscribed_count',
		'unsubscribed_count',
		'males_count',
		'females_count',
		'undefined_count',
		'availables',
		'subscribed',
		'unsubscribed',
		'males',
		'females',
		'undefined'
	];*/

  public function getOptionsAttribute($raw) {
    if (is_string($raw) && !empty($raw)) {
      return json_decode($raw);
    }
    return [];
  }

  public function setOptionsAttribute($value) {
    if (is_array($value) || is_object($value)) {
      $value = json_encode($value);
    } else {
      $vlue = null;
    }
    $this->attributes['options'] = $value;
  }

	public function logs(){
		$logs = $this->hasMany('App\Models\User\Log','resource_id')
						 		 ->where('code','like','eval.%')
								 ->orderBy('created_at','DESC');
		return $this->applyFiltersToQuery($logs);
	}

	public function parents(){
		return $this->hasMany('App\Models\Evaluation\Heritage','evaluation_id');
	}

	public function childs(){
		return $this->hasMany('App\Models\Evaluation\Heritage', 'parent_id');
	}

	protected function getGender($value){
		$ids = [];
		foreach($this->getCleanRel('users',true) as $eval){
			if($value === null && $eval->user){
				if($eval->user->profile->sex != 'm' &&
					 $eval->user->profile->sex != 'f'){
					$ids[] = $eval->user_id;
				}
			}
			else if($eval->user && $eval->user->profile->sex == $value){
				$ids[] = $eval->user_id;
			}
		}
		return $ids;
	}

	public function getMalesCountAttribute(){
		return count($this->getMalesAttribute());
	}

	public function getFemalesCountAttribute(){
		return count($this->getFemalesAttribute());
	}

	public function getUndefinedCountAttribute(){
		return count($this->getUndefinedAttribute());
	}

	public function getMalesAttribute(){
		return User::whereIn('id',$this->getGender('m'))->get();
	}

	public function getFemalesAttribute(){
		return User::whereIn('id',$this->getGender('f'))->get();
	}

	public function getUndefinedAttribute(){
		return User::whereIn('id',$this->getGender(null))->get();
	}




	public function getApprovedCountAttribute(){
		return count($this->approved);
	}

	public function getReprovedCountAttribute(){
		return count($this->reproved);
	}

	public function approved(){
		$ids = [0];
		foreach($this->getCleanRel('users',true) as $k => $eval){
			if($eval->presented && $eval->approved_percent <= $eval->score){
				$ids[] = $eval->id;
			}
		}
		$query = $this->users()->whereIn('id',$ids)->orderBy('score','desc');
		return $this->applyFiltersToQuery($query);
	}

	public function reproved(){
		$ids = [0];
		foreach($this->getCleanRel('users',true) as $k => $eval){
			if($eval->presented && $eval->approved_percent > $eval->score){
				$ids[] = $eval->id;
			}
		}
		$query = $this->users()->whereIn('id',$ids)->orderBy('score','asc');
		return $this->applyFiltersToQuery($query);
	}

	/**
	 * Devuelve la cantidad de usuarios habilitados para inscribirse en la evaluación actual.
	 * Hay que tener en cuenta que, sólo devolverá el resultado si la evaluación se
	 * encuentra actualmente abierta.
	 *
	 * @return int
	 */
	public function getAvailablesCountAttribute(){
		return count($this->availables);
	}
	/**
	 * Devuelve los usuarios habilitados para inscribirse en la evaluación actual.
	 * Hay que tener en cuenta que, sólo devolverá el resultado si la evaluación
	 * se encuentra actualmente abierta.
	 *
	 * @return Collection
	 */
	public function getAvailablesAttribute(){
		$users = User::where('company_id',$this->company_id)->with('profile');
		if(!$this->is_open){
			$users->where('id',0);
		}
		$users = $users->get();
		$segmentations = $this->segmentations;
		$levels = $this->levels;
		if(count($segmentations)){
			foreach($users as $k => $user){
				$usegmentations = $user->segmentations;
				$remove = true;
				foreach($segmentations as $segmentation){
					foreach($usegmentations as $usegmentation){
						if($usegmentation->id == $segmentation->id){
							$remove = false;
							break;
						}
					}
				}
				if($remove){
					unset($users[$k]);
				}
			}
		}
		if(count($levels)){
			foreach($users as $k => $user){
				$remove = true;
				foreach($levels as $level){
					if($user->level_id == $level->id){
						$remove = false;
						break;
					}
				}
				if($remove){
					unset($users[$k]);
				}
			}
		}
		return $users;
	}

	/**
	 * Devuelve la cantidad de usuarios que se suscribieron a la  evaluación actual.
	 * @return int
	 */
	public function getSubscribedCountAttribute(){
		return count($this->getSubscribedAttribute());
	}
	/**
	 * Devuelve los usuarios que se suscribieron a la evaluación actual.
	 *
	 * @return Collection
	 */
	public function getSubscribedAttribute(){
		$ids = [];
		foreach($this->getCleanRel('users',true) as $eval){
			$ids[] = $eval->user_id;
		}
		return User::whereIn('id',$ids)->get();
	}
	/**
	 * Devuelve la cantidad de usuarios que aún no se han suscrito a la evaluación actual.
	 *
	 * Hay que tener en cuenta que, sólo devolverá el resultado si la evaluación
	 * se encuentra actualmente abierta.
	 *
	 * @return int
	 */
	public function getUnsubscribedCountAttribute(){
		return count($this->getUnsubscribedAttribute());
	}
	/**
	 * Devuelve los usuarios que aún no se han suscrito a la evaluación actual.
	 *
	 * @return Collection
	 */
	public function getUnsubscribedAttribute(){
		$users = $this->getAvailablesAttribute();
		foreach($users as $k => $user){
			if($user->evals()->where('evaluation_id',$this->id)){
				unset($users[$k]);
			}
		}
		return $users;
	}
	/**
	 * Devuelve la cantidad de días en que el curso está "abierto".
	 * @return integer
	 */
	public function getOpenDaysAttribute(){
		return Carbon::now()->diffInDays(Carbon::parse($this->finish_at)) + 1;
	}

	/**
	 * Determina si la evaluación actual está abierta.
	 * @return boolean
	 */
	public function getIsOpenAttribute(){
		$now = Carbon::now()->toDateString();
		return $now <= $this->finish_at && $now >= $this->start_at;
	}


	public function company(){
		return $this->belongsTo('App\Models\Company','company_id');
	}

	public function levels(){
		return $this->hasMany('App\Models\Evaluation\Level','evaluation_id');
	}

	public function course(){
		if($this->free){
			return null;
		}
		return $this->belongsTo('App\Models\Course','resource_id');
	}

	public function segmentations(){
		return $this->hasMany('App\Models\Evaluation\Segmentation','evaluation_id');
	}

	public function exam(){
		if($this->free){
			return $this->belongsTo('App\Models\Exam','resource_id');
		}
		return $this->course->exam();
	}

	public function users(){
		return $this->hasMany('App\Models\User\Evaluation','evaluation_id');
	}

	public function current_user(){
		$user = app('Api\Api')->auth->user();
		return $this->hasOne('App\Models\User\Evaluation','evaluation_id')
								->where('user_id',($user)?$user->id:0);
	}

	public function delete() {
		foreach (['users','childs','segmentations','levels'] as $name) {
			if ($this->{$name}) {
				foreach($this->{$name} as $rel) {
					$rel->delete();
				}
			}
		}
		return parent::delete();
	}
}
