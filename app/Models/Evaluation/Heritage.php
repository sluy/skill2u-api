<?php
namespace App\Models\Evaluation;
use App\Models\Model;

class Heritage extends Model
{
	public $crossRelModel = 'App\Models\Evaluation';

	public $crossRelAlias  = 'parent';

	protected $fillable = ['evaluation_id',
												 'parent_id'];

  public function parent(){
  	return $this->belongsTo('App\Models\Evaluation','parent_id');
  }

  public function evaluation(){
  	return $this->belongsTo('App\Models\Evaluation','evaluation_id');
  }

  public function child(){
  	return $this->evaluation();
  }
}