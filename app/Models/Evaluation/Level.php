<?php
namespace App\Models\Evaluation;

use App\Models\Model;

class Level extends Model
{
	protected $fillable = ['level_id',
												 'evaluation_id'];
  public function level(){
  	return $this->belongsTo('App\Models\Level','level_id');
  }
  public function evaluation(){
  	return $this->belongsTo('App\Models\Evaluation','evaluation_id');
  }
}