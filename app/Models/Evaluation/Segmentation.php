<?php
namespace App\Models\Evaluation;

use App\Models\Model;

class Segmentation extends Model
{
	protected $fillable = ['segmentation_id',
												 'evaluation_id'];

  public function segmentation(){
  	return $this->belongsTo('App\Models\Segmentation','segmentation_id');
  }

  public function evaluation(){
  	return $this->belongsTo('App\Models\Evaluation','evaluation_id');
  }
}