<?php
namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes,
		Carbon\Carbon;

class Exam extends Model
{
	use SoftDeletes;
  protected $fillable = ['company_id','title','info','picture','auto_weight','random', 'approved_percent','questions','questions_quantity','options'];
  
	protected $casts = [
		'auto_weight' => 'boolean',
		'approved_percent' => 'integer',
		'questions_quantity' => 'integer',
    'questions'          => 'json',
    'random' => 'boolean'
	];
  
  public function getOptionsAttribute($raw) {
    if (is_string($raw) && !empty($raw)) {
      return json_decode($raw);
    }
    return [];
  }

  public function setOptionsAttribute($value) {
    if (is_array($value) || is_object($value)) {
      $value = json_encode($value);
    } else {
      $vlue = null;
    }
    $this->attributes['options'] = $value;
  }

	public function delete() {
		foreach(['courses', 'evals'] as $name) {
			if($this->{$name}) {
				foreach($this->{$name} as $rel) {
					$rel->delete();
				}
			}
		}
		parent::delete();
	}

	public function company(){
		return $this->belongsTo('App\Models\Company','company_id');
	}

	public function courses(){
		return $this->hasMany('App\Models\Course', 'exam_id');
	}

	public function evals(){
		return $this->hasMany('App\Models\Evaluation','resource_id')->where('free',true);
	}


	/**
	 * Devuelve la cantidad de días en que el curso está "abierto".
	 * @return integer 
	 */
	public function openEvals(){
		$now = Carbon::now()->toDateString();
		return $this->hasMany('App\Models\Evaluation','resource_id')
								->where('free',true)
								->where('finish_at','>=',$now)
								->where('start_at', '<=',$now);
	}

}