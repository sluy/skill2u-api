<?php

namespace App\Models;
use Api\Database\Eloquent\Configurable;

class Level extends Model
{
	use Configurable;
	protected $fillable = [
		'company_id',
		'title',
		'picture',
		'order',
		'options',
		'up',
		'down'
	];

	protected $fillableOptions = ['info','color','icon'];


	//protected $appends = ['trans'];

	public function getTransAttribute(){
		return [
			'title'    => app('Api\Api')->translator->gtrans($this->title),
			'options'  => [
				'info' => app('Api\Api')->translator->gtrans($this->getOptions()->get('info','','string'))
			]
		];
	}

	protected $castOptions = [
		'info'  => 'string',
		'color' => 'string',
		'icon'  => 'string'
	];

	public function users(){
		return $this->hasMany('App\Models\User','level_id');
	}

	public function evaluations(){
		return $this->hasMany('App\Models\Evaluation\Level','level_id');
	}

	public function toPerformances(){
		return $this->hasMany('App\Models\User\Performance','to_level_id');
	}

	public function fromPerformances(){
		return $this->hasMany('App\Models\User\Performance','from_level_id');
	}
}
