<?php
namespace App\Models;

class Lib extends Model{

	protected $fillable = ['company_id','title','content','picture','file'];
	/*
	protected $appends = [
		'trans',
		'views_count',
		'downloads_count',
		'categories_count',
    'tags_count',
    'options'
	];*/

  public function getOptionsAttribute($raw) {
    if (is_string($raw) && !empty($raw)) {
      return json_decode($raw);
    }
    return [];
  }

  public function setOptionsAttribute($value) {
    if (is_array($value) || is_object($value)) {
      $value = json_encode($value);
    } else {
      $vlue = null;
    }
    $this->attributes['options'] = $value;
  }

	public function getTransAttribute(){
		return [
			'title'    => app('Api\Api')->translator->gtrans($this->title),
			'content'  => app('Api\Api')->translator->gtrans($this->content)
		];
	}

	public function getViewsCountAttribute(){
		return count($this->views);
	}

	public function getDownloadsCountAttribute(){
		return count($this->downloads);
	}

	public function getCategoriesCountAttribute(){
		return count($this->categories);
	}

	public function getTagsCountAttribute(){
		return count($this->tags);
	}


	public function views(){
		return $this->logs()->where('code','lib.view');
	}

	public function downloads(){
		return $this->logs()->where('code','lib.download');
	}


	public function logs(){
		$logs = $this->hasMany('App\Models\User\Log','resource_id')
						 		 ->where('code','like','lib.%')
								 ->orderBy('created_at','DESC');
		return $this->applyFiltersToQuery($logs);
	}

  public function company(){
  	return $this->belongsTo('App\Models\Company','company_id');
  }

  public function categories(){
  	return $this->hasMany('App\Models\Lib\Category','lib_id');
  }

  public function tags(){
  	return $this->hasMany('App\Models\Lib\Tag','lib_id');
  }

  public function segmentations(){
  	return $this->hasMany('App\Models\Lib\Segmentation','lib_id');
  }

  public function levels(){
  	return $this->hasMany('App\Models\Lib\Level','lib_id');
  }
}
