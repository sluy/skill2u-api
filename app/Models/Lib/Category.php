<?php
namespace App\Models\Lib;

use App\Models\Model;

class Category extends Model
{
	protected $fillable = ['category_id','lib_id'];

  public function lib(){
  	return $this->belongsTo('App\Models\Lib','lib_id');
  }

  public function category(){
  	return $this->belongsTo('App\Models\Category','category_id');
  }
}