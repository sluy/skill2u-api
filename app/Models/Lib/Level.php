<?php
namespace App\Models\Lib;

use App\Models\Model;

class Level extends Model
{
	protected $fillable = ['level_id','lib_id'];

  public function lib(){
  	return $this->belongsTo('App\Models\Lib','lib_id');
  }

  public function level(){
  	return $this->belongsTo('App\Models\Level','level_id');
  }
}