<?php
namespace App\Models\Lib;

use App\Models\Model;

class Segmentation extends Model
{
	protected $fillable = ['segmentation_id','lib_id'];

  public function lib(){
  	return $this->belongsTo('App\Models\Lib','lib_id');
  }

  public function segmentation(){
  	return $this->belongsTo('App\Models\Segmentation','segmentation_id');
  }
}