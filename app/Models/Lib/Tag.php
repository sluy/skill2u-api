<?php
namespace App\Models\Lib;

use App\Models\Model;

class Tag extends Model
{
	protected $fillable = ['tag_id','lib_id'];

  public function lib(){
  	return $this->belongsTo('App\Models\Lib','lib_id');
  }

  public function tag(){
  	return $this->belongsTo('App\Models\Tag','tag_id');
  }
}