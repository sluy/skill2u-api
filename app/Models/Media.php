<?php 

namespace App\Models;

class Media extends Model {
  protected $fillable = ['type','primary_key','resource'];
  
  protected $table = 'media';

  public $timestamps = false;

  public function rel() {
    return $this->belongsTo('App\Models\\'.ucfirst($this->type),'primary_key');
  }
}