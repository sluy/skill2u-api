<?php namespace App\Models;
use Api\Database\Eloquent\Model as Base;

class Model extends Base{
	protected $_dateRange = [];
	protected $_debug = false;

	protected $_appliedFilters = [];


	public function getCleanRel($name,$models=false){
		if(isset($this->relations[$name])){
			unset($this->relations[$name]);
		}
		return $models
					 ? $this->{$name}
					 : $this->{$name}();
	}

	public function hasAppliedFilter($name){
		foreach(func_get_args() as $name){
			if(!isset($this->_appliedFilters[$name])){
				return false;
			}
		}
		return true;
	}

	public function getAppliedFilter($name){
		return isset($this->_appliedFilters[$name])
					 ? $this->_appliedFilters[$name]
					 : null;
	}

	public function setAppliedFilters($filters){
		$this->_appliedFilters = $filters;
		return $this;
	}

	public function hasFilter($name){
		return method_exists($this,$name.'Filter');
	}

	public function applyFiltersToQuery($query){
		foreach($this->_appliedFilters as $name => $opts){
			$name = camel_case($name);
			if(method_exists($this,$name.'Filter')){
				$query = $this->{$name.'Filter'}($query,$opts['value'],$opts['literal']);
			}
		}
		return $query;
	}

	protected function dateRangeFilter($query,$range,$literal,$field='created_at'){
		if(is_array($range) && count($range) === 2){
			$range = array_values($range);
			$query->where($field, '>=', $range[0])
					  ->where($field, '<=', $range[1]);
		}
		return $query;
	}

	protected function periodRangeFilter($query,$performance,$literal,$field='created_at'){
		return $this->dateRangeFilter($query,$performance,$literal,$field);
	}


	/*
	public function debug(){
		$this->_debug = true;
	}

	public function hasDateRange(){
		return !empty($this->_dateRange);
	}

	public function getStartRange(){
		return $this->getDateRange('start');
	}

	public function getFinishRange(){
		return $this->getDateRange('finish');
	}

	public function getDateRange($type=null){
		if(empty($this->_dateRange)){
			return $type === null
						 ? [null,null]
						 : null;
		}
		if($type === null){
			return $this->_dateRange;
		}
		return $type === 'start'
					 ? $this->_dateRange[0]
					 : $this->_dateRange[1];
	}
	public function setDateRange($start,$finish){
		$this->relations = [];
		$this->_dateRange = [$start,$finish];
		return $this;
	}

	public function clearDateRange(){
		$this->relations = [];
		$this->_dateRange = [];
		return $this;
	}

	public function setCurrentPerformanceRange(){
		return $this->setPerformanceRange('current');
	}

	public function setPerformanceRange($performance){
		if($performance === 'current'){
			$range = app('Api\Api')->company->getRuningPeriodRange();
			if($range){
				$this->setDateRange($range[0],$range[1]);
			}
		}else{
			if(!$performance instanceof Performance){
				$performance = Performance::find($performance);
			}
			if($performance){
				$this->setDateRange($performance->start_at,$performance->finish_at);
			}
		}
		return $this;
	}

	public function formatRangeQuery($query,$dateField = 'created_at'){
		if($this->hasDateRange()){
			$query->where($dateField, '>=', $this->getStartRange())
					  ->where($dateField, '<=', $this->getFinishRange());
		}
		return $query;
	}
	*/
}