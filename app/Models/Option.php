<?php
namespace App\Models;

class Option extends Model {


	protected $fillable = ['name','value'];

  public $timestamps = false;

}