<?php

namespace App\Models;
use Api\Database\Eloquent\Configurable;

class Performance extends Model
{
	use Configurable;

	protected $fillable = [
		'company_id',
		'title',
		'use_user_eval',
		'user_eval_weight',
		'start_at',
		'finish_at',
		'finished',
		'picture',
		'options'
	];

	protected $casts = [
		'finished' => 'boolean',
		'use_user_eval' => 'boolean'
	];

	protected $fillableOptions = ['info','color','icon'];

	protected $castsOptions = [
		'info'  => 'string',
		'color' => 'string',
		'icon'  => 'string'
	];

	//protected $appends = ['trans'];

	public function getTransAttribute(){
		return [
			'title'    => app('Api\Api')->translator->gtrans($this->title)
		];
	}

	public function company(){
		return $this->belongsTo('App\Models\Company','company_id');
	}

	public function scores(){
		return $this->hasMany('App\Models\User\Performance','performance_id');
	}
}
