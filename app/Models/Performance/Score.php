<?php namespace App\Models\Performance;

use App\Models\Model;

class Score extends Model
{
	protected $fillable = ['user_id','performance_id','value','info'];

	public function performance(){
		return $this->belongsTo('App\Models\Performance','performance_id');
  }
  
  public function fromLevel() {
    return $this->belongsTo('App\Models\Level', 'from_level_id');
  }

  public function toLevel() {
    return $this->belongsTo('App\Models\Level', 'to_level_id');
  }
	
	public function user(){
		return $this->belongsTo('App\Models\User','user_id');
	}
}