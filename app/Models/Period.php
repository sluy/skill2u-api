<?php
namespace App\Models;
use Api\Database\Eloquent\Configurable;

class Period extends Model
{
	use Configurable;
	
	protected $fillable = [
		'company_id',
		'title',
		'months',
		'start_at',
		'picture',
		'options'
	];

	protected $fillableOptions = ['info','color','icon'];

	protected $castOptions = [
		'info'  => 'string',
		'color' => 'string',
		'icon'  => 'string'
	];

  public function company(){
  	return $this->belongsTo('App\Models\Company','company_id');
  }
}
