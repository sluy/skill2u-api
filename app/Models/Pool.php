<?php namespace App\Models;
use Carbon\Carbon;

//ALTER TABLE `pool_votes`
//CHANGE `value` `value` json NULL AFTER `user_id`;
//ALTER TABLE `pools`
//CHANGE `multi` `valid_answers` int(3) NULL DEFAULT '0' AFTER `answers`;

class Pool extends Model
{
	protected $fillable = ['company_id',
												 'title',
												 'question',
												 'answers',
												 'valid_answers',
												 'results',
												 'start_at',
												 'finish_at',
                         'options'];

	public $casts = [
		'valid_answers' => 'integer'
	];

	public function getValidAnswersAttribute($value) {
		$value = (int) $value;
		if ($value != 1) {
			return count($this->answers);
		}
		return 1;
	}

	public function getIsMultiAttribute() {
		return $this->valid_answers != 1;
	}

	public function getCanVoteAttribute() {
		if ($this->hasVoted) {
			return false;
		}
		$segmentations = Pool::find($this->id)->segmentations;
		if (!count($segmentations)) {
			return true;
		}
		foreach(app('Api\Api')->auth->user()->segmentations as $pivot) {
			foreach($segmentations as $compare) {
				if ($pivot->segmentation_id === $compare->segmentation_id) {
					return true;
				}
			}
		}
		return false;
	}

	public function getAnswersAttribute($raw) {
		if (is_string($raw)) {
			try {
				$tmp = json_decode($raw, true);
				if (is_array($tmp)) {
					$raw = $tmp;
				}
			} catch (\Exception $e) { }
		}
		return is_array($raw)
			? $raw
			: [];
	}

	public function getIsOpenAttribute() {
		if (!$this->start_at && !$this->finish_at) {
			return true;
		}
		$now = Carbon::now()->toDateString();

		return $this->start_at && $this->start_at <= $now && (!$this->finish_at || $now <= $this->finish_at);
	}

	public function getVotesTotalAttribute() {
		return count(Pool::find($this->id)->votes);
	}
	/**
	 * Dtermina si el usuario actual votó en la encuesta.
	 */
	public function getHasVotedAttribute() {
		return (Pool::find($this->id)->votes()->where('user_id', app('Api\Api')->auth->user()->id)->first());
	}

	public function getAnswerVotesCountAttribute() {
		$votes = [];
		foreach($this->answers as $key => $answer) {
			$votes[$key] = 0;
		}
		foreach(Pool::find($this->id)->votes as $vote) {

			foreach($vote->value as $key) {
				if (array_key_exists($key, $votes)) {
					$votes[$key]++;
				}
			}
		}
		return $votes;
	}

	public function getAnswerVotesPercentAttribute() {
		$total = 0;
		$count = $this->answer_votes_count;
		$percents = [];
		foreach($count as $value) {
			$total += $value;
		}
		foreach($count as $key => $value) {
				$percents[$key] = ($value * 100) / $total;
		}
		return $percents;
	}

	public function setAnswersAttribute($value) {
		$this->attributes['answers'] = json_encode(is_array($value) ? $value : [], true);
	}

	public function segmentations() {
		return $this->hasMany('App\Models\Pool\Segmentation', 'pool_id');
	}

	public function votes() {
		return $this->hasMany('App\Models\Pool\Vote', 'pool_id');
	}
}
