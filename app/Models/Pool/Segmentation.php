<?php
namespace App\Models\Pool;

use App\Models\Model;

class Segmentation extends Model
{
	protected $fillable = ['segmentation_id',
												 'evaluation_id'];

  public function segmentation(){
  	return $this->belongsTo('App\Models\Segmentation','segmentation_id');
  }

  public function pool(){
  	return $this->belongsTo('App\Models\Pool','pool_id');
  }
}
