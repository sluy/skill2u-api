<?php
namespace App\Models\Pool;

use App\Models\Model;

class Vote extends Model
{
	protected $fillable = ['pool_id',
												 'user_id',
												 'value'];

  public function pool(){
  	return $this->belongsTo('App\Models\Pool','pool_id');
  }

  public function user() {
    return $this->belongsTo('App\Models\User', 'user_id');
  }

	public function getValueAttribute($value) {
		if (is_string($value)) {
			try {
				$tmp = json_decode($value, true);
				$value = is_array($tmp) ? $tmp : [(int)$value];
			} catch (\Exception $e) {

			}
		}
		return is_array($value) ? $value : [];
	}

	public function setValueAttribute($value) {
		if (is_array($value)) {
			$this->attributes['value'] = json_encode($value,true);
		} else {
			$this->attributes['value'] = json_encode([(int)$value],true);
		}
	}
}
