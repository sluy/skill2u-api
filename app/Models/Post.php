<?php namespace App\Models;

class Post extends Model
{
	protected $fillable = ['user_id',
												 'company_id',
												 'title',
												 'blog',
												 'news',
												 'type',
												 'resource',
												 'content',
                         'date',
                          'options'];

	protected $casts = [
		'blog'   => 'boolean',
		'news'   => 'boolean',
		'date'   => 'datetime',
	];

	/*
	protected $appends = [
		'liked',
		'trans',
		'likes_count',
		'comments_count',
		'views_count',
    'activity',
    'rate_count',
    'rate_overall'
	];*/

  public function getRateCountAttribute() {
    return count($this->rates);
  }

  public function getRateOverallAttribute() {
    $total = 0;
    $qty = 0;
    foreach($this->rates as $rate) {
      $total += (int) $rate->data;
      $qty++;
    }
    return ($qty > 0) ? $total/$qty : 0;
  }

  public function getOptionsAttribute($raw) {
    if (is_string($raw) && !empty($raw)) {
      return json_decode($raw);
    }
    return [];
  }

  public function setOptionsAttribute($value) {
    if (is_array($value) || is_object($value)) {
      $value = json_encode($value);
    } else {
      $vlue = null;
    }
    $this->attributes['options'] = $value;
  }

	public function getActivityAttribute(){
		return $this->getViewsCountAttribute() +
					 $this->getLikesCountAttribute() +
					 $this->getCommentsCountAttribute();
	}

	public function getLikesCountAttribute(){
		return count($this->likes);
	}

	public function getCommentsCountAttribute(){
		return count($this->comments);
	}


	public function getViewsCountAttribute(){
		return count($this->getViewsAttribute());
	}

	public function getViewsAttribute(){
		$files = [];
		return  $this->logs()->where('code','post.view')
												 ->orderBy('created_at','DESC')
												 ->with('user.profile')->get();
	}


	public function logs(){
		$logs = $this->hasMany('App\Models\User\Log','resource_id')
						 		 ->where('code','like','post.%')
								 ->orderBy('created_at','DESC');
		return $this->applyFiltersToQuery($logs);
	}

	public function getTransAttribute(){
		return [
			'title'   => app('Api\Api')->translator->gtrans($this->title),
			'content' => app('Api\Api')->translator->gtrans($this->content),
		];
	}

	public function getLikedAttribute(){
		$user = app('Api\Api')->auth->user();
		if($user && $this->id){
			if($this->likes()->where('user_id',$user->id)->first()){
				return true;
			}
		}
		return false;
	}

  public function user(){
  	return $this->belongsTo('App\Models\User','user_id');
  }

  public function company(){
  	return $this->belongsTo('App\Models\Company','company_id');
  }

  public function segmentations(){
  	return $this->hasMany('App\Models\Post\Segmentation', 'post_id');
  }

  public function categories(){
	  return $this->hasMany('App\Models\Post\Category','post_id');
  }

  public function tags(){
	return $this->hasMany('App\Models\Post\Tag','post_id');
}

  public function levels(){
  	return $this->hasMany('App\Models\Post\Level','post_id');
  }

  public function likes(){
  	return $this->hasMany('App\Models\User\Feedback', 'primary_key')->where(function($q){
  		return $q->where('type','like')
  						 ->where('name','post');
  	});
  }

  public function comments(){
  	return $this->hasMany('App\Models\User\Feedback', 'primary_key')->where(function($q){
  		return $q->where('type','comment')
               ->where('name','post')
               ->with('media');
  	});
  }

  public function rates(){
  	return $this->hasMany('App\Models\User\Feedback', 'primary_key')->where(function($q){
  		return $q->where('type','rate')
  						 ->where('name','post');
  	});
  }

  public static function allowed($user){
  	if(!$user instanceof User){
  		$user = User::find($user);
  	}
  	$segmentations = $user->segmentations()->pluck('segmentation_id')->toArray();
  	$query = self::doesntHave('segmentations');
  	if(count($segmentations)){
  		$query->orWhereHas('segmentations',function($q) use($segmentations){
								$q->whereIn('segmentation_id',$segmentations);
						   });
  	}
  	$query->where('company_id',$user->company_id);
  	return $query;
  }
}
