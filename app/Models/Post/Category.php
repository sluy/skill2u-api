<?php
namespace App\Models\Post;

use App\Models\Model;

class Category extends Model
{
	protected $fillable = ['category_id','post_id'];

  public function post(){
  	return $this->belongsTo('App\Models\Post','post_id');
  }

  public function category(){
  	return $this->belongsTo('App\Models\Category','category_id');
  }
}