<?php
namespace App\Models\Post;
use App\Models\Model;

class Level extends Model
{
	protected $fillable = ['level_id',
												 'post_id'];

  public function level(){
  	return $this->belongsTo('App\Models\Level','level_id');
  }

  public function post(){
  	return $this->belongsTo('App\Models\Post','post_id');
  }
}