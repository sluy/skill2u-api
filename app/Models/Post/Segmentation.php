<?php
namespace App\Models\Post;
use App\Models\Model;

class Segmentation extends Model
{
	protected $fillable = ['segmentation_id',
												 'post_id'];

  public function segmentation(){
  	return $this->belongsTo('App\Models\Segmentation','segmentation_id');
  }

  public function post(){
  	return $this->belongsTo('App\Models\Post','post_id');
  }
}