<?php
namespace App\Models\Post;

use App\Models\Model;

class Tag extends Model
{
	protected $fillable = ['tag_id','post_id'];

  public function lib(){
  	return $this->belongsTo('App\Models\Post','post_id');
  }

  public function tag(){
  	return $this->belongsTo('App\Models\Tag','tag_id');
  }
}