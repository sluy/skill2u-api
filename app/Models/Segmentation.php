<?php

namespace App\Models;
use Api\Database\Eloquent\Configurable;

class Segmentation extends Model
{
	use Configurable;
	protected $fillable = ['company_id','name','picture','options','parent_id'];

	protected $fillableOptions = ['info','color','icon'];

	protected $castOptions = [
		'info'  => 'string',
		'color' => 'string',
		'icon'  => 'string'
	];

	//protected $appends = ['trans'];

	public function getTransAttribute(){
		return [
			'name'    => app('Api\Api')->translator->gtrans($this->name)
		];
	}


	public function childs(){
		return $this->hasMany('App\Models\Segmentation','parent_id','id');
	}

	public function parent(){
		return $this->belongsTo('App\Models\Segmentation','parent_id');
	}

  public function users(){
  	return $this->hasMany('App\Models\User\Segmentation','segmentation_id')->with('user');
  }

  public function libs(){
  	return $this->hasMany('App\Models\Lib\Segmentation', 'segmentation_id')->with('lib');
  }
}
