<?php
namespace App\Models;

class Tag extends Model
{
	protected $fillable = ['company_id','title','info','options'];

	//protected $appends = ['trans'];

  public function getOptionsAttribute($raw) {
    if (is_string($raw) && !empty($raw)) {
      return json_decode($raw);
    }
    return [];
  }

  public function setOptionsAttribute($value) {
    if (is_array($value) || is_object($value)) {
      $value = json_encode($value);
    } else {
      $vlue = null;
    }
    $this->attributes['options'] = $value;
  }

	public function getTransAttribute(){
		return [
			'title'    => app('Api\Api')->translator->gtrans($this->title)
		];
	}

  public function company(){
  	return $this->belongsTo('App\Models\Company','company_id');
  }

  public function libs(){
  	return $this->hasMany('App\Models\Lib\Tag','tag_id');
  }

  public function posts() {
	  return $this->hasMany('App\Models\Post\Tag', 'post_id');
  }
}
