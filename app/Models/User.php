<?php
namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes,
		Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract,
		Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract,
		Api\Contracts\Options\Configurable as ConfigurableContract,
		Api\Database\Eloquent\Configurable,
		Api\Api\Auth\Authenticatable,
		Api\Api\Auth\Authorizable,
		App\Models\User\Feedback,
		App\Models\User\Evaluation as UserEvaluation,
		App\Models\User\Log as UserLog;

class User extends Model implements AuthenticatableContract,
																		AuthorizableContract,
																		ConfigurableContract {
	use Authenticatable,Authorizable,SoftDeletes,Configurable;
  protected $fillable = ['company_id',
  											 'level_id',
  											 'email',
  											 'password',
  											 'options',
  											 'system_admin',
                         'company_admin',
                         'adobe_role'];

  protected $hidden = ['password'];

	protected $fillableOptions = ['allow_multiple_sessions','locale'];

	protected $castOptions = [
		'allow_multiple_sessions' => 'boolean',
		'locale'                  => 'string'
	];

	protected $casts = [
		'system_admin'  => 'boolean',
		'company_admin' => 'boolean'
	];
	/*
	protected $appends = [
		'rank',
		'unique_system_admin',
		'unique_company_admin',
		'subscribed_count',
		'presented_count',
		'unpresented_count',
		'posts_count',
		'comments_count',
		'likes_count',
		'activity',
		'downloaded_count',
		'downloaded'
	];*/

	public function getAdobeRoleAttribute($value) {
    return empty($value) ? 'student' : $value;
	}

	public function getActivityAttribute(){
		return $this->getPresentedCountAttribute() +
					 $this->getPostsCountAttribute() +
					 $this->getCommentsCountAttribute() +
					 $this->getDownloadedCountAttribute();
	}

	public function getDownloadedCountAttribute(){
		return count($this->getDownloadedAttribute());
	}

	public function getDownloadedAttribute(){
		$files = [];
		$logs = UserLog::where('user_id', $this->id)
								->where('code','lib.download')
								->orderBy('created_at','DESC');
		foreach($logs as $log){
			$log->toArray();
			$log['file'] = Lib::find($log->resource_id)->toArray();
			$files[] = $log;
		}
		return $files;
	}

	public function getSubscribedCountAttribute(){
		return count($this->applyFiltersToQuery(UserEvaluation::where('user_id', $this->id))->get());
	}

	public function subscribed(){
		return $this->applyFiltersToQuery($this->getCleanRel('evals'));
	}

	public function getPresentedCountAttribute(){
		return count($this->applyFiltersToQuery(UserEvaluation::where('user_id', $this->id)->where('presented', true))->get());
	}

	public function presented(){
		return $this->subscribed()->where('presented',true);
	}

	public function getUnpresentedCountAttribute(){
		return count($this->applyFiltersToQuery(UserEvaluation::where('user_id', $this->id)->where('presented', false))->get());
	}

	public function unpresented(){
		return $this->subscribed()->where('presented',false);
	}

	public function getUniqueSystemAdminAttribute(){
		return $this->id &&
					 $this->system_admin &&
					 !User::where('system_admin',1)
							 ->where('id','!=',$this->id)
							 ->first();
	}

	public function getUniqueCompanyAdminAttribute(){
		return $this->id &&
					 $this->company_admin &&
					 $this->company &&
					 !$this->company->users()->where('company_admin',1)
						 								       ->where('id','!=',$this->id)
													         ->first();
	}


	public function logs(){
		return $this->applyFiltersToQuery($this->hasMany('App\Models\User\Log','user_id'));
	}

  public function sessions(){
  	return $this->hasMany('App\Models\User\Session','user_id');
	}

	public function recoveries() {
		return $this->hasMany('App\Models\User\Recoveries', 'user_id');
	}

  public function company(){
  	return $this->belongsTo('App\Models\Company','company_id');
  }

  public function segmentations(){
  	return $this->hasMany('App\Models\User\Segmentation', 'user_id');
  }

  public function profile(){
    return $this->hasOne('App\Models\User\Profile','user_id');
  }

  public function performances(){
  	return $this->hasMany('App\Models\User\Performance', 'user_id');
  }

  public function lastestPerformance(){
  	$performance = (!$this->company)
  								 ? null
  								 : $this->company
												->performances()
												->where('finished',1)
												->orderBy('finish_at','desc')
												->first();
		$performance = ($performance)
								   ? $performance->id
								   : 0;
		return $this->hasOne('App\Models\User\Performance')->where('performance_id',$performance);
  }

  public function getRankAttribute(){
  	$lastest = $this->lastestPerformance;
  	return $lastest
  				 ? $lastest->rank
  				 : null;
  }

  public function feedback(){
  	return $this->applyFiltersToQuery($this->hasMany('App\Models\User\Feedback','user_id'));
  }

  public function level(){
  	return $this->belongsTo('App\Models\Level','level_id');
  }

  public function evals(){
  	return $this->hasMany('App\Models\User\Evaluation','user_id');
  }

  public function getPostsCountAttribute(){
  	return count(Post::where('user_id', $this->id)->get());
  }

  public function posts(){
  	return $this->hasMany('App\Models\Post','user_id')
  							->orderBy('created_at','DESC');
  }


  public function getCommentsCountAttribute(){
  	return count(Feedback::where('user_id', $this->id)->where('type', 'comment')->get());
  }

  public function comments(){
  	return $this->getCleanRel('feedback')->where(function($q){
  		$q->where('type','comment');
  	})->orderBy('created_at','DESC');
  }

  public function getLikesCountAttribute(){
		return count(Feedback::where('user_id', $this->id)->where('type', 'like')->get());
  }

  public function likes(){
  	return $this->getCleanRel('feedback')->where(function($q){
  		$q->where('type','like');
  	})->orderBy('created_at','DESC');
  }

  public function notifications(){
  	return $this->hasMany('App\Models\User\Notification','user_id')
  							->orderBy('created_at','DESC');
  }

	public static function generatePassword($length = 8,
																	 $add_dashes = false,
																	 $available_sets = 'luds'){
		$sets = array();
		if(strpos($available_sets, 'l') !== false)
			$sets[] = 'abcdefghjkmnpqrstuvwxyz';
		if(strpos($available_sets, 'u') !== false)
			$sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
		if(strpos($available_sets, 'd') !== false)
			$sets[] = '23456789';
		if(strpos($available_sets, 's') !== false)
			$sets[] = '!@#$%&*?';
		$all = '';
		$password = '';
		foreach($sets as $set)
		{
			$password .= $set[array_rand(str_split($set))];
			$all .= $set;
		}
		$all = str_split($all);
		for($i = 0; $i < $length - count($sets); $i++)
			$password .= $all[array_rand($all)];
		$password = str_shuffle($password);
		if(!$add_dashes)
			return $password;
		$dash_len = floor(sqrt($length));
		$dash_str = '';
		while(strlen($password) > $dash_len)
		{
			$dash_str .= substr($password, 0, $dash_len) . '-';
			$password = substr($password, $dash_len);
		}
		$dash_str .= $password;
		return $dash_str;
	}

  /*
  public function getSystemPerformanceAttribute(){
  	$media = 0;
  	$count = 0;
  	foreach($this->courses as $course){
  		$media += $course->score;
  		$count++;
  	}
  	return $count > 0 ? number_format($media/$count, 2, '.', '') : '0.00' ;
  }

  public function getSystemLevelAttribute(){
  	return $this->company->levels()
  				        			 ->where('points','<=',$this->system_performance)
  								       ->orderBy('points','desc')
  								       ->first();
  }

  public function getPerformanceAttribute(){
  	if(isset($this->company->options['auto_performance']) &&
  		 $this->company->options['auto_performance'] === true){
  		return $this->system_performance;
  	}
  	$media = 0;
  	//Último performance creado
  	$last = $this->company->performances()->orderBy('created_at','desc')->first();
  	if($last){
  		$tmp = $this->performances()->where('performance_id',$last->id)->first();

  		if($tmp && $tmp->value > 0){
  			$media = $tmp->value;
  		}
  	}
  	return number_format($media, 2, '.', '');
  }

  public function getLevelAttribute(){
  	return $this->company->levels()
  				        			 ->where('points','<=',$this->performance)
  								       ->orderBy('points','desc')
  								       ->first();
  }
  */
}
