<?php namespace App\Models\User;
use App\Models\Model;

class Evaluation extends Model
{
	protected $fillable = [
												 'user_id',
												 'evaluation_id',
												 'approved_percent',
												 'score',
												 'exam'
												];

	protected $casts = [
		'presented' => 'boolean',
		'exam'   => 'json'
	];

	protected $appends  = ['approved'];

	public function getApprovedAttribute(){
		return $this->score >= $this->approved_percent;
	}

  public function user(){
  	return $this->belongsTo('App\Models\User','user_id');
  }

  public function evaluation(){
  	return $this->belongsTo('App\Models\Evaluation','evaluation_id');
	}
}