<?php namespace App\Models\User;

use App\Models\Model;
use App\Models\Post;

class Feedback extends Model {
  protected $fillable = ['user_id','type','name','primary_key','data'];

  public function user(){
  	return $this->belongsTo('App\Models\User','user_id');
  }
  protected $appends = ['trans'];

	public function getTransAttribute(){
		return [
			'data'    => app('Api\Api')->translator->gtrans($this->data)
		];
	}

  public function rel(){
  	return $this->belongsTo('App\Models\\'.ucfirst($this->name),'primary_key','id');
  }

  public function media() {
    return $this->hasMany('App\Models\\Media', 'primary_key')->where('type', 'feedback');
  }
}