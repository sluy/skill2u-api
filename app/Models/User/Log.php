<?php namespace App\Models\User;

use App\Models\Model;

class Log extends Model {
  protected $fillable = ['user_id','code','resource_id','data'];

  public function user(){
  	return $this->belongsTo('App\Models\User','user_id');
  }
}