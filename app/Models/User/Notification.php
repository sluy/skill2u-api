<?php namespace App\Models\User;

use App\Models\Model;

class Notification extends Model {	
  protected $fillable = ['user_id','type','title','message','unread'];
  
  public function user(){
  	return $this->belongsTo('App\Models\User','user_id');
  }
}