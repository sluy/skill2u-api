<?php namespace App\Models\User;
use App\Models\Model;

class Performance extends Model
{
	protected $fillable = [
												 'user_id',
												 'performance_id',
												 'from_level_id',
												 'to_level_id',
												 'system_eval',
												 'user_eval',
												 'total'
												];


	public function getRankAttribute(){
		if(!$this->performance->finished){
			return null;
		}
		$items = Performance::where('performance_id',$this->performance_id)
												->orderBy('total','desc')
												->get();
		$count = 1;
		foreach($items as $item){
			if($item->user_id == $this->user_id){
				return $count;
			}
			$count++;
		}
	}

  public function user(){
  	return $this->belongsTo('App\Models\User','user_id');
  }
  public function performance(){
  	return $this->belongsTo('App\Models\Performance','performance_id');
  }

  public function fromLevel(){
  	return $this->belongsTo('App\Models\Level','from_level_id');
  }

  public function toLevel(){
  	return $this->belongsTo('App\Models\Level','to_level_id');
  }
}