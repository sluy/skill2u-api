<?php namespace App\Models\User;

use App\Models\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Api\Database\Eloquent\Configurable;

class Profile extends Model
{
	use Configurable {
		getOptionsAttribute as getSocialAttribute;
		setOptionsAttribute as setSocialAttribute;
	}
  protected $fillable = [
    'user_id',
    'name',
    'lastname',
    'birthday',
    'sex',
    'phone',
    'bio',
    'photo',
    'social'];

  protected $optionsField = 'social';

  protected $fillableOptions = ['facebook','twitter','skype'];

  public function user(){
  	return $this->belongsTo('App\Models\User','user_id');
  }
}