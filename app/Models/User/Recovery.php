<?php namespace App\Models\User;

use App\Models\Model;

class Recovery extends Model {
	
  protected $fillable = ['user_id','token'];

  public function user(){
  	return $this->belongsTo('App\Models\User','user_id');
  }
}