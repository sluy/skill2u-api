<?php namespace App\Models\User;

use App\Models\Model;

class Segmentation extends Model
{
	protected $fillable = ['user_id','segmentation_id'];


	public function segmentation(){
		return $this->belongsTo('App\Models\Segmentation','segmentation_id');
	}

	public function user(){
		return $this->belongsTo('App\Models\User','user_id');
	}
}