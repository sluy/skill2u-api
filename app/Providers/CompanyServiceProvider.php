<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Response;
use App\Services\Company;

class CompanyServiceProvider extends ServiceProvider
{
	public function register(){
		$this->app->singleton(Company::class,function($app){
			return new Company();
		});
	}

  /**
   * Register the application's response macros.
   *
   * @return void
   */
  public function boot()
  {
  }
}