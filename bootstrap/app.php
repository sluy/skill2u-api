<?php

require_once __DIR__.'/../vendor/autoload.php';

function can_trash($model){
	return $model instanceof Illuminate\Database\Eloquent\Model && 
				 in_array('Illuminate\Database\Eloquent\SoftDeletes', class_uses($model));
}


try {
    (new Dotenv\Dotenv(__DIR__.'/../'))->load();
} catch (Dotenv\Exception\InvalidPathException $e) {
    //
}

/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
|
| Here we will load the environment and create the application instance
| that serves as the central piece of this framework. We'll use this
| application as an "IoC" container and router for this framework.
|
*/

$app = new Laravel\Lumen\Application(
    realpath(__DIR__.'/../')
);

 $app->withFacades();

 $app->withEloquent();

/*
|--------------------------------------------------------------------------
| Register Container Bindings
|--------------------------------------------------------------------------
|
| Now we will register a few bindings in the service container. We will
| register the exception handler and the console kernel. You may add
| your own bindings here if you like or you can make another file.
|
*/

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    App\Exceptions\Handler::class
);

$app->singleton(
    Illuminate\Contracts\Console\Kernel::class,
    App\Console\Kernel::class
);

/*
|--------------------------------------------------------------------------
| Register Middleware
|--------------------------------------------------------------------------
|
| Next, we will register the middleware with the application. These can
| be global middleware that run before and after each request into a
| route or middleware that'll be assigned to some specific routes.
|
*/

 $app->middleware([
    Api\Http\Middleware\CorsMiddleware::class,
 	Api\Http\Middleware\JsonRequest::class
 ]);

 $app->routeMiddleware([
    //Usuario autenticado
   'auth'           => Api\Http\Middleware\Auth::class,
//     'auth'           => App\Http\Middleware\Authenticate::class,
   //Administrador de la compañía
   'admin'          => App\Http\Middleware\Admin::class,
   'member'         => App\Http\Middleware\Member::class,
   //Administrador del sistema
   'sysop'          => App\Http\Middleware\Sysop::class,
 ]);

/*
|--------------------------------------------------------------------------
| Register Service Providers
|--------------------------------------------------------------------------
|
| Here we will register all of the application's service providers which
| are used to bind services into the container. Service providers are
| totally optional, so you are not required to uncomment this line.
|
*/
 $app->register(App\Providers\AppServiceProvider::class);
 //$app->register(App\Providers\AuthServiceProvider::class);
 $app->register(App\Providers\EventServiceProvider::class);
 $app->register(Api\Providers\ApiServiceProvider::class);

// $app->register(App\Providers\CatchAllOptionsRequestsProvider::class);

// 
// composer require flipbox/lumen-generator
$app->register(Flipbox\LumenGenerator\LumenGeneratorServiceProvider::class);

// composer require flipbox/lumen-generator
$app->register(Api\Providers\CatchAllOptionsRequestsProvider::class);
/*
|--------------------------------------------------------------------------
| Load The Application Routes
|--------------------------------------------------------------------------
|
| Next we will include the routes file so that they can all be added to
| the application. This will provide all of the URLs the application
| can respond to, as well as the controllers that may handle them.
|
*/

Api\Api::bootstrap($app);
return $app;
