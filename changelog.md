# Changelog

## [0.0.18] - 2018-05-24

### Fixes
- Forbidden Score FIx.

## [0.0.17] - 2018-05-24

### Fixes
- Duplicated cors header *.

## [0.0.16] - 2018-05-24

### Fixes
- Cors.
- Order By Downloads & Views (LIBS)

## [0.0.15] - 2018-05-23

### Fixes
- Dump en userinfo.

## [0.0.14] - 2018-05-22

### Fixes
- Eliminación de relaciones corregida (Evaluaciones, Cursos, Exámenes),

## [0.0.13] - 2018-05-21

### Cambios

- Endpoint misc/proxy trabajará ahora sólo con el método "get" (usa file_get_contents en vez de cURL, el servidor no lo tiene instalado).

## [0.0.12] - 2018-05-20

### Adiciones

- Endpoint para obtener la información de sitios web foraneos (POST misc/proxy).

### Correcciones

- Agregados eliminaciones de diversos modelos.
- Bugs generales.

## [0.0.11] - 2018-05-17

### Correcciones
- Relaciones en filtros personalizados.  
- Corrección a un error que al enviar `null` en un filtro devolvía todos los registros.
- Descripción de los filtros en la paginación de evaluaciones.

## [0.0.10] - 2018-05-17

### Correcciones
- Autogenerador de períodos.  

## [0.0.9] - 2018-05-16

### Cambios
- Se disminuyó el límite mínimo en la paginación de `10` a `2`.  

## [0.0.8] - 2018-05-16

### Correcciones
- Edición de segmentaciones sin padre (`id = 0` o `null`)

# Changelog
## [0.0.7] - 2018-05-15

### Cambios
- Imágen opcional en los recursos de la biblioteca.

### Correcciones
- Endpoints del Postman corregidos.
- Se puede cambiar satisfactoriamente la fecha de nacimiento en la administración de usuarios.
- Se pueden eliminar satisfactoriamente niveles que no tengan asociaciones.

## [0.0.6] - 2018-05-13

### Adiciones
- Endpoints para la recuperación de contraseñas.

### Correcciones
- Cronjob : Se descubrió que el bug del cronjob al crear períodos no era un problema de codificación. Faltaba la definición del mismo para el subdominio de desarrollo (dapi.skill2u.com).

## [0.0.5] - 2018-05-13

### Adiciones
- Exclusión de campos en paginaciones y búsquedas. Se habilitaron los parámetros del query string `only=campo1,campo2,....` para mostrar sólo los campos suministrados, y `exclude=campo1,campo2` para mostrar los campos que no coincidan con los dados. Si está definido `exclude` no tomará en cuenta `only`.

### Correcciones
- Los mensajes de error personalizados muestran las variables asociadas correctamente (por ejemplo, `:mime` => `pdf` al subir un archivo de mime diferente a pdf biblioteca).
- Se muestra el error `required` al no suministrar una imagen en la creación de un recurso en la biblioteca.

# Changelog

## [0.0.4] - 2018-05-12

### Adiciones
- Changelog

### Correcciones
- Discriminación de campos en la paginación : Ahora la paginación permite filtrar campos con los parámetros del query string `search_fields`  y `search_values`.

## [0.0.3] - 2018-05-12

### Adiciones
- Changelog

### Correcciones
- Tiempo de respuesta : Deshabilitadas las traducciones del sistema usando la API de Google Translator.

## [0.0.2] - 2018-05-12

### Correcciones
- php artisan serve. Se corrigió un error en el sistema y ahora se puede correr localmente.

## [0.0.1] - 2018-05-12

### Adiciones
- Api base entera.

### Bugs
- php artisan serve error.
- Tiempo de respuesta (+ 30000 ms).
- Paginación sin discriminación de campos.