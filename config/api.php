<?php return [
	'auth' => [
		'session' => [
			'token' => [
				'from'         => 'all',
				'name'         => 'Authorization',
				'expiration'   => 24 //Un día. Por defecto una hora.
			]		
		],
	],
	'user' => [
		'models' => [
			'user'    => 'App\User',
			'profile' => 'App\UserProfile',
			'group'   => 'App\UserGroup',
			'session' => 'App\UserSession'
		],
	],
	'company' => [
		'session' => [
			'token' => [
				'from'         => 'header',
				'name'         => 'Company'
			]		
		],
	],
	'upload' => [
		'path' => 'uploads',
		'url'  => 'uploads'
	],
];