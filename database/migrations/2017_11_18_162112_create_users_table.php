<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('company_id')->unsigned()->nullable()->default(0);
            $table->bigInteger('level_id')->unsigned()->nullable()->default(0);
            $table->string('email',255);
            $table->string('password',64);
            $table->boolean('system_admin')->nullable()->default(false);
            $table->boolean('company_admin')->nullable()->default(false);
            $table->string('adobe_role',255)->nullable()->default('');
            $table->json('options')->nullable();
            $table->softDeletes();
            $table->timestamps();
            $table->unique('email');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
