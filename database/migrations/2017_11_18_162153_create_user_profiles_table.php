<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_profiles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->string('name',64)->nullable()->default('');
            $table->string('lastname',64)->nullable()->default('');
            $table->date('birthday')->nullable()->default('1900-01-01');
            $table->string('sex',1)->nullable()->default('');
            $table->string('phone',64)->nullable()->default('');
            $table->text('bio')->nullable();
            $table->string('photo',255)->nullable()->default('');
            $table->json('social')->nullable();
            $table->softDeletes();
            $table->timestamps();
            $table->index('user_id');
            $table->foreign('user_id')->references('id')->on('users')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_profiles');
    }
}
