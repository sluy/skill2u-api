<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('company_id')->unsigned();
            $table->bigInteger('user_id')->unsigned();
            $table->string('title',64)->nullable()->default('');
            $table->boolean('news')->nullable()->default(0);
            $table->string('type',12);
            $table->string('resource',255)->nullable();
            $table->text('content')->nullable()->default('');
            $table->datetime('date')->nullable()->default('1900-01-01 00:00:00');
            $table->json('levels')->nullable();
            $table->timestamps();
            $table->foreign('company_id')->references('id')->on('companies')
                  ->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
