<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostSegmentationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_segmentations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('post_id')->unsigned();
            $table->bigInteger('segmentation_id')->unsigned();
            $table->timestamps();
            $table->foreign('post_id')
            			->references('id')
            			->on('posts')
                  ->onDelete('cascade');
            $table->foreign('segmentation_id')
            			->references('id')
            			->on('segmentations')
                  ->onDelete('cascade');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_segmentations');
    }
}