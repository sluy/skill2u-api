<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('categories', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->bigInteger('company_id')->unsigned();
        $table->string('title',64);
        $table->text('info')->nullable()->default('');
        $table->string('picture',255)->nullable();
        $table->string('color',255)->nullable();
        $table->timestamps();
        $table->foreign('company_id')->references('id')->on('companies')
              ->onDelete('cascade');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('categories');
    }
}
