<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLibsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('libs', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->bigInteger('company_id')->unsigned();
        $table->string('title',64);
        $table->text('content')->nullable();
        $table->string('picture',255);
        $table->string('file',255);
        $table->timestamps();
        $table->foreign('company_id')->references('id')->on('companies')
              ->onDelete('cascade');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('libs');
    }
}
