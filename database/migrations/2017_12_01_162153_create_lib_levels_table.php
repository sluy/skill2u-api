<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLibLevelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lib_levels', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('lib_id')->unsigned();
            $table->bigInteger('level_id')->unsigned();
            $table->timestamps();
            $table->foreign('lib_id')
            			->references('id')
            			->on('libs')
                  ->onDelete('cascade');
            $table->foreign('level_id')
            			->references('id')
            			->on('levels')
                  ->onDelete('cascade');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lib_levels');
    }
}