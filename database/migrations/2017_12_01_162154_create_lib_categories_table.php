<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLibCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lib_categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('lib_id')->unsigned();
            $table->bigInteger('category_id')->unsigned();
            $table->timestamps();
            $table->foreign('lib_id')
            			->references('id')
            			->on('libs')
                  ->onDelete('cascade');
            $table->foreign('category_id')
            			->references('id')
            			->on('categories')
                  ->onDelete('cascade');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lib_categories');
    }
}