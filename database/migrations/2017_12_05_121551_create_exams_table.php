<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('exams', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->bigInteger('company_id')->unsigned();
        $table->string('title',64);
        $table->text('info')->nullable();
        $table->string('picture',255)->nullable();
        $table->boolean('auto_weight')->nullable()->default(false);
        $table->decimal('approved_percent',5,2)->nullable()->default('75.00');
        $table->integer('questions_quantity')->nullable()->default(0);
        $table->json('questions')->nullable();
        $table->timestamps();
        $table->softDeletes();
        $table->foreign('company_id')->references('id')->on('companies')
              ->onDelete('cascade');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('exams');
    }
}
