<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('courses', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->bigInteger('company_id')->unsigned();
        $table->bigInteger('exam_id')->unsigned();
        $table->string('title',64);
        $table->text('info')->nullable();
        $table->string('picture',255)->nullable()->default('');
        $table->string('type');
        $table->string('resource',255)->nullable()->default('');
        $table->timestamps();
        $table->softDeletes();
        $table->foreign('company_id')->references('id')->on('companies')
              ->onDelete('cascade');
        $table->foreign('exam_id')->references('id')->on('exams')
              ->onDelete('cascade');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('courses');
    }
}