<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseHeritagesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('course_heritages', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->bigInteger('course_id')->unsigned();
      $table->bigInteger('parent_id')->unsigned();
      $table->timestamps();
      $table->foreign('course_id')->references('id')->on('courses')
            ->onDelete('cascade');
      $table->foreign('parent_id')->references('id')->on('courses')
            ->onDelete('cascade');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('course_heritages');
  }
}