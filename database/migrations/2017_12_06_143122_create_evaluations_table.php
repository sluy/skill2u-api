<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvaluationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evaluations', function (Blueprint $table) {
            $table->bigIncrements('id');
        		$table->bigInteger('company_id')->unsigned();
            $table->boolean('free')->nullable()->default(0);
            $table->bigInteger('resource_id')->unsigned();
            $table->string('title',64);
		        $table->text('info')->nullable();
		        $table->string('picture',255)->nullable()->default('');
		        $table->date('start_at');
		        $table->date('finish_at');
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('company_id')
            			->references('id')
            			->on('companies')
                  ->onDelete('cascade');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evaluations');
    }
}