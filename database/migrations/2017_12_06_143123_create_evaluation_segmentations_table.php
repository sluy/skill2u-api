<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvaluationSegmentationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evaluation_segmentations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('evaluation_id')->unsigned();
            $table->bigInteger('segmentation_id')->unsigned();
            $table->timestamps();
            $table->foreign('evaluation_id')
            			->references('id')
            			->on('evaluations')
                  ->onDelete('cascade');
            $table->foreign('segmentation_id')
            			->references('id')
            			->on('segmentations')
                  ->onDelete('cascade');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evaluation_segmentations');
    }
}