<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvaluationHeritagesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('evaluation_heritages', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->bigInteger('evaluation_id')->unsigned();
      $table->bigInteger('parent_id')->unsigned();
      $table->timestamps();
      $table->foreign('evaluation_id')->references('id')->on('evaluations')
            ->onDelete('cascade');
      $table->foreign('parent_id')->references('id')->on('evaluations')
            ->onDelete('cascade');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('evaluation_heritages');
  }
}