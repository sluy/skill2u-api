<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserEvaluationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_evaluations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('evaluation_id')->unsigned();
            $table->boolean('presented')->nullable()->default(false);
            $table->decimal('approved_percent',5,2)->nullable()->default('75.00');
            $table->decimal('score',5,2)->nullable()->default('0.00');
            $table->json('exam')->nullable();

            $table->timestamps();
            $table->index(['user_id','evaluation_id']);
            $table->foreign('user_id')
            			->references('id')
            			->on('users')
                  ->onDelete('cascade');
            $table->foreign('evaluation_id')
            			->references('id')
            			->on('evaluations')
                  ->onDelete('cascade');            
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_evaluations');
    }
}