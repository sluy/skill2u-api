<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerformancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('performances', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('company_id')->unsigned();
            $table->string('title',64);
            $table->string('picture',255)->nullable();
            $table->json('options')->nullable();
            $table->decimal('user_eval_weight',5,2)->nullable()->default('75.00');
            $table->date('start_at');
            $table->date('finish_at');
            $table->boolean('finished')->nullable()->default(false);
            $table->softDeletes();
            $table->timestamps();
            $table->index(['company_id']);
            $table->foreign('company_id')->references('id')->on('companies')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('performances');
    }
}