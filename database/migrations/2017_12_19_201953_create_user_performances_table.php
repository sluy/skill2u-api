<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPerformancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_performances', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('performance_id')->unsigned();
            $table->bigInteger('from_level_id')->unsigned();
            $table->bigInteger('to_level_id')->unsigned();
            $table->decimal('system_eval',5,2)->nullable();
            $table->decimal('user_eval',5,2)->nullable();
            $table->decimal('total',5,2)->nullable();
            $table->timestamps();
            $table->index(['user_id','performance_id']);
            $table->foreign('user_id')->references('id')->on('users')
                  ->onDelete('cascade');
            $table->foreign('performance_id')->references('id')->on('performances')
                  ->onDelete('cascade');
            $table->foreign('from_level_id','from_level_foreign')->references('id')->on('levels')
                  ->onDelete('cascade');
            $table->foreign('to_level_id','to_level_foreign')->references('id')->on('levels')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_performances');
    }
}