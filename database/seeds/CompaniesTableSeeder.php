<?php
use Illuminate\Database\Seeder,
		Carbon\Carbon;

class CompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('companies')->insert([
        	'name'    => 'Skill2U',
        	'logo'    => '',
        	'status'  => true,
        	'options' => '{"info":"Compañía maestra.","user_eval_weight": 20,"notify_users":true,"notify_courses":true,"notify_comments":true,"notify_news":true}',
        	'created_at' => Carbon::now()->toDateTimeString(),
        	'updated_at' => Carbon::now()->toDateTimeString(),
        ]);
    }
}


