<?php
use Illuminate\Database\Seeder,
		Carbon\Carbon;

class CoursesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('courses')->insert([
        	'company_id'        => 1,
        	'exam_id'           => 1,
        	'title'             => 'Nivel I',
        	'info'              => 'Curso No 1',
        	'picture'           => '',
        	'type'              => 'youtube',
        	'resource'          => 'https://youtu.be/-xySL11xtMs',
        	'created_at' => Carbon::now()->toDateTimeString(),
        	'updated_at' => Carbon::now()->toDateTimeString(),
        ]);

        DB::table('courses')->insert([
        	'company_id'        => 1,
        	'exam_id'           => 1,
        	'title'             => 'Nivel II',
        	'info'              => 'Curso No 2',
        	'picture'           => '',
        	'type'              => 'youtube',
        	'resource'          => 'https://youtu.be/-xySL11xtMs',
        	'created_at' => Carbon::now()->toDateTimeString(),
        	'updated_at' => Carbon::now()->toDateTimeString(),
        ]);

        DB::table('courses')->insert([
        	'company_id'        => 1,
        	'exam_id'           => 1,
        	'title'             => 'Nivel III',
        	'info'              => 'Curso No 3',
        	'picture'           => '',
        	'type'              => 'youtube',
        	'resource'          => 'https://youtu.be/-xySL11xtMs',
        	'created_at' => Carbon::now()->toDateTimeString(),
        	'updated_at' => Carbon::now()->toDateTimeString(),
        ]);

    }
}




