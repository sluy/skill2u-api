<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	//composer dump-autoload -o  al agregar seeders
	    $this->call([
	        /*
	        CompaniesTableSeeder::class,
	        LevelsTableSeeder::class,
	        ExamsTableSeeder::class,
	        CoursesTableSeeder::class,
	        EvaluationsTableSeeder::class,
	        EvaluationLevelsTableSeeder::class,
	        PeriodsTableSeeder::class,
	         */
	        UsersTableSeeder::class,
	        UserProfilesTableSeeder::class,
	    ]);
    }
}
