<?php
use Illuminate\Database\Seeder,
		Carbon\Carbon;

class EvaluationLevelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$now = Carbon::now();
        DB::table('evaluation_levels')->insert([
        	'evaluation_id' => 1,
        	'level_id'      => 1
        ]);
        DB::table('evaluation_levels')->insert([
        	'evaluation_id' => 2,
        	'level_id'      => 2
        ]);
        DB::table('evaluation_levels')->insert([
        	'evaluation_id' => 3,
        	'level_id'      => 3
        ]);
    }
}




