<?php
use Illuminate\Database\Seeder,
		Carbon\Carbon;

class EvaluationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$now = Carbon::now();
        DB::table('evaluations')->insert([
        	'company_id'        => 1,
        	'title'             => 'Exámen Bronce',
        	'info'              => 'Curso-Examen para nivel Bronce.',
        	'resource_id'       => 1,
        	'start_at'          => $now->copy()->subMonths(3)->toDateString(),
        	'finish_at'         => $now->copy()->toDateString(),
        	'created_at' => $now->copy()->subMonths(3)->toDateTimeString(),
        	'updated_at' => $now->copy()->subMonths(3)->toDateTimeString(),
        ]);
        DB::table('evaluations')->insert([
        	'company_id'        => 1,
        	'title'             => 'Exámen Plata',
        	'info'              => 'Curso-Examen para nivel Plata.',
        	'resource_id'       => 2,
        	'start_at'          => $now->copy()->subMonths(3)->toDateString(),
        	'finish_at'         => $now->copy()->toDateString(),
        	'created_at' => $now->copy()->subMonths(3)->toDateTimeString(),
        	'updated_at' => $now->copy()->subMonths(3)->toDateTimeString(),
        ]);
        DB::table('evaluations')->insert([
        	'company_id'        => 1,
        	'title'             => 'Exámen Oro',
        	'info'              => 'Curso-Examen para nivel Oro.',
        	'resource_id'       => 3,
        	'start_at'          => $now->copy()->subMonths(3)->toDateString(),
        	'finish_at'         => $now->copy()->toDateString(),
        	'created_at' => $now->copy()->subMonths(3)->toDateTimeString(),
        	'updated_at' => $now->copy()->subMonths(3)->toDateTimeString(),
        ]);
    }
}




