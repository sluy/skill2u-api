<?php
use Illuminate\Database\Seeder,
		Carbon\Carbon;

class ExamsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('exams')->insert([
        	'company_id'        => 1,
        	'title'             => 'Test Drive',
        	'info'              => 'Prueba General',
        	'picture'           => '',
        	'auto_weight'       => 1,
        	'approved_percent'  => '80.00',
        	'questions_quantity' => 2,
        	'questions'         =>'[{"text":"Primera","weight":10,"correct_answers":[1,2],"answers_quantity":4,"answers":["respuesta1","respuesta2(c)","respuesta3(c)","respuesta4","respuesta5","respuesta6"]},{"text":"Segunda","weight":50,"correct_answers":[1],"answers_quantity":0,"answers":["respuesta1","respuesta2(c)","respuesta3"]},{"text":"Tercera","weight":40,"correct_answers":[3],"answers_quantity":3,"answers":["respuesta1","respuesta2","respuesta3","respuesta4(c)","respuesta5"]}]',
        	'created_at' => Carbon::now()->toDateTimeString(),
        	'updated_at' => Carbon::now()->toDateTimeString(),
        ]);
    }
}


