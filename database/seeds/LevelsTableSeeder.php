<?php
use Illuminate\Database\Seeder,
		Carbon\Carbon;

class LevelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('levels')->insert([
        	'company_id'    => 1,
        	'title'         => 'Bronce',
        	'picture'    => '',
        	'order'   => 0,
        	'options' => '{"info":"Principiante","color":"red","icon":"fa fa-user"}',
        	'created_at' => Carbon::now()->toDateTimeString(),
        	'updated_at' => Carbon::now()->toDateTimeString(),
        ]);

        DB::table('levels')->insert([
        	'company_id'    => 1,
        	'title'         => 'Plata',
        	'picture'    => '',
        	'order'   => 1,
        	'options' => '{"info":"Intermedio","color":"blue","icon":"fa fa-user"}',
        	'created_at' => Carbon::now()->toDateTimeString(),
        	'updated_at' => Carbon::now()->toDateTimeString(),
        ]);

        DB::table('levels')->insert([
        	'company_id'    => 1,
        	'title'         => 'Oro',
        	'picture'    => '',
        	'order'   => 2,
        	'options' => '{"info":"Avanzado","color":"green","icon":"fa fa-user"}',
        	'created_at' => Carbon::now()->toDateTimeString(),
        	'updated_at' => Carbon::now()->toDateTimeString(),
        ]);
    }
}


