<?php
use Illuminate\Database\Seeder,
		Carbon\Carbon;

class PeriodsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('periods')->insert([
        	'company_id'        => 1,
        	'title'             => 'Trimestral',
        	'start_at'          => Carbon::now()->subMonths(3)->toDateString(),
        	'months'            => 3,
        	'created_at' => Carbon::now()->toDateTimeString(),
        	'updated_at' => Carbon::now()->toDateTimeString(),
        ]);
    }
}




