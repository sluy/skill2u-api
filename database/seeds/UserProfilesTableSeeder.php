<?php

use Illuminate\Database\Seeder,
		Carbon\Carbon;

class UserProfilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_profiles')->insert([
        	'user_id'    => 1,
        	'name'       => 'Stefan',
        	'lastname'   => 'Luy',
        	'sex'        => 'm',
        	'birthday'   => '1983-12-26',
        	'bio'        => 'Administrador maestro.',
        	'phone'      => '+584261120951',
        	'social'     => '{"facebook":"","twitter":"","skype":""}',
        	'created_at' => Carbon::now()->toDateTimeString(),
        	'updated_at' => Carbon::now()->toDateTimeString(),
        ]);
    }
}
