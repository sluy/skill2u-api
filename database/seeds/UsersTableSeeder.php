<?php

use Illuminate\Database\Seeder,
		Illuminate\Support\Facades\Hash,
		Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        	'company_id'    => 0,
        	'level_id'      => 1,
        	'email'         => 'admin@skill2u.com',
        	'password'      => Hash::make('112321Asd'),
        	'system_admin'  => true,
        	'company_admin' => true,
        	'options'       => '{"allow_multiple_sessions":false}',
        	'created_at'    => Carbon::now()->toDateTimeString(),
        	'updated_at'    => Carbon::now()->toDateTimeString(),
        ]);
    }
}
