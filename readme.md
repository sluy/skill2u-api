# Configuración del servidor:

##Apache
Configurar apache con los módulos **rewrite** y **headers**. 
Si el módulo **rewrite** no es activado, no se resolverán las rutas del sistema y si sucede lo mismo con el módulo **headers**, el servidor devolverá **CORS**.

##PHP
A su vez, usar la versión de **PHP 7.X**(7.1 recomendado) con todos los módulos necesarios para Laravel (Mbstring,OpenSSL,etc...)-

Para que el sistema permita la subida de archivos mayores a 2 MB, aumentar los valores de los parámetros `upload_max_filefize` y `post_max_size` en el `php.ini`:

+ `upload_max_filesize = 100M`
+ `post_max_size = 100M `

Si estos parámetros no son modificados, cuando se suba un archivo mayor a los 2 MB la api resolverá un request vacío.

####Nota

Para ver la ubicación real del `php.ini`, entrar al root de la api (por ejemplo:[http://lapi.skill2u.com](http://lapi.skill2u.com/)) y buscar el parámetro `Configuration File (php.ini) Path`. Su valor será la ruta absoluta del archivo.

## Bases de datos

La Api utiliza por defecto **MySQL 5.X** . Por defecto se trabajará con la base de datos y configuración de la cuenta de acceso:

+ **database** : skill2u_api
+ **user**     : root
+ **pass**     : 112321Asd


#API

##Instalación

Para instalar la api y sus dependencias sólo resta copiar este repositorio y tener instalado y configurado [Composer](https://getcomposer.org/). Luego correr desde la consola/terminal:

`composer install`

## Configuración

+ Mover el .env.example a .env (tal como laravel) y configurar los datos de la bd.
+ Ejecutar desde la consola/terminal `php artisan migrate` y `php artisan db:seed`.

Al terminar la configuración creará en el sistema la empresa **Skill2U** y los usuarios *admin@skill2u.com* y *sluy1283@gmail.com* . Ambos usuarios usan de contraseña *112321Asd*.

## Rutas (Endpoints):

Para el testing de los endpoints se recomienda el uso de la aplicación [Postman](https://www.getpostman.com/).

En el archivo *~/postman.json* están todas las rutas del sistema. A su vez en archivo **~/postman.vars.png** hay una captura demo de variables de entorno para el sistema.

De manera general, las rutas quedaron :

+ `~/auth`    : Alberga todo lo relacionado con la autenticación del usuario.
+ `~/backend` : Contiene todas las rutas relacionadas con la administración de la compañía.
+ `~/frontend`: Incluye todas las rutas asociadas a la información que pueden ver y manejar los usuarios de la compañía.

##Paginación:

Todos los endpoints llamados **Paginate** permiten paginar registros.
La paginación del sistema soporta distintos tipos de variables por medio del query de la petición para la discriminación y ordenamiento teniendo así que por ejemplo sobre la ruta [lapi.skill2u.com/backend/exams?](lapi.skill2u.com/backend/exams?):

+ `search_fields=title,info&search_values=hola,mundo` : Buscará los registros cuyo título contenga los caracteres `hola` y en la información sea `mundo`.
+ `order_by=title` : Ordenará los registros por su título.
+ `order_inverse=true` : El ordenamiento será de mayor a menor.
+ `limit=5` : Mostrará como máximo 5 registros por página.
+ `trashed=true` : Incluirá los resultados eliminados. Sólo disponible si el modelo de los registros tiene implementados **Soft Deletes**.
+ `all=true` : Mostrará todos los resultados sin paginación. Si esta opción está activada ignorará `limit`.

##Envío de datos:

El sistema soporta la recepción de datos tanto por `json` como por `form-data`. En el caso de usar `form-data` (por ejemplo, al subir archivos) siempre usar el **spoffing** enviando los datos por el método `POST` y definiendo en el query string la variable `_method={nombre}`. 

Un ejemplo de  `PUT` sobre los exámenes sería (lapi.skill2u.com/backend/exams/1?\_method=put)[lapi.skill2u.com/backend/exams/1?\_method=put].

##Autenticación

Es simple y lo normal. Un usuario loguea y envía el token. El token generado deberá ser definido en el header `Authorization` de la petición y de alli ya el sistema puede determinar si es un administrador o no. Cada vez que se loguea el sistema crea un nuevo token, esto para permitir el inicio de sesión desde múltiples dispositivos. Esta opción estará deshabilitada si en las opciones del usuario está establecido el parámetro `allow_multiple_connections=false`.

##Internacionalización

El sistema implementa la internacionalización para distintos tipos de idiomas. Para cambiar el idioma a mostrar definir en el header `Accept-Language` las siglas del idioma a usar (Por ej `Accept-Language=es`). 

##Usuarios y compañías

Tal cómo se especificó, se removió la opción de múltiples compañías para un usuario. Esto quiere decir que ahora la administración de la compañía activa incluye también la administración total de los usuarios que la componen (incluyendo sus perfiles).

##Exámenes

Los exámenes serán la herramienta fundamental para medir el desempeño de los usuarios en los cursos. La calificación obtenida en ellos siempre será en base a **100**, después de todo más que una calificación regular es una media porcentual de desempeño.

Al crear/actualizar un exámen la estructura base será:

```javascript
{
	/**
	 * Título de la prueba.
	 * @var {String}
	 */
  "title" : "Primera prueba",
  /**
   * Información adicional de la prueba.
   * @var {String|null}
   */
  "info"  : "Primera prueba diseñada en el sistema",
  /**
   * Imagen de la prueba.
   * Sólo puede ser definida si se envían los datos actuales por form-data.
   * @var {File|null}
   */
  "picture" : null,
  /**
   * Especifica la cantidad de preguntas a mostrar en el examen. Si se definieron 20 preguntas y el 
   * valor de {question_quantity=4}, el sistema sorteará aleatoriamente la selección de 4 preguntas 
   * dentro de las 20 definidas.
   * Esto se implementó para evitar (si se desea) la generación de exámenes exactamente iguales (sólo
   * con las preguntas en posiciones distintas) y poder tener exámenes realmente distintos.
   * Si {question_quantity=0} se mostrarán todas las preguntas definidas en el examen.
   * @var {Integer}
   */
  "question_quantity" : 4 
  /**
   * Determinará si el "peso" o porcentajes de cada pregunta serán establecidos automáticamente por el
   * sistema. Si {question_quantity>0}, este parámetro será siempre {TRUE}.
   * Contemplar que si {auto_weight=false}, se deberá definir en cada pregunta el "peso" de la misma, y,
   * la sumatoria del peso de cada pregunta deberá ser exactamente igual a {100}.
   * @var {Boolean}
   */
  "auto_weight" : false,
  /**
   * Definición de las preguntas.
   * Cada elemento de la matriz será una pregunta.
   * @var {Array}
   */
  "questions" : [
  	{
  		/**
  		 * Texto a mostrar en la pregunta.
  		 * @var {String}
  		 */
  		"text"   : "Primera pregunta",
  		/**
  		 * "Peso" o valor en porcentaje de la pregunta. Sólo será tomado en cuenta si la opción principal 
  		 * {auto_weight = false}. Tomar en cuenta que si por ejemplo la pregunta tiene un peso de {20} y existen 
  		 * dos respuestas correctas, cada respuesta correcta valdrá {10}.
  		 * @var {Number}
  		 */
  		"weight" : 20,
  		/**
  		 * Claves de las respuestas correctas dentro de la matriz de respuestas. Mínimo debe existir una 
  		 * respuesta correcta en cada pregunta.
  		 * @var {Array}
  		 */
  		"correct_answers" : [0,1],
  		/**
  		 * Cantidad de respuestas correctas a mostrar. Trabaja exactamente igual a la opción {question_quantity}.
  		 * @var {Integer}
  		 */
  		"answers_quantity": 4,
  		/**
  		 * Matriz con las respuestas de la pregunta.
  		 * El valor de cada item de la matriz deberá ser el texto a mostrar.
  		 * @var {Array}
  		 */
  		"answers" : [
  			"Respuesta 1", "Respuesta 2", "Respuesta 3", "Respuesta 4", "Respuesta 5", "Respuesta 6"
  		]
  	}
  ]
}
```

##Performance's de usuarios

La compañía tendrá dos tipos de performance para los usuarios, uno será el calculado por medio de las calificaciones obtenidas en los cursos (`system_performance`) y otro que será especificado por la administración de la empresa (`performance`).

Así como los exámenes, dicho performance será tomado a razón porcentual, siendo 100 lo máximo que se pueda obtener por un usuario.

#### System Performance

En cuanto al performance del sistema, será sólo una media de la calificación obtenida por el usuario en los exámenes y cursos a los cuales se suscribió.

El performance del sistema estará disponible bajo el parámetro `system_performance` en todas las rutas donde se devuelve el modelo de un usuario.

#### Performance de "Campo"

El performance de "Campo" es especificado por la administración del sistema y es usado para visualizar si el nivel académico obtenido por el usuario compagina con el desempeño real del mismo. 

El performance de campo estará disponible bajo el parámetro `performance` en todas las rutas donde se devuelve el modelo de un usuario. Hay que tomar en cuenta que cada vez que se genere un nuevo "corte" de performance, este valor será `0.00` hasta que se defina por la administración. 

En el caso que la empresa quiera deshabilitar el performance de campo y usar por defecto el del sistema, establecer en las opciones de la empresa (en la ruta **~/backend/company**) `auto_performance=true`.

##Niveles
La empresa podrá definir niveles dependiendo de el performance que han tenido los usuarios tanto por sistema como en el "campo". 

Para obtener el nivel del usuario se dispondrán de los parámetros `system_level`(bajo el performance del sistema) y `level` (bajo el performance de campo).