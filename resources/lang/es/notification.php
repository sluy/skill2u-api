<?php
return [
	'company_register' => [
		'title'   => 'Skill2U: Su compañía [company.name] ha sido registrada.',
		'message' => 'Su compañía <b>[company.name]</b> ha sido registrada en el sistema.' .
								 'Para iniciar sesión use las siguientes credenciales: <br/><br/>'.
							 	 'Usuario: [user.email]<br/>'.
							 	 'Contraseña: [user.password]<br/><br/><br/>'
	],
	'sysop_register' => [
		'title'   => 'Bienvenido a Skill2U',
		'message' => 'Su cuenta ha sido registrada en el sistema.' .
								 'Para iniciar sesión use las siguientes credenciales: <br/><br/>'.
								 'Usuario: [user.email]<br/>'.
								 'Contraseña: [user.password]<br/><br/><br/>'
	],
	'user_register'  => [
		'title'   => 'Bienvenido a Skill2U',
		'message' => 'Su cuenta de skill2u ha sido creada por la compañía [company.name].' .
								 'Para iniciar sesión use las siguientes credenciales: <br/><br/>'.
							   'Usuario: [user.email]<br/>'.
							   'Contraseña: [user.password]<br/>'
	],
	'user_recovery_start' => [
		'title'    => 'Recuperación de cuenta',
		'message'  => 'Para poder continuar con el proceso de recuperación de su cuenta, ' .
									'haga <a href="dhome.skill2u.com/auth/recovery/:token">click en '.
									'este enlace</a> o copie y pegue la siguiente dirección en el '.
									'navegador de su preferencia: <br/><br/>' .
									'<b>dhome.skill2u.com/auth/recovery/:token</b><br/><br/>' . 
									'Recuerde que el enlace será válido por 48 horas.'
	],
	'user_recovery_finish' => [
		'title'   => 'Contraseña Cambiada',
		'message' => 'La contraseña de su cuenta ha sido cambiada satisfactoriamente.<br/>' .
								 'Recuerde mantener sus credenciales seguras.' 
	]
];