<?php
return [
		'unknow'       => 'La petición ha sido manejada por el sistema pero la respuesta es desconocida.',
    'success'       => 'Operación exitosa.',
    'ok'           => 'Operación exitosa.',
    'created'      => 'Recurso Creado.',
    'updated'      => 'Recurso Actualizado.',
    'deleted'      => 'Recurso eliminado.',
    'restored'     => 'Recurso restaurado.',
    'error'        => 'Error en los datos suministrados.',
    'failure'      => 'Error interno en el sistema.',
    'bad'          => 'Petición erronea.',
    'notfound'     => 'Recurso no encontrado.',
    'forbidden'    => 'No tiene permisos para acceder al recurso.',
    'unauthorized' => 'Debe iniciar sesión para acceder al recurso.'
];