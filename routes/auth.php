<?php
$this
->controller('Auth\User')
->post('signin')
->get('@recoveryCheck', '/recovery/{token}')
->post('@recoveryStart','/recovery')
->post('@recoveryFinish','recovery/{token}')
->group(['middleware' => 'auth'], function($router){
	$router->get('current')
				 ->put('passwordChange')
				 ->patch('profileChange')
				 ->options('optionsChange')
				 ->delete('signout');
});