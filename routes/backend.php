<?php
$this
->controller('Backend\Company')
->middleware(['admin'])
->get('@find')
->put('@update');