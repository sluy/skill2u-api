<?php 
$this->crud()
->get('singleXls', '{id}/export/xls')
->get('xls', 'export/xls')
->get('csv', 'export/csv')
->cross('heritages');