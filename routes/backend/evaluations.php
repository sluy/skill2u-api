<?php 
$this
->get('xls', '{id}/export/xls')
->crud()
->cross('segmentations')
->cross('levels')
->cross('heritages')
->controller('Evaluations\Users')
->get('@paginate','/{cid}/users')
->get('@find','/{cid}/user/{id}');