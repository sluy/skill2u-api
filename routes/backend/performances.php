<?php 
$this
->get('paginate')
->get('find','{id}')
->put('update','{id}')
->get('csv', '{id}/csv')
->get('xls', '{id}/xls')
->get('scores', '{id}/all_scores')

->controller('Performances\Scores')
->get('paginate', '{pid}/scores')
->get('find', '{pid}/scores/{id}')
->patch('import','{pid}/scores')
->put('store','{pid}/scores/{id}');


/*
->controller('Performances\Scores')
->get('paginate','{pid}/scores')
->get('find','{pid}/scores/{id}')
->put('saveAll','{pid}/scores')
->delete('deleteAll','{pid}/scores');
*/




