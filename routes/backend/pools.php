<?php
  $this->crud()
       ->get('xls', 'export/xls')
       ->get('csv', 'export/csv')
  		 ->cross('segmentations');
