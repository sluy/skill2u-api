<?php 
$this->crud()
     ->get('xls', 'export/xls')
     ->get('csv', 'export/csv')
     ->cross('categories')
		 ->cross('segmentations')
		 ->cross('levels');