<?php
$this->get('@likedPosts','liked')
		 ->get('@commentedPosts','commented')
		 ->get('@commenterUsers','commenter')
		 ->get('@systemPerformance','system_performance')
		 ->get('@performance','performance')
		 ->get('@onlineUsers','online');
		 