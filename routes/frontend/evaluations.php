<?php
$this
->get('@paginate')
->get('@actives', '/actives')
->get('@find','/{id}')
->post('@join','/{id}')
->put('@evaluate', '/{id}');

