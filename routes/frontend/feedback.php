<?php
$this
->get('@paginate', '/{name}/{pk}/{type}')
->get('@find', '/{name}/{pk}/{type}/{id}')
->post('@create', '/{name}/{pk}/{type}')
->put('@update', '/{name}/{pk}/{type}/{id}')
->delete('@delete', '/{name}/{pk}/{type}/{id}');