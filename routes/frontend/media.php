<?php
$this
->get('@paginate', '/{type}/{pk}')
->get('@find', '/{type}/{pk}/{id}')
->post('@create', '/{type}/{pk}')
->put('@update', '/{type}/{pk}/{id}')
->delete('@delete', '/{type}/{pk}/{id}');