<?php
$this
->crud()
->put('@like','/{id}/likes')

->controller('Posts\Comments')
->get('@paginate', '{fid}/comments')
->get('@find','{fid}/comments/{id}')
->post('@create','{fid}/comments')
->put('@update','{fid}/comments/{id}')
->delete('@delete','{fid}/comments/{id}');